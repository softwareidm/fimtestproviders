﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.LoadProvider
{
    public static class ShuffleExension
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            // based on fisher yates
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }

    public class LoadRuleFunctions : IRuleFunctions
    {
        static Dictionary<string, Tuple<long, int>> Increments;
        public static AttributeValue Increment(AttributeValue seed, AttributeValue context, AttributeValue interval)
        {
            if (Increments == null)
            {
                Increments = new Dictionary<string, Tuple<long, int>>();
            }

            if (seed == null)
            {
                seed = new AttributeValue(0);
            }

            if (interval == null)
            {
                interval = new AttributeValue(1);
            }

            var cont = "increment";
            if (context != null && context.StringValue != null)
            {
                cont = context.StringValue + "_increment";
            }

            if (!Increments.ContainsKey(cont))
            {
                Increments.Add(cont, new Tuple<long, int>(seed.IntegerValue, LoadFixture.CurrentIteration));
            }

            if (Increments[cont].Item2 != LoadFixture.CurrentIteration)
            {
                Increments[cont] = new Tuple<long, int>(Increments[cont].Item1 + interval.IntegerValue, LoadFixture.CurrentIteration);
            }

            return new AttributeValue(Increments[cont].Item1);
        }

        public static AttributeValue RandName(AttributeValue context)
        {
            var cont = "randName";
            if (context != null && context.StringValue != null)
            {
                cont = context.StringValue + "_randName";
            }

            var index = (int)Rand(new AttributeValue(0), new AttributeValue(Names.NameGroups.Count), new AttributeValue(cont)).IntegerValue;
            var subContext = new AttributeValue(cont + "_subList");
            var subList = Names.NameGroups[index];
            var subI = (int)Rand(new AttributeValue(0), new AttributeValue(subList.Length - 1), subContext).IntegerValue;
            return new AttributeValue(subList[subI]);
        }

        static Random rand;
        static Dictionary<string, Tuple<int, int>> randCache;
        public static AttributeValue Rand(AttributeValue min, AttributeValue max, AttributeValue context)
        {

            if (max == null)
            {
                max = min;
                min = new AttributeValue(0);
            }

            if (rand == null)
            {
                rand = new Random();
            }

            var cont = "rand";
            if (context != null && context.StringValue != null)
            {
                cont = context.StringValue + "_rand";
            }

            if (randCache == null)
            {
                randCache = new Dictionary<string, Tuple<int, int>>();
            }

            if (!randCache.ContainsKey(cont) || randCache[cont].Item1 != LoadFixture.CurrentIteration)
            {
                randCache[cont] = new Tuple<int, int>(LoadFixture.CurrentIteration, rand.Next((int)min.IntegerValue, (int)max.IntegerValue));
            }

            return new AttributeValue(randCache[cont].Item2);
        }

        static Dictionary<string, Tuple<int, int, List<int>>> trackRand;
        public static AttributeValue UniqueRand(AttributeValue min, AttributeValue max, AttributeValue context)
        {
            if (max == null)
            {
                max = min;
                min = new AttributeValue(0);
            }

            if (rand == null)
            {
                rand = new Random();
            }

            var cont = "uniquerand";
            if (context != null && context.StringValue != null)
            {
                cont = context.StringValue + "_uniquerand";
            }

            if (trackRand == null)
            {
                trackRand = new Dictionary<string, Tuple<int, int, List<int>>>();
            }

            // if the range of available numbers is less than twice number 
            if (LoadFixture.TotalIterations * 2 > (max.IntegerValue - min.IntegerValue))
            {
                if (!trackRand.ContainsKey(cont))
                {
                    var list = new List<int>((int)(max.IntegerValue - min.IntegerValue));
                    for (int i = (int)min.IntegerValue; i < max.IntegerValue; i++)
                    {
                        list.Add(i);
                    }

                    list.Shuffle();
                    trackRand.Add(cont, new Tuple<int,int,List<int>>(LoadFixture.CurrentIteration, 0, list));
                }

                var entry = trackRand[cont];
                if (entry.Item1 != LoadFixture.CurrentIteration)
                {
                    trackRand[cont] = new Tuple<int, int, List<int>>(LoadFixture.CurrentIteration, entry.Item2 + 1, entry.Item3);
                }

                return new AttributeValue(entry.Item3[trackRand[cont].Item2]);
            }
            else
            {
                if (!trackRand.ContainsKey(cont))
                {
                    var r = rand.Next((int)min.IntegerValue, (int)max.IntegerValue);
                    trackRand.Add(cont, new Tuple<int, int, List<int>>(LoadFixture.CurrentIteration, r, new List<int> { r }));
                }

                if (trackRand[cont].Item1 != LoadFixture.CurrentIteration)
                {
                    var seen = trackRand[cont].Item3;
                    int r = rand.Next((int)min.IntegerValue, (int)max.IntegerValue);
                    while (seen.Contains(r))
                    {
                        r = rand.Next((int)min.IntegerValue, (int)max.IntegerValue);
                    }

                    seen.Add(r);
                    trackRand[cont] = new Tuple<int, int, List<int>>(LoadFixture.CurrentIteration, r, seen);
                }

                return new AttributeValue(trackRand[cont].Item2);
            }
        }

        public static AttributeValue Memo(AttributeValue name)
        {
            return new AttributeValue(Evaluate.MemoValues[name.StringValue]);
        }

        static Dictionary<string, List<Dictionary<string, object>>> dataCache;
        static void SetCache(string system)
        {
            if (dataCache == null)
            {
                dataCache = new Dictionary<string, List<Dictionary<string, object>>>();
            }

            if (!dataCache.ContainsKey(system))
            {
                var prov = LoadFixture.Systems[system].Provider;
                var DataImage = prov.GetType().GetMethod("DataImage");
                var add = (List<Dictionary<string, object>>)DataImage.Invoke(prov, new object[] { });
                dataCache.Add(system, add);
            }
        }

        public static AttributeValue Pick(AttributeValue system, AttributeValue field, AttributeValue context)
        {
            SetCache(system.StringValue);
            var cont = "_pick";
            if (context != null && context.StringValue != null)
            {
                cont = context.StringValue + "_pick";
            }

            var index = Rand(new AttributeValue(0), new AttributeValue(dataCache[system.StringValue].Count), new AttributeValue(cont));
            return new AttributeValue(dataCache[system.StringValue][(int)index.IntegerValue][field.StringValue]);
        }

        public static AttributeValue PickUnique(AttributeValue system, AttributeValue field, AttributeValue context)
        {
            SetCache(system.StringValue);
            var cont = "_pickunique";
            if (context != null && context.StringValue != null)
            {
                cont = context.StringValue + "_pickunique";
            }

            SetCache(system.StringValue);
            var index = UniqueRand(new AttributeValue(0), new AttributeValue(dataCache[system.StringValue].Count), new AttributeValue(cont));
            return new AttributeValue(dataCache[system.StringValue][(int)index.IntegerValue][field.StringValue]);
        }

        public static AttributeValue PickIter(AttributeValue system, AttributeValue field, AttributeValue offset)
        {
            SetCache(system.StringValue);

            var index = LoadFixture.CurrentIteration + (int)offset.IntegerValue;

            return new AttributeValue(dataCache[system.StringValue][index][field.StringValue]);
        }

        public static AttributeValue DataCount(AttributeValue system)
        {
            SetCache(system.StringValue);

            return new AttributeValue(dataCache[system.StringValue].Count);
        }
    }
}
