﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Newtonsoft.Json;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.LoadProvider
{
    

    public class LoadFixture : IFixture
    {
        public string Name { get; set; }
        public string Iterations { get; set; }
        [JsonIgnore]
        public SyncSystem System { get; set; }
        public string Operation { get; set; }
        public List<KeyValuePair<string, string>> Memos { get; set; }
        public List<object> Fixtures { get; set; }
        [JsonIgnore]
        public string Path { get; set; }
        public event ProgressEvent ReportProgress = delegate { };

        internal static int currentIteration;
        internal static int totalIterations;
        internal static string currentFixture;
        [JsonIgnore]
        public static int CurrentIteration { get { return currentIteration; } }
        [JsonIgnore]
        public static int TotalIterations { get { return totalIterations; } }
        [JsonIgnore]
        public static string CurrentFixture { get { return currentFixture; } }
        [JsonIgnore]
        public static IDictionary<string, SyncSystem> Systems { get; set; }

        public void Load()
        {
            totalIterations = int.Parse(Fixture.HandleVal(Iterations).ToString());
            currentFixture = this.Name;
            for (int i = 0; i < totalIterations; i++)
            {
                currentIteration = i;
                foreach (var memo in Memos)
                {
                    Evaluate.MemoValues[memo.Key] = Fixture.HandleVal(memo.Value);
                }

                bool doFix = true;
                foreach (var obj in Fixtures)
                {
                    if (obj is string) // condition element
                    {
                        doFix = new AttributeValue(Fixture.HandleVal((string)obj)).BooleanValue;
                    }
                    else
                    {
                        if (!doFix)
                        {
                            continue;
                        }
                        
                        var fix = (IFixture)obj;
                        try
                        {
                            fix.Load();
                        }
                        catch (Exception e)
                        {
                            ReportProgress.Invoke(this, new ProgressEventArgs(MessageLevel.Error,
                                "Error {0} loading fixture {1}\r\n{2}",
                                e.Message, Fixture.HandleVal(fix.Name), e.StackTrace));

                            if (e.InnerException != null)
                            {
                                ReportProgress.Invoke(this, new ProgressEventArgs(
                                    MessageLevel.Error,
                                    "{0}:\r\n{1}",
                                    e.InnerException.Message, e.InnerException.StackTrace));
                            }
                        }
                    }
                }

                int mod = 10;
                while (totalIterations / mod > 100)
                {
                    mod *= 10;
                }

                if (i % mod == 0 && i > 0)
                {
                    ReportProgress.Invoke(this, new ProgressEventArgs("Loaded {0} times", i));
                }
            }
        }

        public void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            Memos = new List<KeyValuePair<string, string>>();
            foreach (var attr in new XName[] { "name", ModelHelper.XSI + "type", "operation" })
            {
                ModelHelper.RequiredAttribute(xml, attr, typeof(Fixture));
            }

            Name = xml.Attribute("name").Value;

            Systems = systems;
            Operation = xml.Attribute("operation").Value;

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "iterations", typeof(LoadFixture));
            Iterations = xml.Element(ModelHelper.SI + "iterations").Value;

            foreach (var m in xml.Elements(ModelHelper.SI + "memo"))
            {
                Memos.Add(new KeyValuePair<string, string>(m.Attribute("name").Value, m.Value));
            }

            Fixtures = new List<object>();
            foreach (var f in xml.Elements())
            {
                if (f.Name == ModelHelper.SI + "condition")
                {
                    Fixtures.Add(f.Value);
                }
                else if (f.Name == ModelHelper.SI + "fixture")
                {
                    ModelHelper.RequiredAttribute(f, ModelHelper.XSI + "type", typeof(IFixture));
                    var kind = f.Attribute(ModelHelper.XSI + "type").Value;
                    if (kind.Contains(':'))
                    {
                        kind = kind.Split(':')[1];
                    }

                    if (!ProviderTypes.Classes.ContainsKey(kind.ToLower()))
                    {
                        throw new XmlParseException(String.Format("Error parsing system. Class {0} not loaded.", kind), f, typeof(Fixture));
                    }

                    var con = ProviderTypes.Classes[kind.ToLower()].GetConstructor(new Type[] { });
                    var add = (IFixture)con.Invoke(new object[] { });
                    add.Path = Path;
                    add.Initialize(f, systems);
                    Fixtures.Add(add);
                }
            }
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "fixture",
                new XAttribute(ModelHelper.XSI + "type", this.GetType().Name),
                new XAttribute("name", Name),
                new XAttribute("operation", Operation),
                new XAttribute("system", "N/A"),
                new XElement("iterations", Iterations),
                from m in Memos
                select new XElement(ModelHelper.SI + "memo", new XAttribute("name", m.Key), m.Value),
                from f in Fixtures
                select f is string ? new XElement(ModelHelper.SI + "condition", f) : ((IFixture)f).GetXml());
        }
    }
}
