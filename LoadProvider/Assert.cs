﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Newtonsoft.Json;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.LoadProvider
{
    public class AssertCondition
    {
        public string Condition { get; set; }

        public List<IAssertion> Asserts { get; set; }
    }

    public class LoadAssert : IAssertion
    {
        public string Name { get; set; }
        public string Iterations { get; set; }
        [JsonIgnore]
        public SyncSystem System { get; set; }
        public string Operation { get; set; }
        public List<KeyValuePair<string, string>> Memos { get; set; }

        public List<AssertCondition> Conditions { get; set; }
        
        [JsonIgnore]
        public string Path { get; set; }
        public event ProgressEvent ReportProgress = delegate { };

        public void UnbindEvents()
        {
            ReportProgress = delegate { };
        }

        [JsonIgnore]
        public static int CurrentIteration { get { return LoadFixture.currentIteration; } }
        [JsonIgnore]
        public static int TotalIterations { get { return LoadFixture.totalIterations; } }

        public bool Test()
        {
            bool passed = true;
            LoadFixture.totalIterations = int.Parse(Fixture.HandleVal(Iterations).ToString());
            LoadFixture.currentFixture = this.Name;

            Dictionary<string, List<Tuple<string, int>>> Failures = new Dictionary<string, List<Tuple<string, int>>>();

            List<string> assertFailures = new List<string>();
            for (int i = 0; i < LoadFixture.totalIterations; i++)
            {
                LoadFixture.currentIteration = i;
                foreach (var memo in Memos)
                {
                    Evaluate.MemoValues[memo.Key] = Fixture.HandleVal(memo.Value);
                }

                foreach (var condition in Conditions)
                {
                    var cval = new AttributeValue(Fixture.HandleVal(condition.Condition));
                    if (String.IsNullOrEmpty(condition.Condition) || new AttributeValue(Fixture.HandleVal(condition.Condition)).BooleanValue)
                    {
                        foreach (var a in condition.Asserts)
                        {
                            var name = Fixture.HandleVal(a.Name).ToString();
                            try
                            {
                                if (!a.Test())
                                {
                                    passed = false;

                                    if (!Failures.ContainsKey(name))
                                    {
                                        Failures[name] = new List<Tuple<string, int>>();
                                    }

                                    Failures[name].Add(new Tuple<string, int>("", i));

                                    ReportProgress.Invoke(a, new ProgressEventArgs(MessageLevel.Error, "Failed {0} on iteration {1}", name, i + 1));
                                }
                                else
                                {
                                    ReportProgress.Invoke(a, new ProgressEventArgs("Passed {0} on iteration {1}", name, i + 1));
                                }
                            }
                            catch (Exception e)
                            {
                                passed = false;

                                if (!Failures.ContainsKey(name))
                                {
                                    Failures[name] = new List<Tuple<string, int>>();
                                }

                                Failures[name].Add(new Tuple<string, int>(e.Message, i));

                                ReportProgress.Invoke(a, new ProgressEventArgs(MessageLevel.Error, "Failed {0} on iteration {1}, error {2}", Fixture.HandleVal(a.Name), i + 1, e.Message));
                            }
                        }
                    }
                }
            }

            if (Failures.Count > 0)
            {
                // construct error
                var errs = new List<string>();
                foreach (var fail in Failures)
                {
                    if (fail.Value.Count > 2)
                    {
                        errs.Add(String.Format("{0} failed {1} iterations.", fail.Key, fail.Value.Count));
                    }
                    else
                    {
                        errs.Add(String.Join(",\r\n ", from pair in fail.Value
                                                   select String.Format("{0} failed iteration {1}", fail.Key, pair.Item2)));
                    }
                }

                throw new Exception("Load Asserts Failures: " + String.Join(", ", errs));
            }

            return passed;
        }

        IAssertion AddAssert(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            ModelHelper.RequiredAttribute(xml, ModelHelper.XSI + "type", typeof(IFixture));
            var kind = xml.Attribute(ModelHelper.XSI + "type").Value;
            if (kind.Contains(':'))
            {
                kind = kind.Split(':')[1];
            }

            if (!ProviderTypes.Classes.ContainsKey(kind.ToLower()))
            {
                throw new XmlParseException(String.Format("Error parsing system. Class {0} not loaded.", kind), xml, typeof(Fixture));
            }

            var con = ProviderTypes.Classes[kind.ToLower()].GetConstructor(new Type[] { });
            var add = (IAssertion)con.Invoke(new object[] { });
            add.Initialize(xml, systems);
            return add;
        }

        public void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            Memos = new List<KeyValuePair<string, string>>();
            foreach (var attr in new XName[] { "name", ModelHelper.XSI + "type", "operation" })
            {
                ModelHelper.RequiredAttribute(xml, attr, typeof(Fixture));
            }

            Name = xml.Attribute("name").Value;

            LoadFixture.Systems = systems;
            Operation = xml.Attribute("operation").Value;

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "iterations", typeof(LoadFixture));
            Iterations = xml.Element(ModelHelper.SI + "iterations").Value;

            foreach (var m in xml.Elements(ModelHelper.SI + "memo"))
            {
                Memos.Add(new KeyValuePair<string, string>(m.Attribute("name").Value, m.Value));
            }

            Conditions = new List<AssertCondition>();
            var baseGroup = new AssertCondition { Condition = "true", Asserts = new List<IAssertion>() };
            foreach (var el in xml.Elements())
            {
                if (el.Name == ModelHelper.SI + "group")
                {
                    var add = new AssertCondition
                    {
                        Condition = el.Attributes("condition").Count() > 0 ? el.Attribute("condition").Value : null,
                        Asserts = (from a in el.Elements(ModelHelper.SI + "assert")
                                   select AddAssert(a, systems)).ToList()
                    };

                    Conditions.Add(add);
                }
                else if (el.Name == ModelHelper.SI + "assert")
                {
                    baseGroup.Asserts.Add(AddAssert(el, systems));
                }
            }

            if (baseGroup.Asserts.Count > 0)
            {
                Conditions.Insert(0, baseGroup);
            }
        }

        public XElement GetXml()
        {
            var ret = new XElement(
                ModelHelper.SI + "assert",
                new XAttribute(ModelHelper.XSI + "type", this.GetType().Name),
                new XAttribute("name", Name),
                new XAttribute("operation", Operation),
                new XAttribute("system", "N/A"),
                new XElement("iterations", Iterations),
                from m in Memos
                select new XElement(ModelHelper.SI + "memo", new XAttribute("name", m.Key), m.Value)
               );

            foreach (var assertGroup in Conditions)
            {
                if (String.IsNullOrEmpty(assertGroup.Condition))
                {
                    ret.Add(from a in assertGroup.Asserts
                            select a.GetXml());
                }
                else
                {
                    ret.Add(new XElement(
                        ModelHelper.SI + "group",
                        new XAttribute("condition", assertGroup.Condition),
                        from a in assertGroup.Asserts
                        select a.GetXml()));
                }
            }

            return ret;
        }
    }
}
