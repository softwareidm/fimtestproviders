using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LoadProvider")]
[assembly: AssemblyDescription("FIM Test provider for Load Testing")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SoftwareIDM")]
[assembly: AssemblyProduct("LoadProvider")]
[assembly: AssemblyCopyright("Copyright � SoftwareIDM")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7409a65d-52c4-4a8a-bb76-efa75b8556e5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.2.4.237")]
[assembly: AssemblyFileVersion("2.2.4.237")]
