
# Make Portal Baseline

# Copyright SoftwareIDM

# run with $baseLine = $true to export portal config to a new baseline
Param(
	$baseLine=$false,
	[string]$schemaFile="$pwd\schema.xml",
	[string]$policyFile="$pwd\policy.xml",
	[string]$portalFile="$pwd\portal.xml"
)

# ensure PSSnapin is included
if ( (Get-PSSnapin -Name FIMAutomation -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin FIMAutomation
}

# function to dump baseline xml files from portal
Function Save-Baseline {
	# Starter for empty build:
	$schema = Export-FIMConfig -SchemaConfig
	$policy = Export-FIMConfig -PolicyConfig
	$portal = Export-FIMConfig -PortalConfig
	$schema | ConvertFrom-FIMResource -file $schemaFile
	$policy | ConvertFrom-FIMResource -file $policyFile
	$portal | ConvertFrom-FIMResource -File $portalFile
}


Write-Host "Exporting baseline portal configuration"
Save-Baseline