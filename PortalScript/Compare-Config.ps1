# Copyright SoftwareIDM 
 
Param(
		[string]$schemaBase="$pwd\mim_schema.xml",
		[string]$policyBase="$pwd\mim_policy.xml",
		[string]$portalBase="$pwd\mim_portal.xml",
		[string]$schemaFile="$pwd\schema.xml",
		[string]$policyFile="$pwd\policy.xml",
		[string]$portalFile="$pwd\portal.xml",
    $doDeletes=$true, # whether to include fixtures for baseline objects that were deleted (must have schema, policy, and portal set to  $true)
    $doPolicy=$true,
    $doSchema=$true,
    $doPortal=$true,
    [string]$URI = "http://localhost:5725/resourcemanagementservice"
)

# Uncomment to hide progress pop-up windows in PowerShell ISE
$ProgressPreference="SilentlyContinue"

# ensure PSSnapin is included
if ( (Get-PSSnapin -Name FIMAutomation -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin FIMAutomation
}
 
# Schema Configuration is defined as the following resource types:
# AttributeTypeDescription, BindingDescription, ObjectTypeDescription, ConstantSpecifier, SchemaSupportedLocales
 
# Policy Configuration is the following:
# ManagementPolicyRule, Set, WorkflowDefinition, EmailTemplate, FilterScope, ActivityInformationConfiguration, Function, SynchronizationRule, SynchronizationFilter
 
# Portal Configuration is:
# HomePageConfiguration, PortalUI, Configuration, NavigationBarConfiguration, SearchScopeConfiguration, Configuration, ObjectVisualizationConfiguration
 
# Helper to get attribute by name from Portal object
Function Get-Attr {
    Param($object, [string]$attr)
    if ($object -is [system.array]) {
        $object = $object[0]
    }
   
    $attrs = $null
    if ($object.ResourceManagementObject -eq $null -and $object.ResourceManagementAttributes -ne $null){
        $attrs = $object.ResourceManagementAttributes
    } else {
        $attrs = $object.ResourceManagementObject.ResourceManagementAttributes
    }
   
    $attrs | Where-Object { $_.AttributeName -eq $attr }
}
 
# Helper to get object display name
Function Get-Disp {
    Param ($object)
    (Get-Attr -object $object -attr "DisplayName").Value
}
 
# Best guess at a unique xpath filter for the object.
Function Get-Filter {
    Param($object)
   
    if ($object.ResourceManagementObject -ne $null) {
        $object = $object.ResourceManagementObject
    }
   
    switch ($object.ObjectType) {
        "BindingDescription" {
            $boundA = (Get-Attr -object $object -attr "BoundAttributeType").Value
            if ($script:idMap.ContainsKey($boundA)) {
                $boundA = $script:idMap[$boundA]
            } else {
                if (!$script:idCache.ContainsKey($boundA)) {
                    $o = Export-FIMConfig -CustomConfig "/*[ObjectID='$($boundA.Replace('urn:uuid:', ''))']" -uri $URI
                    $script:idCache[$o.ResourceManagementObject.ObjectIdentifier] = $o
                }
               
                $boundA = $script:idCache[$boundA]
            }
           
            $boundO = (Get-Attr -object $object -attr "BoundObjectType").Value
            if ($script:idMap.ContainsKey($boundO)) {
                $boundO = $script:idMap[$boundO]
            } else {
                if (!$script:idCache.ContainsKey($boundO)) {
                    $o = Export-FIMConfig -CustomConfig "/*[ObjectID='$($boundO.Replace('urn:uuid:', ''))']" -uri $URI
                    $script:idCache[$o.ResourceManagementObject.ObjectIdentifier] = $o
                }
               
                $boundO = $script:idCache[$boundO]
            }
           
            if ($boundA -eq $null -or $boundO -eq $null) {
                Write-Error "Object reference 'BoundAttributeType' or 'BoundObjectType' not in scope for $(Get-Disp $object)"
            } else {
                "/$($object.ObjectType)[BoundObjectType=$(Get-Filter $boundO) and BoundAttributeType=$(Get-Filter $boundA)]"
            }
        }
                               
        "AttributeTypeDescription" { "/$($object.ObjectType)[Name='$((Get-Attr -object $object -attr "Name").Value)']" }
        default { "/$($object.ObjectType)[DisplayName='$(Get-Disp $object)']" }
    }
}
 
# make a hash map of all the ids in the baseline files
# idMap is used to lookup existing object details, idTouched is used to see if an object was deleted
Function Map-Ids {
    Param($objects, $cache)
    $objects | Foreach-Object {
		$cache[$_.ResourceManagementObject.ObjectIdentifier] = $_
	}
    $objects | Foreach-Object {
		$script:idTouched[$_.ResourceManagementObject.ObjectIdentifier] = $false
	}
}
 
# omit attributes don't get added as <field> or <multifield> in fixtures. They can be left out because they shouldn't be set,
# or because they're part of the core fixture definition
$omit = @("ObjectID", "CreatedTime", "ObjectType", "DisplayName", "Description", "Creator", "DeletedTime", "DetectedRulesList", "ExpectedRulesList", "MVObjectID")
$omitObj = @("ma-data")
 
Function Lookup-Object {
	Param($val)
   
	if (!($val -match "^urn:uuid:")) {
        $val = "urn:uuid:" + $val.ToLower();
	}
   
	$ret = $nil
	if ($script:idMap.ContainsKey($val)) {
	    $ret = $script:idMap[$val]
	} elseif ($script:idCache.ContainsKey($val)) {
		$ret = $script:idCache[$val]
	} else {
		$o = Export-FIMConfig -CustomConfig "/*[ObjectID='$($val.Replace('urn:uuid:', ''))']" -uri $URI
		if ($o) {
		    if ($o -is [system.array]) {
		    	$o = $o[0]
		    }
		    
            $filt = Get-Filter -object $o
            $script:referredObjs[$filt] = $true

		    $script:idCache[$o.ResourceManagementObject.ObjectIdentifier] = $o
		    $ret = $o
		}
	}
   
	$ret
}
 
# Helper returns a value after trying to swap out reference guids for xpath filters
Function Handle-Val {
    Param($val)
    if ($val -match "^urn:uuid:") {
		$o = Lookup-Object $val
		if ($o) {
			$val = "[$(Get-Filter -object $o)]"
		} else {
			$val = "TODO: Not Found [/*[ObjectID=$($val.Replace('urn:uuid:', ''))]]"
		}
    } elseif ($val -match "(?!0{8})\w{8}-\w{4}-\w{4}-\w{4}-\w{12}") {
		
		$matches = [regex]::matches($val, "((?!0{8})\w{8}-\w{4}-\w{4}-\w{4}-\w{12})")
	   
		$mod = $false
		foreach ($m in $matches) {
			$id = $m.Groups[1].Value
			if ($val -eq "{$id}") {
				break
			}
			
			$o = Lookup-Object $id
			if ($o) {
				$mod = $true
				$val = $val.replace($id, "{{$(Get-Filter -object $o)}}")
			}
		}
	   
		if ($mod) {
	        $val = "[$val]"
		}
	}
   
    $val
}
 
# Helper to add an attribute to a fixture
Function Set-Attr {
    Param($attr, $fixture, $xml)
    if ($omit -notcontains $attr.AttributeName) {
        if ($attr.IsMultiValue) {
            $mfield = $xml.CreateElement("multifield", $script:ns)
            $mfield.SetAttribute('name', $attr.AttributeName)
            $mfield.SetAttribute('type', 'Replace')
            foreach ($v in $attr.Values) {
                $val = $xml.CreateElement("value", $script:ns)
                $nil = $val.AppendChild($xml.CreateTextNode((Handle-Val -val $v)))
                $nil = $mfield.AppendChild($val)
            }
           
            $nil = $fixture.AppendChild($mfield)
        } else {
            $field = $xml.CreateElement("field", $script:ns)
            $field.SetAttribute('name', $attr.AttributeName)
            $nil = $field.AppendChild($xml.CreateTextNode((Handle-Val -val $attr.Value))) # Handle reference
            $nil = $fixture.AppendChild($field)
        }
    }
}
 
# Take an object and the baseline version, and construct an updateFixture if it's changed
Function Make-Update {
    Param($changes, $object, $original, $xml, [string]$system="Portal")
    $filt = Get-Filter -object $object
    $fix = $xml.CreateElement("fixture", $script:ns)
    $nil = $fix.SetAttribute('type', 'http://www.w3.org/2001/XMLSchema-instance', 'PortalFixture')
    $fix.SetAttribute('name', "Update $($filt)")
    $fix.SetAttribute('system', $system)
    $fix.SetAttribute('operation', 'Update')
    $f = $xml.CreateElement("filter", $script:ns)
    $nil = $f.AppendChild($xml.CreateTextNode($filt))
    $nil = $fix.AppendChild($f)
   
    $nameCache = ""
    $regex = "[$([char]0)-$([char]31)]" # ascii null to unit separator (matches non-printing characters)
    $changed = $false
    foreach ($c in $changes.Changes) {
        if ($c.attributeName -ne $nameCache) {
            $a = Get-Attr -object $object -attr $c.AttributeName
            $a2 = Get-Attr -object $original -attr $c.AttributeName
 
            if (($a.Value -replace $regex, '') -ne ($a2.Value -replace $regex, '')) {
                $changed = $true
                Set-Attr -attr $a -fixture $fix -xml $xml
            }
        }
       
        $nameCache =  $c.AttributeName
    }
   
    if ($changed) {
		$oType = $object.ResourceManagementObject.ObjectType
		if ($script:exportTable.ContainsKey($oType)) {
			$script:exportTable[$oType] += $fix
		} else {
			$script:exportTable[$oType] = @( $fix )
		}
    }
}
 
Function Make-Add {
    Param($object, $xml, [string]$system="Portal")
    $filt = Get-Filter -object $object
    $fix = $xml.CreateElement("fixture", $script:ns)
    $nil = $fix.SetAttribute('type', 'http://www.w3.org/2001/XMLSchema-instance', 'PortalFixture')
    $fix.SetAttribute('name', "Add $($filt)")
    $fix.SetAttribute('system', $system)
    $fix.SetAttribute('operation', 'Add')
    $add = $xml.CreateElement('add', $script:ns)
   
    $f = $xml.CreateElement('filter', $script:ns)
    $nil = $f.AppendChild($xml.CreateTextNode($filt))
    $nil = $add.AppendChild($f)
   
    $ot = $xml.CreateElement('ObjectType', $script:ns)
    $nil = $ot.AppendChild($xml.CreateTextNode((Get-Attr -object $object -attr 'ObjectType').Value))
    $nil = $add.AppendChild($ot)
   
    $d = $xml.CreateElement('DisplayName', $script:ns)
    $nil = $d.AppendChild($xml.CreateTextNode((Get-Attr -object $object -attr 'DisplayName').Value))
    $nil = $add.AppendChild($d)
   
    if (Get-Attr -object $object -attr 'Description') {
        $de = $xml.CreateElement('Description', $script:ns)
        $nil = $de.AppendChild($xml.CreateTextNode((Get-Attr -object $object -attr 'Description').Value))
        $nil = $add.AppendChild($de)
    }
   
    $nil = $fix.AppendChild($add)
    # fields
    $attrs = $null
    if ($object.ResourceManagementObject -eq $null -and $object.ResourceManagementAttributes -ne $null){
        $attrs = $object.ResourceManagementAttributes
   } else {
        $attrs = $object.ResourceManagementObject.ResourceManagementAttributes
    }
   
    foreach ($attr in $attrs) {
        Set-Attr -attr $attr -fixture $fix -xml $xml
    }
               
	$oType = $object.ResourceManagementObject.ObjectType
	if ($script:exportTable.ContainsKey($oType)) {
		$script:exportTable[$oType] += $fix
	} else {
		$script:exportTable[$oType] = @( $fix )
	}
}
 
Function Handle-Config {
    Param($objects, $xml, [string]$system="Portal")
   
    $script:ns = "http://softwareidm.com/fimtest"
   
    foreach($o in $objects) {
        if ($omitObj -contains $o.ResourceManagementObject.ObjectType) {
            continue
        }

        if ($script:idMap.ContainsKey($o.ResourceManagementObject.ObjectIdentifier)) {
            $script:idTouched[$o.ResourceManagementObject.ObjectIdentifier] = $true
            # Check if different
            $origin = $script:idMap[$o.ResourceManagementObject.ObjectIdentifier]
            $j = Join-FIMConfig -Source $origin -Target $o -DefaultJoin "ObjectID"
            $c = $j | Compare-FIMConfig
            if ($c) {
                Make-Update -changes $c -object $o -original $origin -xml $xml -system $system
            }
        } else {
            Make-Add -object $o -system $system -xml $xml
        }
    }
   
    $null
}
 
Function Handle-Deletes {
    $fixtures = [xml] '<?xml version="1.0" encoding="utf-8" ?>
    <fixtures xmlns="http://softwareidm.com/fimtest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation=".\ .\fixtures.xsd">
    </fixtures>'
   
    foreach($key in $script:idTouched.Keys) {
        if (-not $script:idTouched[$key]) {
            # make Delete
            $object = $script:idMap[$key]
           
            $filt = Get-Filter -object $object
            $fix = $fixtures.CreateElement("fixture", $script:ns)
            $nil = $fix.SetAttribute('type', 'http://www.w3.org/2001/XMLSchema-instance', 'PortalFixture')
            $fix.SetAttribute('name', "Delete $($filt)")
            $fix.SetAttribute('system', $system)
            $fix.SetAttribute('operation', 'Delete')
            $f = $fixtures.CreateElement("filter", $script:ns)
            $nil = $f.AppendChild($fixtures.CreateTextNode($filt))
            $nil = $fix.AppendChild($f)
            $fixtures.LastChild.AppendChild($fix)
        }
    }
               
	$fixtures.Save("$pwd\Fixtures\portal_deletes.xml")
}
 
 
Write-Host "Importing configs from files"

# ID map contains the objects as they are in the baseline comparison
$script:idMap = @{}
# ID touched indicates whether a candidate import object has been looked at yet. If we get to the end, then anything untouched is a delete candidate
$script:idTouched = @{}
# ID cache contains objects that are part of the import set, or that have been retrieved on the fly
$script:idCache = @{}

# fixtures to be exported, grouped by name
$script:exportTable = @{}

# referenced objects that had to be looked up in the target system
$script:referredObjs = @{}
 
$fixtures = [xml] '<?xml version="1.0" encoding="utf-8" ?>
	<fixtures xmlns="http://softwareidm.com/fimtest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation=".\ .\fixtures.xsd">
	</fixtures>'

$cases = [xml] '<?xml version="1.0" encoding="utf-8" ?>
	<tests xmlns="http://softwareidm.com/fimtest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation=".\ ..\xsd\tests.xsd">
      
    </tests>'

if ($doSchema) {
    Write-Host "Processing Schema"
    $schema = ConvertTo-FIMResource -file $schemaBase
    $schema2 = ConvertTo-FIMResource -file $schemaFile
    Map-Ids -objects $schema -cache $script:idMap
	Map-Ids -objects $schema2 -cache $script:idCache
}

if ($doPolicy) {
    Write-Host "Processing Policy"
    $policy = ConvertTo-FIMResource -file $policyBase
    $policy2 = ConvertTo-FIMResource -file $policyFile
    Map-Ids -objects $policy -cache $script:idMap
	Map-Ids -objects $policy2 -cache $script:idCache
}

if ($doPortal) {
    Write-Host "Processing Portal"
    $portal = ConvertTo-FIMResource -file $portalBase
    $portal2 = ConvertTo-FIMResource -file $portalFile
    Map-Ids -objects $portal -cache $script:idMap
	Map-Ids -objects $portal2 -cache $script:idCache
}

if ($doSchema) {
    Write-Host "Comparing schema objects"
    Handle-Config -objects $schema2 -xml $fixtures
}

if ($doPolicy) {
	Write-Host "Comparing policy objects"
    Handle-Config -objects $policy2 -xml $fixtures
}

if ($doPortal) {
	Write-Host "Comparing portal objects"
    Handle-Config -objects $portal2 -xml $fixtures
}

$suite = $cases.CreateElement("suite", $script:ns)
$suite.SetAttribute("name", "Fixture List")

foreach ($key in $script:exportTable.Keys) {
	$set = $script:exportTable[$key]

    $fixes = $cases.CreateElement("fixtures", $script:ns)

	foreach ($fix in $set) {
		$nil = $fixtures.LastChild.AppendChild($fix)
        $ref = $cases.CreateElement("ref", $script:ns)
        $ref.SetAttribute("name", $fix.name)
        $fixes.AppendChild($ref)
	}
               
	$fixtures.Save("$pwd\Fixtures\$($key)_Fixtures.xml")
    $fixtures.fixtures.RemoveAll()

    $case = $cases.CreateElement("test", $script:ns)
    $case.AppendChild($fixes)
    $case.SetAttribute("name", $key)
    $suite.AppendChild($case)
}

$cases.LastChild.AppendChild($suite)
$cases.Save("$pwd\Fixtures\RefList.xml")

$reffed = [xml] '<?xml version="1.0" encoding="utf-8" ?>
	<references xmlns="http://softwareidm.com/fimtest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	</references>'
foreach ($key in $script:referredObjs.Keys) {
    $ref = $reffed.CreateElement("ref", $script:ns)
    $ref.SetAttribute("name", $key)
    $reffed.LastChild.AppendChild($ref)
}

$reffed.Save("$pwd\Fixtures\TODOCheckReferences.xml")

if ($doDeletes) {
	Handle-Deletes
}
