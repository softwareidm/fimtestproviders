﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    #region File Operations

    class TxtRowWrapper : IDictionary<string, AttributeValue>
    {
        Dictionary<string, string> Row { get; set; }
        public TxtRowWrapper(Dictionary<string, string> row)
        {
            Row = row;
        }

        public AttributeValue this[string key]
        {
            get
            {
                var val = Row[key];
                if (val == "")
                {
                    val = null;
                }

                return new AttributeValue(val);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #region Unimplemented

        public void Add(string key, AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(string key)
        {
            throw new NotImplementedException();
        }

        public ICollection<string> Keys
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(string key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(string key, out AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public ICollection<AttributeValue> Values
        {
            get { throw new NotImplementedException(); }
        }

        public void Add(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<string, AttributeValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<string, AttributeValue>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    class TxtFile
    {
        public Dictionary<string, Dictionary<string, string>> data;
        List<Dictionary<string, string>> orderedData;
        TxtProvider provider;
        public TxtFile(TxtProvider provider)
        {
            data = new Dictionary<string, Dictionary<string, string>>();
            orderedData = new List<Dictionary<string, string>>();
            this.provider = provider;
        }

        public void Add(Dictionary<string, string> add)
        {
            if (!data.ContainsKey(add[provider.PK]))
            {
                data.Add(add[provider.PK], add);
                orderedData.Add(add);
            }
            else
            {
                var dat = data[add[provider.PK]];

                foreach (var field in add.Keys)
                {
                    if (field == provider.PK) { continue; }
                    dat[field] = add[field];
                }
            }
        }

        public void Update(Dictionary<string, string> update)
        {
            var key = update[provider.PK];

            foreach (var pair in update)
            {
                if (pair.Key == provider.PK)
                {
                    continue;
                }

                data[key][pair.Key] = pair.Value;
            }
        }

        public void Delete(string pkVal)
        {
            var row = data[pkVal];
            for (int i = 0; i < orderedData.Count; i++)
            {
                if (orderedData[i][provider.PK] == pkVal)
                {
                    orderedData.RemoveAt(i);
                    break;
                }
            }

            data.Remove(pkVal);
        }

        public void Erase()
        {
            orderedData.Clear();
            data.Clear();
        }

        IEnumerable<string> ParseRow(string row, TxtProvider provider)
        {
            char separator = provider.Separator[0];
            StringBuilder accum = new StringBuilder();
            if (provider.HasQuotes)
            {
                bool inQuote = false;
                char quote = row[0];
                for (int i = 0; i < row.Length; i++)
                {
                    if (inQuote)
                    {
                        if (row[i] == quote)
                        {
                            inQuote = false;
                        }
                        else
                        {
                            accum.Append(row[i]);
                        }
                    }
                    else
                    {
                        if (row[i] == quote)
                        {
                            inQuote = true;
                        }
                        else if (row[i] == separator)
                        {
                            yield return accum.ToString();
                            accum = new StringBuilder();
                        }
                    }
                }
            }
            else
            {
                bool inQuote = false;
                char quote = '"';
                accum = new StringBuilder();
                for (int i = 0; i < row.Length; i++)
                {
                    if (row[i] == provider.Separator[0] && (i == 0 || row[i - 1] != '\\') && !inQuote)
                    {
                        yield return accum.ToString();
                        accum = new StringBuilder();
                    }
                    else
                    {
                        if (row[i] == quote) { inQuote = !inQuote; }
                        accum.Append(row[i]);
                    }
                }
            }

            yield return accum.ToString();
        }

        void ParseHeader(string header, TxtProvider provider)
        {
            provider.Fields = Regex.Split(header, @"(?<!\\)" + Regex.Escape(provider.Separator)).ToList();
        }

        public void Parse()
        {
            FileStream stream;

            if (!File.Exists(provider.FilePath))
            {
                stream = File.Create(provider.FilePath);
                stream.Close();
            }

            var text = File.ReadAllText(provider.FilePath);

            var rows = text.Split('\n');
            if (provider.HasHeader)
            {
                ParseHeader(rows[0].TrimEnd(), provider);
            }
            
            var keyIndex = provider.Fields.IndexOf(provider.PK);
            for (int i = provider.HasHeader ? 1 : 0; i < rows.Length; i++)
            {
                var row = rows[i].Trim();
                if (row.Length == 0) { continue; }
                var cols = ParseRow(row, provider).ToList();
                var add = new Dictionary<string, string>();
                var pk = "";
                int j = 0;
                foreach (var f in cols)
                {
                    if (j == keyIndex)
                    {
                        pk = f;
                    }

                    add.Add(provider.Fields[j], f);
                    j++;
                }

                if (!String.IsNullOrEmpty(pk))
                {
                    data.Add(pk, add);
                    orderedData.Add(add);
                }
            }
        }

        public void Save()
        {
            using (var stream = File.Open(provider.FilePath, FileMode.Create))
            {
                var write = new StreamWriter(stream);

                if (provider.HasHeader)
                {
                    write.WriteLine(provider.GetHeader());
                }
                else
                {
                    if (orderedData.Count == 0)
                    {
                        write.WriteLine();
                    }
                }

                if (provider.HasQuotes)
                {
                    foreach (var row in orderedData)
                    {
                        write.WriteLine(String.Join(provider.Separator, (from f in provider.Fields
                                                                         select "\"" + row[f] + "\"").ToArray()));
                    }
                }
                else
                {
                    foreach (var row in orderedData)
                    {
                        write.WriteLine(
                            String.Join(
                                provider.Separator, 
                                (from f in provider.Fields
                                 select row[f].StartsWith("\"") ? row[f] : row[f].Replace(provider.Separator, "\\" + provider.Separator)
                                 ).ToArray()
                            )
                        );
                    }
                }

                write.Flush();
                write.Close();
            }
        }
    }

    #endregion

    public class TxtProvider : IProvider
    {
        public string FilePath { get; set; }
        public string Separator { get; set; }
        public bool HasHeader { get; set; }
        public bool HasQuotes { get; set; }
        public string PK { get; set; }
        public List<string> Fields { get; set; }

        static Dictionary<string, TxtFile> files;

        public void Initialize(XElement xml)
        {
            foreach (var el in new string[] { "path", "separator", "hasHeader", "pk", "fields" })
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + el, typeof(TxtProvider));
            }

            FilePath = xml.Element(ModelHelper.SI + "path").Value;
            Separator = xml.Element(ModelHelper.SI + "separator").Value.Replace("\\t", "\t");
            HasHeader = xml.Element(ModelHelper.SI + "hasHeader").Value == "true";
            HasQuotes = xml.Element(ModelHelper.SI + "hasQuotes").Value == "true";
            PK = xml.Element(ModelHelper.SI + "pk").Value;
            Fields = new List<string>();
            Fields.AddRange(from f in xml.Element(ModelHelper.SI + "fields").Elements(ModelHelper.SI + "field")
                            select f.Value);
            if (!Fields.Contains(PK))
            {
                throw new XmlParseException("Fields must include pk", xml, typeof(TxtProvider));
            }

            if (files == null)
            {
                files = new Dictionary<string, TxtFile>();
            }
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "provider",
                new XAttribute(ModelHelper.XSI + "type", "TxtProvider"),
                new XElement(ModelHelper.SI + "path", FilePath),
                new XElement(ModelHelper.SI + "separator", Separator.Replace("\t", "\\t")),
                new XElement(ModelHelper.SI + "hasHeader", HasHeader),
                new XElement(ModelHelper.SI + "hasQuotes", HasQuotes),
                new XElement(ModelHelper.SI + "pk", PK),
                new XElement(ModelHelper.SI + "fields",
                    from f in Fields
                    select new XElement(ModelHelper.SI + "field", f)));
        }

        public string GetHeader()
        {
            if (HasQuotes)
            {
                return String.Join(Separator, (from f in Fields
                                               select "\"" + f + "\"").ToArray());
            }

            return String.Join(Separator, Fields.ToArray());
        }

        public void WriteFixture(IFixture write)
        {
            if (!files.ContainsKey(FilePath))
            {
                files.Add(FilePath, new TxtFile(this));
                files[FilePath].Parse();
            }

            if (!(write is TxtFixture))
            {
                throw new Exception("TxtProvider must take TxtFixture");
            }

            var fix = (TxtFixture)write;
            var pk = fix.HandleTxtVal(fix.PK).ToString();

            switch (fix.Operation)
            {
                case "Add":
                    var add = new Dictionary<string, string>();
                    add.Add(PK, pk);
                    foreach (string f in Fields)
                    {
                        if (fix.Fields.Where(fi => fi.Key == f).Count() > 0)
                        {
                            var v = fix.HandleTxtVal(fix.GetField(f));
                            add[f] = v == null ? "" : v.ToString();
                        }
                        else if (f != PK)
                        {
                            add[f] = "";
                        }
                    }

                    files[FilePath].Add(add);
                    files[FilePath].Save();
                    break;
                case "Update":
                    var update = new Dictionary<string, string>();
                    update.Add(PK, pk);
                    foreach (var f in fix.Fields)
                    {
                        update.Add(f.Key, fix.HandleTxtVal(f.Value).ToString());
                    }

                    files[FilePath].Update(update);
                    files[FilePath].Save();

                    break;
                case "Delete":
                    files[FilePath].Delete(pk);
                    files[FilePath].Save();
                    break;
                case "Erase":
                    files[FilePath].Erase();
                    files[FilePath].Save();
                    break;
                default:
                    throw new NotImplementedException("This provider does not support this fixture object operation");
            }
        }

        public List<Dictionary<string, object>> DataImage()
        {
            if (!files.ContainsKey(FilePath))
            {
                files.Add(FilePath, new TxtFile(this));
                files[FilePath].Parse();
            }

            var ret = new List<Dictionary<string, object>>();
            foreach (var row in files[FilePath].data.Values)
            {
                var add = new Dictionary<string, object>();
                foreach (var pair in row)
                {
                    add.Add(pair.Key, pair.Value);
                }

                ret.Add(add);
            }

            return ret;
        }

        public bool TestAssert(IAssertion assertion)
        {
            if (!files.ContainsKey(FilePath))
            {
                files.Add(FilePath, new TxtFile(this));
                files[FilePath].Parse();
            }

            if (!(assertion is TxtAssert))
            {
                throw new ArgumentException("ADProvider only accepts ADAssertions");
            }

            var assert = (TxtAssert)assertion;

            bool ret = true;

            switch (assert.Operation)
            {
                case "Exists":
                    ret = files[FilePath].data.ContainsKey(Fixture.HandleVal(assert.PK).ToString());
                    break;
                case "NotHasValue":
                    if (!files[FilePath].data.ContainsKey(Fixture.HandleVal(assert.PK).ToString()))
                    {
                        throw new KeyNotFoundException(String.Format("File {0} does not contain record for {1}.", FilePath, Fixture.HandleVal(assert.PK)));
                    }

                    var nrow = files[FilePath].data[Fixture.HandleVal(assert.PK).ToString()];
                    foreach (var field in assert.Fields)
                    {
                        if (Evaluate.TestAssertVal(field, new TxtRowWrapper(nrow), this))
                        {
                            throw new FormatException(String.Format("Field {0}:{1} matches {2}",
                                field.Field, nrow[field.Field], field.Value));
                        }
                    }

                    break;
                case "HasValue":
                    if (!files[FilePath].data.ContainsKey(assert.PK))
                    {
                        throw new KeyNotFoundException(String.Format("File {0} does not contain record for {1}.", FilePath, Fixture.HandleVal(assert.PK)));
                    }

                    var row = files[FilePath].data[Fixture.HandleVal(assert.PK).ToString()];
                    foreach (var field in assert.Fields)
                    {
                        if (!Evaluate.TestAssertVal(field, new TxtRowWrapper(row), this))
                        {
                            throw new FormatException(String.Format("Field {0}:{1} does not match {2}",
                                field.Field, row[field.Field], field.Value));
                        }
                    }

                    break;
                case "NotExists":
                    ret = !files[FilePath].data.ContainsKey(Fixture.HandleVal(assert.PK).ToString());
                    break;
                default:
                    throw new NotImplementedException("This provider does not support this assertion type");
            }

            return ret;
        }
    }

    public class TxtFixture : Fixture
    {
        public string PK { get; set; }

        public TxtFixture() : base() { }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);
            if (xml.Elements(ModelHelper.SI + "pk").Count() > 0)
            {
                PK = xml.Element(ModelHelper.SI + "pk").Value;
            }
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            if (!String.IsNullOrEmpty(PK))
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "pk", PK));
            }

            return ret;
        }

        public object HandleTxtVal(string val)
        {
            var ret = Fixture.HandleVal(val);
            if (ret == null)
            {
                ret = "";
            }

            return ret.ToString();
        }
    }

    public class TxtAssert : Assertion
    {
        public string PK { get; set; }

        public TxtAssert() : base() { }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "pk", typeof(TxtAssert));
            PK = xml.Element(ModelHelper.SI + "pk").Value;
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            ret.SetElementValue(ModelHelper.SI + "pk", PK);

            return ret;
        }
    }
}
