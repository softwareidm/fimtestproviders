﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    class RowWrapper : IDictionary<string, AttributeValue>
    {
        DbDataReader Reader { get; set; }
        public RowWrapper(DbDataReader reader)
        {
            Reader = reader;
        }

        #region Unimplemented
        public void Add(string key, AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(string key)
        {
            throw new NotImplementedException();
        }

        public ICollection<string> Keys
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(string key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(string key, out AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public ICollection<AttributeValue> Values
        {
            get { throw new NotImplementedException(); }
        }

        public void Add(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<string, AttributeValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<string, AttributeValue>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
        #endregion

        public AttributeValue this[string key]
        {
            get
            {
                var val = Reader[key];
                if (val == DBNull.Value)
                {
                    val = null;
                }

                return new AttributeValue(val);
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public class SQLProvider : IProvider
    {
        public string ConnectionString { get; set; }
        public string DBType { get; set; }
        public string Table { get; set; }
        public string PK { get; set; }
        List<string> PKs { get; set; }
        string Database { get; set; }
        public string SQLServiceAccount { get; set; }

        public SQLProvider()
        {
            DBType = "SqlServer";
        }

        void SetDerivedValues()
        {
            Database = Regex.Match(ConnectionString, @"(?:Initial Catalog|Database)\s?=\s?([^;]+)", RegexOptions.IgnoreCase).Groups[1].Value;

            PKs = new List<string>();
            var key = new StringBuilder();
            bool open = false; // track if inside square braces
            bool start = true; // skip spaces if at start of field
            if (String.IsNullOrEmpty(PK)) // allow null PK for backup/restore fixtures
            {
                return;
            }

            for (int i = 0; i < PK.Length; i++)
            {
                if (PK[i] == '[' && !open)
                {
                    open = true;
                    start = false;
                }
                else if (PK[i] == ']' && (PK.Length == i + 1 || PK[i + 1] != ']') && open)
                {
                    open = false;
                    if (key.Length > 0)
                    {
                        PKs.Add(key.ToString());
                        key = new StringBuilder();
                        start = true;
                    }
                }
                else if (PK[i] == ',' && !open)
                {
                    if (key.Length > 0)
                    {
                        PKs.Add(key.ToString());
                        key = new StringBuilder();
                        start = true;
                    }
                }
                else if (PK[i] != ' ' || !start)
                {
                    start = false;
                    key.Append(PK[i]);
                }
            }

            if (key.Length > 0)
            {
                PKs.Add(key.ToString());
            }
        }

        public void Initialize(XElement xml)
        {
            foreach (var el in new string[] { "connectionString", "table", "pk" })
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + el, typeof(SQLProvider));
            }

            ConnectionString = xml.Element(ModelHelper.SI + "connectionString").Value;
            if (xml.Elements(ModelHelper.SI + "dbType").Count() > 0)
            {
                DBType = xml.Element(ModelHelper.SI + "dbType").Value;
            }
            else
            {
                DBType = "SqlServer";
            }

            Table = xml.Element(ModelHelper.SI + "table").Value;
            PK = xml.Element(ModelHelper.SI + "pk").Value;

            SetDerivedValues();

            if (xml.Elements(ModelHelper.SI + "sqlServiceAccount").Count() > 0)
            {
                SQLServiceAccount = xml.Element(ModelHelper.SI + "sqlServiceAccount").Value;
            }
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "provider",
                new XAttribute(ModelHelper.XSI + "type", "SQLProvider"),
                new XElement(ModelHelper.SI + "connectionString", ConnectionString),
                new XElement(ModelHelper.SI + "dbType", DBType),
                new XElement(ModelHelper.SI + "table", Table),
                new XElement(ModelHelper.SI + "pk", PK));
        }

        string GetTableName()
        {
            if (DBType == "Oracle")
            {
                return Quote.SqlQuote(Table, "\"", "\"");
            }
            else if (DBType == "MySql")
            {
                return Quote.SqlQuote(Table, "`", "`");
            }

            return Quote.SqlQuote(Table, true);
        }

        string ColName(string col)
        {
            if (DBType == "Oracle")
            {
                return Quote.SqlQuote(col, "\"", "\"");
            }
            else if (DBType == "MySql")
            {
                return Quote.SqlQuote(col, "`", "`");
            }

            return Quote.SqlQuote(col);
        }

        public List<Dictionary<string, object>> DataImage()
        {
            var ret = new List<Dictionary<string, object>>();
            if (DBType != "SqlServer")
            {
                throw new ArgumentException("Data Image is only supported for SQL Server");
            }

            var cols = new List<string>();
            foreach (var col in Sql.SqlHelper.Query(ConnectionString, DBType, String.Format("exec sp_columns {0}", GetTableName())))
            {
                cols.Add((string)col["COLUMN_NAME"]);
            }

            foreach (var row in Sql.SqlHelper.Query(ConnectionString, DBType, String.Format("SELECT * FROM {0}", GetTableName())))
            {
                var add = new Dictionary<string, object>();
                foreach (var col in cols)
                {
                    var v = row[col];
                    if (v == DBNull.Value)
                    {
                        v = null;
                    }

                    add.Add(col, v);
                }

                ret.Add(add);
            }

            return ret;
        }

        string buildInsert(SQLFixture fixture, List<SqlParameter> parameters)
        {
            int i = 0;
            var fieldNames = new List<string>();
            var keyParams = new List<string>();

            string query = String.Format("INSERT INTO {0} (", GetTableName());
            if (!String.IsNullOrEmpty(fixture.PK))
            {
                fieldNames.Add(ColName(PK));
                keyParams.Add("@pk");
                parameters.Add(new SqlParameter("@pk", fixture.HandleSqlVal(fixture.PK)));
            }

            foreach (var pair in fixture.Fields)
            {
                var add = fixture.HandleSqlVal(pair.Value);
                if (add == null)
                {
                    add = DBNull.Value;
                }

                parameters.Add(new SqlParameter("@" + i.ToString(), add));
                keyParams.Add("@" + i.ToString());
                fieldNames.Add(ColName(pair.Key));
                i++;
            }

            query += String.Join(", ", fieldNames.ToArray());
            query += ") VALUES (";
            query += String.Join(", ", keyParams.ToArray()) + ")";
            
            return query;
        }

        string buildUpdate(SQLFixture fixture, List<SqlParameter> parameters)
        {
            string query = String.Format("UPDATE {0} SET ", GetTableName());
            int i = 0;
            List<string> sets = new List<string>();
            foreach (var pair in fixture.Fields)
            {
                if (PKs.Contains(pair.Key))
                {
                    continue;
                }

                var up = fixture.HandleSqlVal(pair.Value);
                if (up == null)
                {
                    up = DBNull.Value;
                }

                parameters.Add(new SqlParameter("@" + i.ToString(), up));
                sets.Add(String.Format("{0}=@{1}", ColName(pair.Key), i.ToString()));
                i++;
            }

            query += String.Join(", ", sets.ToArray());
            if (!String.IsNullOrEmpty(fixture.PK))
            {
                parameters.Add(new SqlParameter("@pk", fixture.HandleSqlVal(fixture.PK)));
                query += String.Format(" WHERE {0}=@pk", ColName(PK));
            }
            else
            {
                query += " WHERE ";
                var wheres = new List<string>();
                for (int j = 0; j < PKs.Count; j++)
                {
                    parameters.Add(new SqlParameter("@" + (j + i).ToString(), fixture.HandleSqlVal(fixture.GetField(PKs[j]))));
                    wheres.Add(String.Format("{0}=@{1}", ColName(PKs[j]), j + i));
                }

                query += String.Join(" AND ", wheres);
            }

            return query;
        }

        string buildDelete(SQLFixture fixture, List<SqlParameter> parameters)
        {
            string query = String.Format("DELETE FROM {0} WHERE ", GetTableName());
            if (!String.IsNullOrEmpty(fixture.PK))
            {
                query += String.Format("{0}=@key", ColName(PK));
                parameters.Add(new SqlParameter("@key", fixture.PK));
            }
            else
            {
                var wheres = new List<string>();
                for (int i = 0; i < PKs.Count; i++)
                {
                    parameters.Add(new SqlParameter("@" + i.ToString(), fixture.HandleSqlVal(fixture.GetField(PKs[i]))));
                    wheres.Add(String.Format("{0}=@{1}", ColName(PKs[i]), i));
                }

                query += String.Join(" AND ", wheres);
            }

            return query;
        }

        string buildErase()
        {
            return String.Format("TRUNCATE TABLE {0}", GetTableName());
        }

        bool EnsureRight(string path, FileSystemRights right, bool folder)
        {
            if (!String.IsNullOrEmpty(SQLServiceAccount))
            {
                AuthorizationRuleCollection rules;
                DirectorySecurity dirSecurity = null;
                FileSecurity fileSecurity = null;
                DirectoryInfo dirInfo = null;
                FileInfo fileInfo = null;
                if (folder)
                {
                    var dir = Regex.Match(path, @"^(.*)\\[^\\]+$").Groups[1].Value;
                    dirInfo = new DirectoryInfo(dir);
                    dirSecurity = dirInfo.GetAccessControl();
                    rules = dirSecurity.GetAccessRules(true, true, typeof(NTAccount));
                }
                else
                {
                    fileInfo = new FileInfo(path);
                    fileSecurity = fileInfo.GetAccessControl();
                    rules = fileSecurity.GetAccessRules(true, true, typeof(NTAccount));
                }

                bool needs = true;
                foreach (FileSystemAccessRule rule in rules)
                {
                    if (rule.IdentityReference.Value.ToLower().Equals(SQLServiceAccount.ToLower()))
                    {
                        if ((rule.FileSystemRights & right) > 0)
                        {
                            needs = false;
                        }
                    }
                }

                if (needs)
                {
                    if (folder)
                    {
                        dirSecurity.AddAccessRule(new FileSystemAccessRule(new NTAccount(SQLServiceAccount), right, AccessControlType.Allow));
                        dirInfo.SetAccessControl(dirSecurity);
                    }
                    else
                    {
                        fileSecurity.AddAccessRule(new FileSystemAccessRule(new NTAccount(SQLServiceAccount), right, AccessControlType.Allow));
                        fileInfo.SetAccessControl(fileSecurity);
                    }
                    return true;
                }
            }

            return false;
        }

        void RevokeRight(string path, FileSystemRights right, bool folder)
        {
            if (!String.IsNullOrEmpty(SQLServiceAccount))
            {
                DirectorySecurity dirSecurity = null;
                FileSecurity fileSecurity = null;
                AuthorizationRuleCollection rules;
                var dir = Regex.Match(path, @"^(.*)\\[^\\]+$").Groups[0].Value;
                if (folder)
                {
                    dirSecurity = new DirectoryInfo(dir).GetAccessControl();
                    rules = dirSecurity.GetAccessRules(true, true, typeof(NTAccount));
                }
                else
                {
                    fileSecurity = new FileInfo(path).GetAccessControl();
                    rules = fileSecurity.GetAccessRules(true, true, typeof(NTAccount));
                }

                
                foreach (FileSystemAccessRule rule in rules)
                {
                    if (rule.IdentityReference.Value.ToLower().Equals(SQLServiceAccount.ToLower()))
                    {
                        if ((rule.FileSystemRights & right) > 0)
                        {
                            if (folder)
                            {
                                dirSecurity.RemoveAccessRule(rule);
                                new DirectoryInfo(dir).SetAccessControl(dirSecurity);
                            }
                            else
                            {
                                fileSecurity.RemoveAccessRule(rule);
                                new FileInfo(path).SetAccessControl(fileSecurity);
                            }
                            return;
                        }
                    }
                }
            }
        }

        bool PrepareBackupFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            if (File.Exists(path + ".txt"))
            {
                File.Delete(path + ".txt");
            }

            return EnsureRight(path, FileSystemRights.Write, true);
        }

        void doBackup(string path, string connectionString)
        {
            if (DBType != "SqlServer")
            {
                throw new ArgumentException("Backup and Restore is only supported for SQL Server");
            }

            bool changedPermission = PrepareBackupFile(path);

            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    string query = String.Format("BACKUP DATABASE {0} TO DISK = @fileName WITH RETAINDAYS=0", Quote.SqlQuote(Database, true));
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@fileName", path);
                    cmd.CommandTimeout = 60 * 5;
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException s)
                {
                    if (changedPermission)
                    {
                        RevokeRight(path, FileSystemRights.Write, false);
                    }

                    throw s;
                }
            }

            File.WriteAllText(path + ".txt", String.Format("Backup Created {0}", DateTime.Now));

            if (changedPermission)
            {
                RevokeRight(path, FileSystemRights.Write, true);
            }
        }

        void doRestore(string path, string connectionString)
        {
            if (DBType != "SqlServer")
            {
                throw new ArgumentException("Backup and Restore is only supported for SQL Server");
            }

            if (!File.Exists(path + ".txt"))
            {
                throw new FileNotFoundException(String.Format("Error: Tried to load backup from path {0} that does not exist.", path));
            }

            bool changedPermission = EnsureRight(path, FileSystemRights.Read, false);

            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    var cmd = new SqlCommand("SELECT is_broker_enabled FROM sys.databases WHERE name=@db", conn);
                    cmd.Parameters.AddWithValue("@db", Database);
                    bool isBrokerEnabled = (bool)cmd.ExecuteScalar();
                    
                    cmd = new SqlCommand(String.Format(@"ALTER DATABASE {0}
                                             SET OFFLINE
                                             WITH ROLLBACK IMMEDIATE", Quote.SqlQuote(Database, true)), conn);
                    cmd.ExecuteNonQuery();

                    string query = String.Format("RESTORE DATABASE {0} FROM DISK = @fileName WITH REPLACE", Quote.SqlQuote(Database, true));
                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@fileName", path);
                    cmd.CommandTimeout = 60 * 5;
                    cmd.ExecuteNonQuery();

                    cmd = new SqlCommand(String.Format("ALTER DATABASE {0} SET ONLINE", Quote.SqlQuote(Database, true)), conn);
                    if (isBrokerEnabled)
                    {
                        cmd = new SqlCommand(String.Format("ALTER DATABASE {0} SET ENABLE_BROKER", Quote.SqlQuote(Database, true)), conn);
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (SqlException s)
                {
                    if (changedPermission)
                    {
                        RevokeRight(path, FileSystemRights.Read, false);
                    }

                    throw s;
                }
            }

            if (changedPermission)
            {
                RevokeRight(path, FileSystemRights.Read, false);
            }
        }

        public JObject TableSchema()
        {
            if (DBType != "SqlServer")
            {
                throw new ArgumentException("Table Schema is only supported for SQL Server");
            }

            if (PKs == null)
            {
                SetDerivedValues();
            }

            var ret = new JArray();

            foreach (var row in Sql.SqlHelper.Query(ConnectionString, DBType, String.Format("exec sp_columns {0}", Table)))
            {
                ret.Add(new JObject(
                        new JProperty("column", row["COLUMN_NAME"].ToString()),
                        new JProperty("type", row["TYPE_NAME"].ToString()),
                        new JProperty("nullable", row["IS_NULLABLE"].ToString() == "YES")));
            }

            return new JObject(new JProperty("schema", ret));
        }

        public void WriteFixture(IFixture write)
        {
            if (PKs == null)
            {
                SetDerivedValues();
            }

            if (!(write is SQLFixture))
            {
                throw new ArgumentException("SQLProvider must take SQLFixture");
            }

            SQLFixture fix = (SQLFixture)write;

            string cmd = null;
            var parameters = new List<SqlParameter>();
            switch (fix.Operation)
            {
                case "Add":
                    SQLAssert assert;
                    if (!String.IsNullOrEmpty(fix.PK))
                    {
                        assert = new SQLAssert
                        {
                            Operation = "Exists",
                            PK = fix.PK
                        };
                    }
                    else
                    {
                        var fields = new List<AssertField>();
                        foreach (var key in PKs)
                        {
                            fields.Add(new AssertField { Type = AssertFieldType.Value, Value = fix.GetField(key), Field = key });
                        }

                        assert = new SQLAssert
                        {
                            Operation = "Exists",
                            Fields = fields
                        };
                    }

                    if (TestAssert(assert))
                    {
                        cmd = buildUpdate(fix, parameters);
                    }
                    else
                    {
                        cmd = buildInsert(fix, parameters);
                    }
                    break;
                case "Update":
                    cmd = buildUpdate(fix, parameters);
                    break;
                case "Delete":
                    cmd = buildDelete(fix, parameters);
                    break;
                case "Erase":
                    cmd = buildErase();
                    break;
                case "Backup":
                    doBackup(fix.FilePath, ConnectionString.Replace(Database, "master"));
                    return;
                case "Restore":
                    doRestore(fix.FilePath, ConnectionString.Replace(Database, "master"));
                    return;
                default:
                    throw new NotImplementedException(String.Format("This provider does not support fixture operation {0}", fix.Operation));
            }

            if (!String.IsNullOrEmpty(cmd))
            {
                Sql.SqlHelper.NonQuery(ConnectionString, DBType, cmd, parameters.ToArray());
            }
        }

        public bool TestAssert(IAssertion assertion)
        {
            if (PKs == null)
            {
                SetDerivedValues();
            }

            if (!(assertion is SQLAssert))
            {
                throw new ArgumentException("SQLProvider must take SQLAssertion");
            }

            var assert = (SQLAssert)assertion;

            string cmd = null;
            var parameters = new List<SqlParameter>();
            
                string query = String.Format("SELECT -SEL- FROM {0} WHERE ", GetTableName());
                if (!String.IsNullOrEmpty(assert.PK))
                {
                    query += String.Format("{0}=@key", ColName(PK));
                    parameters.Add(new SqlParameter("@key", Fixture.HandleVal(assert.PK)));
                }
                else if (assert.Operation != "SQL")
                {
                    var wheres = new List<string>();
                    for (int i = 0; i < PKs.Count; i++)
                    {
                        parameters.Add(new SqlParameter("@" + i.ToString(), Fixture.HandleVal(assert.Fields.Where(f => f.Field == PKs[i]).First().Value)));
                        wheres.Add(String.Format("{0}=@{1}", ColName(PKs[i]), i));
                    }

                    query += String.Join(" AND ", wheres);
                }

                switch (assert.Operation)
                {
                    case "Exists":
                        cmd = query.Replace("-SEL-", "COUNT(*)");
                        // SQL returns int, Oracle returns decimal
                        return Sql.SqlHelper.Scalar(ConnectionString, DBType, cmd, parameters.ToArray()).ToString() == "1";
                    case "NotHasValue":
                        cmd = query.Replace("-SEL-", "*");
                        foreach (var row in Sql.SqlHelper.Query(ConnectionString, DBType, cmd, parameters.ToArray()))
                        {
                            foreach (var field in assert.Fields)
                            {
                                if (Evaluate.TestAssertVal(field, new RowWrapper(row), this))
                                {
                                    throw new FormatException(String.Format("Field {0}:{1} matches {2}",
                                        field.Field, row[field.Field], field.Value));
                                }
                            }

                            return true;
                        }

                        throw new Exception(String.Format("No record found matching key"));
                    case "HasValue":
                        cmd = query.Replace("-SEL-", "*");
                        foreach (var row in Sql.SqlHelper.Query(ConnectionString, DBType, cmd, parameters.ToArray()))
                        {
                            foreach (var field in assert.Fields)
                            {
                                if (!Evaluate.TestAssertVal(field, new RowWrapper(row), this))
                                {
                                    throw new FormatException(String.Format("Field {0}:{1} does not match {2}",
                                        field.Field, row[field.Field], field.Value));
                                }
                            }

                            return true;
                        }

                        throw new Exception(String.Format("No record found matching key"));
                    case "NotExists":
                        cmd = query.Replace("-SEL-", "COUNT(*)");
                        return Sql.SqlHelper.Scalar(ConnectionString, DBType, cmd, parameters.ToArray()).ToString() == "0";
                    case "SQL":
                        cmd = assert.SQL;
                        parameters = new List<SqlParameter>();
                        foreach (var field in assert.Fields)
                        {
                            parameters.Add(new SqlParameter(field.Field, field.Value));
                        }

                        var res = Sql.SqlHelper.Scalar(ConnectionString, DBType, cmd, parameters.ToArray());
                        if (res == null || res == DBNull.Value)
                        {
                            return false;
                        }

                        if (Regex.IsMatch(res.ToString(), @"-?\d+"))
                        {
                            return long.Parse(res.ToString()) != 0;
                        }

                        return Regex.IsMatch(res.ToString(), @"true|T|Y|1", RegexOptions.IgnoreCase);
                    default:
                        return false;
                }
            }
        
    }

    public class SQLAssert : Assertion
    {
        public string PK { get; set; }
        public string SQL { get; set; }

        public SQLAssert() : base() { }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);
            if (xml.Elements(ModelHelper.SI + "pk").Count() > 0)
            {
                PK = xml.Element(ModelHelper.SI + "pk").Value;
            }
            else if (xml.Elements(ModelHelper.SI + "sql").Count() > 0)
            {
                SQL = xml.Element(ModelHelper.SI + "sql").Value;
            }
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            if (!String.IsNullOrEmpty(PK))
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "pk", PK));
            }

            if (!String.IsNullOrEmpty(SQL))
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "sql", SQL));
            }

            return ret;
        }
    }

    public class SQLFixture : Fixture
    {
        public string FilePath { get; set; } // used for backup/restore
        public string PK { get; set; }

        public object HandleSqlVal(string val)
        {
            var ret = Fixture.HandleVal(val);
            if (ret == null)
            {
                ret = DBNull.Value;
            }

            return ret;
        }

        public SQLFixture() : base() { }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);
            if (xml.Elements(ModelHelper.SI + "pk").Count() > 0)
            {
                PK = xml.Element(ModelHelper.SI + "pk").Value;
            }

            if (xml.Elements(ModelHelper.SI + "path").Count() > 0)
            {
                FilePath = xml.Element(ModelHelper.SI + "path").Value;
            }
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            if (!String.IsNullOrEmpty(PK))
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "pk", PK));
            }

            if (!String.IsNullOrEmpty(FilePath))
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "path", FilePath));
            }

            return ret;
        }
    }

    #region helpers

    static class Quote
    {
        public static string SqlQuote(string val, string left, string right)
        {
            val = val.Replace(right.ToString(), right + right);
            if (val.StartsWith(left) && val.EndsWith(right + right))
            {
                return val.Substring(0, val.Length - 1);
            }
            else
            {
                return left + val + right;
            }
        }

        public static string SqlQuote(string val)
        {
            return SqlQuote(val, "[", "]");
        }

        public static string SqlQuote(string val, bool splitdots)
        {
            if (splitdots)
            {
                string[] vals = val.Split('.');
                StringBuilder ret = new StringBuilder();
                for (int i = 0; i < vals.Length; i++)
                {
                    if (vals[i].Length > 0)
                        ret.Append(SqlQuote(vals[i]) + ".");
                    else
                        ret.Append(".");
                }
                return ret.ToString().TrimEnd('.');
            }
            else
            {
                return SqlQuote(val);
            }
        }
    }

    #endregion
}
