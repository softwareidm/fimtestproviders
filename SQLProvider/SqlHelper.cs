﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using MySql.Data.MySqlClient;

namespace SoftwareIDM.FIMTest.Provider.Sql
{
    static class SqlHelper
    {

        public static object Scalar(string connectionString, string dbType, string query, params SqlParameter[] parameters)
        {
            switch (dbType)
            {
                case "Oracle":
                    var pCopy = new List<OracleParameter>();
                    foreach (var p in parameters)
                    {
                        var name = p.ParameterName;
                        var newName = name.Replace("@", ":p");
                        pCopy.Add(new OracleParameter(newName, p.Value));
                        query = query.Replace(name, newName);
                    }

                    using (var conn = new OracleConnection(connectionString))
                    {
                        conn.Open();
                        var cmd = new OracleCommand(query, conn);
                        cmd.Parameters.AddRange(pCopy.ToArray());
                        return cmd.ExecuteScalar();
                    }

                case "MySQL":
                    var myCopy = (from p in parameters
                                  select new MySqlParameter(p.ParameterName, p.Value)).ToArray();
                    using (var conn = new MySqlConnection(connectionString))
                    {
                        conn.Open();
                        var cmd = new MySqlCommand(query, conn);
                        cmd.Parameters.AddRange(myCopy);
                        return cmd.ExecuteScalar();
                    }

                default:
                    using (var conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        var sCmd = new SqlCommand(query, conn);
                        sCmd.Parameters.AddRange(parameters);
                        return sCmd.ExecuteScalar();
                    }
            }
        }

        public static IEnumerable<DbDataReader> Query(string connectionString, string dbType, string query, params SqlParameter[] parameters)
        {
            switch (dbType)
            {
                case "Oracle":
                    var pCopy = new List<OracleParameter>();
                    foreach (var p in parameters)
                    {
                        var name = p.ParameterName;
                        var newName = name.Replace("@", ":p");
                        pCopy.Add(new OracleParameter(newName, p.Value));
                        query = query.Replace(name, newName);
                    }

                    using (var conn = new OracleConnection(connectionString))
                    {
                        conn.Open();
                        var cmd = new OracleCommand(query, conn);
                        cmd.Parameters.AddRange(pCopy.ToArray());
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            yield return reader;
                        }

                        reader.Close();
                    }

                    break;
                case "MySql":
                    var myCopy = (from p in parameters
                                 select new MySqlParameter(p.ParameterName, p.Value)).ToArray();

                    using (var conn = new MySqlConnection(connectionString))
                    {
                        conn.Open();
                        var cmd = new MySqlCommand(query, conn);
                        cmd.Parameters.AddRange(myCopy);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            yield return reader;
                        }

                        reader.Close();
                    }

                    break;
                default:
                    using (var conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        var cmd = new SqlCommand(query, conn);
                        cmd.Parameters.AddRange(parameters);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            yield return reader;
                        }

                        reader.Close();
                    }

                    break;
            }
        }

        public static void NonQuery(string connectionString, string dbType, string query, params SqlParameter[] parameters)
        {
            switch (dbType)
            {
                case "Oracle":
                    var pCopy = new List<OracleParameter>();
                    foreach (var p in parameters)
                    {
                        var name = p.ParameterName;
                        var newName = name.Replace("@", ":p");
                        pCopy.Add(new OracleParameter(newName, p.Value));
                        query = query.Replace(name, newName);
                    }

                    using (var conn = new OracleConnection(connectionString))
                    {
                        conn.Open();
                        var cmd = new OracleCommand(query, conn);
                        cmd.Parameters.AddRange(pCopy.ToArray());
                        cmd.ExecuteNonQuery();
                    }

                    break;
                case "MySql":
                    var myCopy = (from p in parameters
                                  select new MySqlParameter(p.ParameterName, p.Value)).ToArray();
                    using (var conn = new MySqlConnection(connectionString))
                    {
                        conn.Open();
                        var cmd = new MySqlCommand(query, conn);
                        cmd.Parameters.AddRange(myCopy);
                        cmd.ExecuteNonQuery();
                    }

                    break;
                default:
                    using (var conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.Parameters.AddRange(parameters);
                        cmd.ExecuteNonQuery();
                    }

                    break;
            }
        }
    }

    /// <summary>
    /// Class to escape SQL table and column names. Based on documentation for TSQL QUOTENAME function
    /// </summary>
    static class SqlQuote
    {
        /// <summary>
        /// Quote val with square braces as column name
        /// </summary>
        public static string quote(string val)
        {
            val = val.Replace("]", "]]");
            if (val.StartsWith("[") && val.EndsWith("]]"))
                return val.Substring(0, val.Length - 1);
            else
                return "[" + val + "]";
        }

        /// <summary>
        /// Quote val with square braces. 
        /// If splitdots is true, each part of the '.' separated string will be treated as a separate 
        /// part of the identifier path.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="splitdots"></param>
        /// <returns></returns>
        public static string quote(string val, bool splitdots)
        {
            if (splitdots)
            {
                string[] vals = val.Split('.');
                StringBuilder ret = new StringBuilder();
                for (int i = 0; i < vals.Length; i++)
                {
                    if (vals[i].Length > 0)
                        ret.Append(quote(vals[i]) + ".");
                    else
                        ret.Append(".");
                }
                return ret.ToString().TrimEnd('.');
            }
            else
            {
                return quote(val);
            }
        }
    }
}
