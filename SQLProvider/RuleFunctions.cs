﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    public class SqlRuleFunctions : IRuleFunctions
    {
        public static AttributeValue SqlScalar(AttributeValue connectionString, AttributeValue query, IDictionary<string, AttributeValue> parameters)
        {
            var connS = connectionString.StringValue;

            if (ProviderTypes.Systems.ContainsKey(connS) && ProviderTypes.Systems[connS].Provider is SQLProvider)
            {
                connS = ((SQLProvider)ProviderTypes.Systems[connS].Provider).ConnectionString;
            }
            
            if (parameters == null)
            {
                parameters = new Dictionary<string, AttributeValue>();
            }

            try
            {
                var res = Sql.SqlHelper.Scalar(
                    connS,
                    "Sql",
                    query.StringValue,
                    (from p in parameters
                     select new SqlParameter("@" + p.Key, p.Value.StringValue)).ToArray()
                );

                if (res == DBNull.Value)
                {
                    return new AttributeValue(null);
                }

                return new AttributeValue(res);
            }
            catch (SqlException)
            {
                return new AttributeValue(null);
            }
        }
    }
}
