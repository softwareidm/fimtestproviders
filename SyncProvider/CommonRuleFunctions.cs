﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    public class CommonRuleFunctions : IRuleFunctions
    {
        /// <summary>
        /// Invoke bitwise & operator on two integers.
        /// </summary>
        public static AttributeValue BitAnd(AttributeValue mask, AttributeValue flag)
        {
            return new AttributeValue(flag.IntegerValue & mask.IntegerValue);
        }

        /// <summary>
        /// Invoke bitwise | operator on two integers
        /// </summary>
        public static AttributeValue BitOr(AttributeValue mask, AttributeValue flag)
        {
            return new AttributeValue(flag.IntegerValue | mask.IntegerValue);
        }

        /// <summary>
        /// Formats an AttributeValue parseable by the DateTime class using DateTime.ToString(format)
        /// formatting rules.
        /// </summary>
        public static AttributeValue DateTimeFormat(AttributeValue dateTime, AttributeValue format)
        {
            try
            {
                return new AttributeValue(dateTime.DateValue.Value.ToString(format.StringValue));
            }
            catch (FormatException)
            {
                return new AttributeValue();
            }
        }

        public static AttributeValue ToFileTime(AttributeValue dateTime)
        {
            try
            {
                return new AttributeValue(dateTime.DateValue.Value.ToFileTime());
            }
            catch (FormatException)
            {
                return new AttributeValue();
            }
        }

        public static AttributeValue FromFileTime(AttributeValue time)
        {
            return new AttributeValue(DateTime.FromFileTime(time.IntegerValue));
        }

        public static AttributeValue Now()
        {
            return new AttributeValue(DateTime.Now);
        }

        public static AttributeValue FromUtc(AttributeValue date)
        {
            var dt = DateTime.Parse(date.StringValue, null, System.Globalization.DateTimeStyles.AssumeUniversal);
            return new AttributeValue(dt.ToLocalTime());
        }

        public static AttributeValue ToUtc(AttributeValue date)
        {
            return new AttributeValue(date.DateValue.Value.ToUniversalTime());
        }

        public static AttributeValue AddTime(AttributeValue date, AttributeValue timeSpan)
        {
            TimeSpan ts;
            TimeSpan.TryParse(timeSpan.StringValue, out ts);
            return new AttributeValue(date.DateValue.Value.Add(ts));
        }

        public static AttributeValue MaxInt()
        {
            return new AttributeValue(long.MaxValue);
        }

        public static AttributeValue MinInt()
        {
            return new AttributeValue(long.MinValue);
        }

        /// <summary>
        /// Function to perform a conditional branch. Due to lazy binding of AttributeValues, 
        /// if Rule based attributes are passed to IIF, only the Rule on the followed branch gets executed.
        /// This allows rules with side-effects to be safely used with the IIF function.
        /// </summary>
        /// <param name="condition">condition must evaluate or coerce to a BooleanValue</param>
        public static AttributeValue IIF(AttributeValue condition, AttributeValue valueTrue, AttributeValue valueFalse)
        {
            return condition.BooleanValue ? valueTrue : valueFalse;
        }

        /// <summary>
        /// Invokes String.Join with params[]
        /// </summary>
        public static AttributeValue Join(AttributeValue separator, params AttributeValue[] values)
        {
            return new AttributeValue(String.Join(separator.StringValue,
                    (from v in values
                     where !String.IsNullOrEmpty(v.StringValue)
                     select v.StringValue).ToArray()));
        }

        /// <summary>
        /// Returns first element in values that isn't null. Should be used with caution on Boolean and Integer fields,
        /// since it may skip a Boolean that is false, or an Integer that is 0.
        /// </summary>
        public static AttributeValue FirstNotNull(params AttributeValue[] values)
        {
            foreach (var val in values)
            {
                if (val.Value != null)
                {
                    return val;
                }
            }

            return CommonRuleFunctions.Null();
        }

        /// <summary>
        /// Returns a SafeSubstring of numchars from the Left side of the string.
        /// </summary>
        public static AttributeValue Left(AttributeValue str, AttributeValue numchars)
        {
            return new AttributeValue(str.StringValue.SafeSubstring(0, (int)numchars.IntegerValue));
        }

        /// <summary>
        /// Returns a SafeSubstring of numchars from the Right side of the string.
        /// </summary>
        public static AttributeValue Right(AttributeValue str, AttributeValue numchars)
        {
            return new AttributeValue(str.StringValue.SafeSubstring(
                    str.StringValue.Length - (int)numchars.IntegerValue,
                    (int)numchars.IntegerValue));
        }

        /// <summary>
        /// Invokes String.PadLeft(numchars, padCharacter).
        /// Technically padCharacter must be a StringValued AttributeValue, and only index [0] is used.
        /// </summary>
        public static AttributeValue LeftPad(AttributeValue str, AttributeValue numchars, AttributeValue padCharacter)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str :
                new AttributeValue(str.StringValue.PadLeft((int)numchars.IntegerValue, padCharacter.StringValue[0]));
        }

        /// <summary>
        /// Invokes String.PadRight(numchars, padCharacter)
        /// </summary>
        public static AttributeValue RightPad(AttributeValue str, AttributeValue numchars, AttributeValue padCharacter)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str :
                new AttributeValue(str.StringValue.PadRight((int)numchars.IntegerValue, padCharacter.StringValue[0]));
        }

        /// <summary>
        /// Proxy for String.Length.
        /// </summary>
        public static AttributeValue Length(AttributeValue str)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str : new AttributeValue(str.StringValue.Length);
        }

        /// <summary>
        /// Invokes str.Replace(oldValue, newValue)
        /// </summary>
        public static AttributeValue ReplaceString(AttributeValue str, AttributeValue oldValue, AttributeValue newValue)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str :
                new AttributeValue(str.StringValue.Replace(oldValue.StringValue, newValue.StringValue));
        }

        /// <summary>
        /// Mid is a proxy for SafeSubstring that may be used in Rule definitions.
        /// </summary>
        public static AttributeValue Mid(AttributeValue str, AttributeValue startIndex, AttributeValue numchars)
        {
            return new AttributeValue(str.StringValue.SafeSubstring((int)startIndex.IntegerValue, (int)numchars.IntegerValue));
        }

        /// <summary>
        /// Proxy for String.TrimStart()
        /// </summary>
        public static AttributeValue LTrim(AttributeValue str)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str : new AttributeValue(str.StringValue.TrimStart());
        }

        /// <summary>
        /// Proxy for String.TrimEnd()
        /// </summary>
        public static AttributeValue RTrim(AttributeValue str)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str : new AttributeValue(str.StringValue.TrimEnd());
        }

        /// <summary>
        /// Proxy for String.Trim(), handles null strings.
        /// </summary>
        public static AttributeValue Trim(AttributeValue str)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str : new AttributeValue(str.StringValue.Trim());
        }

        /// <summary>
        /// Converts a string to Title Case e.g. "john DOE" -> "John Doe"
        /// </summary>
        public static AttributeValue ProperCase(AttributeValue str)
        {
            if (!String.IsNullOrEmpty(str.StringValue))
            {
                var ti = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo;
                return new AttributeValue(ti.ToTitleCase(str.StringValue.ToLower()));
            }

            return str;
        }

        /// <summary>
        /// Invokes String.ToLower
        /// </summary>
        public static AttributeValue LowerCase(AttributeValue str)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str : new AttributeValue(str.StringValue.ToLower());
        }

        /// <summary>
        /// Invokes String.ToUpper
        /// </summary>
        public static AttributeValue UpperCase(AttributeValue str)
        {
            return String.IsNullOrEmpty(str.StringValue) ?
                str : new AttributeValue(str.StringValue.ToUpper());
        }

        /// <summary>
        /// Throw an Exception with the given message.
        /// </summary>
        public static AttributeValue Exception(AttributeValue message, params AttributeValue[] args)
        {
            throw new Exception(String.Format(message.StringValue, args));
        }

        /// <summary>
        /// Generates a random number
        /// </summary>
        public static AttributeValue RandomNum(AttributeValue minValue, AttributeValue maxValue)
        {
            return new AttributeValue(new Random().Next((int)minValue.IntegerValue, (int)maxValue.IntegerValue));
        }

        /// <summary>
        /// Invokes String.IsNullOrEmpty. Coerces type as needed.
        /// </summary>
        public static AttributeValue IsNullOrEmpty(AttributeValue value)
        {
            bool ret;
            switch (value.DataType)
            {
                case AttributeType.Boolean:
                    ret = !value.BooleanValue;
                    break;
                case AttributeType.Integer:
                    ret = value.IntegerValue == 0;
                    break;
                case AttributeType.Decimal:
                    ret = value.DoubleValue == 0.0;
                    break;
                default:
                    ret = String.IsNullOrEmpty(value.StringValue);
                    break;
            }
            return new AttributeValue(ret);
        }

        /// <summary>
        /// Combines params[] AttributeValue.BooleanValues with boolean && operator.
        /// Performs short-circuit evaluation with lazy binding Rule based attributes.
        /// </summary>
        public static AttributeValue And(params AttributeValue[] values)
        {
            var ret = true;
            foreach (var r in values)
            {
                ret = ret && r.BooleanValue;
                if (!ret)
                    break;
            }
            return new AttributeValue(ret);
        }

        /// <summary>
        /// Combines params[] AttributeValue.BooleanValues with boolean || operator.
        /// Performs short-circuit evaluation with lazy binding Rule based attributes.
        /// </summary>
        public static AttributeValue Or(params AttributeValue[] values)
        {
            var ret = false;
            foreach (var r in values)
            {
                ret = ret || r.BooleanValue;
            }
            return new AttributeValue(ret);
        }

        /// <summary>
        /// Negates a boolean AttributeValue
        /// </summary>
        public static AttributeValue Not(AttributeValue value)
        {
            return new AttributeValue(!value.BooleanValue);
        }

        /// <summary>
        /// Invokes AttributeValue.Equals(compare)
        /// </summary>
        public static AttributeValue Equals(AttributeValue value, AttributeValue compare)
        {
            return new AttributeValue(value.Equals(compare));
        }

        /// <summary>
        /// Returns an AttributeValue object representing the Null value
        /// (or 0, false, [] when type appropriate).
        /// </summary>
        public static AttributeValue Null()
        {
            return new AttributeValue(null);
        }

        /// <summary>
        /// Proxy for String.EndsWith
        /// </summary>
        public static AttributeValue EndsWith(AttributeValue value, AttributeValue compare)
        {
            return value.StringValue == null ?
                new AttributeValue(false) :
                new AttributeValue(value.StringValue.EndsWith(compare.StringValue));
        }

        /// <summary>
        /// Proxy for String.StartsWith
        /// </summary>
        public static AttributeValue StartsWith(AttributeValue value, AttributeValue compare)
        {
            return value.StringValue == null ?
                new AttributeValue(false) :
                new AttributeValue(value.StringValue.StartsWith(compare.StringValue));
        }

        /// <summary>
        /// Proxy for String.Contains
        /// </summary>
        public static AttributeValue Contains(AttributeValue value, AttributeValue compare)
        {
            if (String.IsNullOrEmpty(value.StringValue) || String.IsNullOrEmpty(compare.StringValue))
            {
                return new AttributeValue(false);
            }

            return new AttributeValue(value.StringValue.Contains(compare.StringValue));
        }

        public static AttributeValue RegexIsMatch(AttributeValue input, AttributeValue pattern)
        {
            if (!String.IsNullOrEmpty(input.StringValue) && !String.IsNullOrEmpty(pattern.StringValue))
            {
                return new AttributeValue(Regex.IsMatch(input.StringValue, pattern.StringValue));
            }

            return Null();
        }

        public static AttributeValue RegexMatch(AttributeValue input, AttributeValue pattern)
        {
            if (!String.IsNullOrEmpty(input.StringValue) && !String.IsNullOrEmpty(pattern.StringValue))
            {
                return new AttributeValue(Regex.Match(input.StringValue, pattern.StringValue).Value);
            }

            return Null();
        }
    }

    static class StringExtension
    {
        /// <summary>
        /// SafeSubstring is a helper proxy for String.Substring that performs bounds checking to avoid
        /// index errors. The returned substring may be "", or may be shorter than the length parameter.
        /// SafeSubstring is cannot be used directly by Rules. See Mid.
        /// </summary>
        public static string SafeSubstring(this String str, int start, int length)
        {
            if (str == null || start < 0 || start > str.Length)
            {
                return "";
            }
            int max = str.Length - start;
            if (max < length)
            {
                length = max;
            }
            return str.Substring(start, length);
        }
    }
}
