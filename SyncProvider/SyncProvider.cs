﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Newtonsoft.Json;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    public class SyncProvider : IProvider
    {
        public string Server { get; set; }
        public string ConnectionString { get; set; }

        [JsonIgnore]
        public string SearchPath { get; set; }

        public void WriteFixture(IFixture write)
        {
            var fixture = (SyncFixture)write;
            ManagementObjectSearcher searcher;
            switch (fixture.Operation)
            {
                case "ClearRuns":
                case "ClearPasswordHistory":
                case "ClearPasswordQueue":
                    searcher = new ManagementObjectSearcher(
                        SearchPath,
                        "select * from MIIS_Server");
                    foreach (ManagementObject sv in searcher.Get())
                    {
                        if (fixture.Operation == "ClearPasswordQueue")
                        {
                            sv.InvokeMethod(fixture.Operation, new object[] { }).ToString();
                        }
                        else
                        {
                            var date = fixture.HandleFIMDate(fixture.ClearBefore);
                            
                            sv.InvokeMethod(fixture.Operation, new object[] { (string)date }).ToString();
                        }
                    }

                    break;
                case "StopMA":
                case "RunMA":
                    searcher = new ManagementObjectSearcher(
                        SearchPath,
                        String.Format("select * from MIIS_ManagementAgent where Name='{0}'", fixture.MA));
                    foreach (ManagementObject ma in searcher.Get())
                    {
                        if (fixture.Operation == "StopMA")
                        {
                            ma.InvokeMethod("Stop", new object[] { });
                        }
                        else
                        {
                            ma.InvokeMethod("Execute", new object[] { fixture.RunProfile });
                        }
                    }

                    break;
                case "Password":
                    var csObjects = fixture.Filter.FindCSObjects(this);
                    foreach (ManagementObject cs in csObjects.Get())
                    {
                        if (String.IsNullOrEmpty(fixture.Password.OldPassword))
                        {
                            cs.InvokeMethod("SetPassword", new object[] { 
                                fixture.Password.NewPassword, 
                                fixture.Password.ForceChangeAtLogon,
                                fixture.Password.UnlockAccount,
                                fixture.Password.EnforcePolicy });
                        }
                        else
                        {
                            cs.InvokeMethod("ChangePassword", new object[] { fixture.Password.OldPassword, fixture.Password.NewPassword });
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException(String.Format("This provider does not support fixture operation {0}", fixture.Operation));
            }
        }

        public bool TestAssert(IAssertion assertion)
        {
            var assert = (SyncAssert)assertion;
            IDictionary<string, AttributeValue> wrapper;
            switch (assert.Operation)
            {
                case "HasValue":
                    wrapper = assert.Filter.DataWrapper(this);
                    foreach (var field in assert.Fields)
                    {
                        if (!Evaluate.TestAssertVal(field, wrapper, this, new NullCompare(), new DateUtcCompare()))
                        {
                            throw new FormatException(String.Format("Field {0} does not match {1}",
                                    field.Field, field.Value));
                        }
                    }

                    break;
                case "NotHasValue":
                    wrapper = assert.Filter.DataWrapper(this);
                    foreach (var field in assert.Fields)
                    {
                        if (Evaluate.TestAssertVal(field, wrapper, this, new NullCompare(), new DateUtcCompare()))
                        {
                            throw new FormatException(String.Format("Field {0} matches {1}",
                                    field.Field, field.Value));
                        }
                    }
                    break;
                default:
                    return false;
            }

            return true;
        }

        public void Initialize(XElement xml)
        {
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "connectionString", typeof(SyncProvider));
            ConnectionString = xml.Element(ModelHelper.SI + "connectionString").Value;

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "server", typeof(SyncProvider));
            Server = xml.Element(ModelHelper.SI + "server").Value;

            if (Server == "localhost")
            {
                SearchPath = @"root\MicrosoftIdentityIntegrationServer";
            }
            else
            {
                SearchPath = String.Format(@"\\{0}\root\MicrosoftIdentityIntegrationServer", Server);
            }
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "provider",
                new XAttribute(ModelHelper.XSI + "type", "SyncProvider"),
                new XElement(ModelHelper.SI + "server", Server),
                new XElement(ModelHelper.SI + "connectionString", ConnectionString));
        }
    }

    public class PasswordSet
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public bool ForceChangeAtLogon { get; set; }
        public bool UnlockAccount { get; set; }
        public bool EnforcePolicy { get; set; }

        public PasswordSet() { }

        public PasswordSet(XElement xml)
        {
            NewPassword = xml.Value;
            if (xml.Name.LocalName == "changePassword")
            {
                ModelHelper.RequiredAttribute(xml, "old", typeof(SyncFixture));
                OldPassword = xml.Attribute("old").Value;
            }
            else
            {
                // set password
                if (xml.Attributes("forceChangeAtLogon").Count() > 0)
                {
                    ForceChangeAtLogon = xml.Attribute("forceChangeAtLogon").Value == "true";
                }

                if (xml.Attributes("unlockAccount").Count() > 0)
                {
                    UnlockAccount = xml.Attribute("unlockAccount").Value == "true";
                }

                if (xml.Attributes("enforcePolicy").Count() > 0)
                {
                    EnforcePolicy = xml.Attribute("enforcePolicy").Value == "true";
                }
            }
        }

        public XElement GetXml()
        {
            if (String.IsNullOrEmpty(OldPassword))
            {
                return new XElement(ModelHelper.SI + "setPassword",
                    new XAttribute("forceChangeAtLogon", ForceChangeAtLogon.ToString().ToLower()),
                    new XAttribute("unlockAccount", UnlockAccount.ToString().ToLower()),
                    new XAttribute("enforcePolicy", EnforcePolicy.ToString().ToLower()),
                        NewPassword);
            }
            else
            {
                return new XElement(ModelHelper.SI + "changePassword",
                    new XAttribute("old", OldPassword), NewPassword);
            }
        }
    }

    public class DateUtcCompare : ICompareHandler
    {

        public Regex Pattern
        {
            get { return new Regex(@"^\[today]$"); }
        }

        public bool Test(string compare, AttributeValue value)
        {
            try
            {
                return DateTime.Parse(value.StringValue).Date == DateTime.Now.ToUniversalTime().Date;
            }
            catch
            {
                return false;
            }
        }
    }

    public class DateUtcEscape : IValueHandler
    {

        public Regex Pattern
        {
            get { return new Regex(@"^\[\s?(today|now)([^\]]*)\s?]$"); }
        }

        public object Handle(string value)
        {
            string format = "yyyy-MM-dd HH:mm:ss.fff";

            var match = Pattern.Match(value);
            DateTime ret = DateTime.Today.ToUniversalTime();
            if (match.Groups[1].Value.Contains("now"))
            {
                ret = DateTime.Now.ToUniversalTime();
            }

            var offset = match.Groups[2].Value.Trim();
            if (!String.IsNullOrEmpty(offset))
            {
                if (offset.Contains(":"))
                {
                    ret = ret.Add(TimeSpan.Parse(offset.Replace(" ", "")));
                }
                else
                {
                    offset = offset.Replace(" ", "");
                    if (offset.StartsWith("+"))
                    {
                        offset = offset.Substring(1);
                    }

                    ret = ret.AddDays(double.Parse(offset));
                }
            }

            return ret.ToString(format);
        }
    }

    public class DateUtcCatchAll : IValueHandler
    {

        public Regex Pattern
        {
            get { return new Regex(".*"); }
        }

        public object Handle(string value)
        {
            try
            {
                string format = "yyyy-MM-dd HH:mm:ss.fff";
                return DateTime.Parse(value).ToUniversalTime().ToString(format);
            }
            catch (FormatException)
            {
                throw new FormatException(String.Format("Value {0} could not be parsed to a DateTime", value));
            }
        }
    }

    public class SyncFixture : IFixture
    {
        public string Name { get; set; }
        public SyncSystem System { get; set; }
        public string Operation { get; set; }
        public string Path { get; set; }

        public string ClearBefore { get; set; }
        public string MA { get; set; }
        public string RunProfile { get; set; }
        public CSFilter Filter { get; set; }
        public PasswordSet Password { get; set; }
        public event ProgressEvent ReportProgress = delegate { };

        public void Load()
        {
            ((SyncProvider)System.Provider).WriteFixture(this);
        }

        public virtual void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            foreach (var attr in new XName[] { "name", ModelHelper.XSI + "type", "operation", "system" })
            {
                ModelHelper.RequiredAttribute(xml, attr, typeof(Fixture));
            }

            Name = xml.Attribute("name").Value;
            if (!systems.ContainsKey(xml.Attribute("system").Value))
            {
                throw new XmlParseException("Fixture references undefined system", xml, typeof(SyncFixture));
            }

            System = systems[xml.Attribute("system").Value];
            Operation = xml.Attribute("operation").Value;

            switch (Operation)
            {
                case "ClearPasswordQueue":
                    break;
                case "ClearRuns":
                case "ClearPasswordHistory":
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "clearBefore", typeof(SyncFixture));
                    ClearBefore = xml.Element(ModelHelper.SI + "clearBefore").Value;
                    break;
                case "StopMA":
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "ma", typeof(SyncFixture));
                    MA = xml.Element(ModelHelper.SI + "ma").Value;
                    break;
                case "RunMA":
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "ma", typeof(SyncFixture));
                    MA = xml.Element(ModelHelper.SI + "ma").Value;
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "runProfile", typeof(SyncFixture));
                    RunProfile = xml.Element(ModelHelper.SI + "runProfile").Value;
                    break;
                case "Password":
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", typeof(SyncFixture));
                    Filter = new CSFilter(xml);
                    if (xml.Elements(ModelHelper.SI + "changePassword").Count() > 0)
                    {
                        Password = new PasswordSet(xml.Element(ModelHelper.SI + "changePassword"));
                    }
                    else
                    {
                        ModelHelper.RequiredElement(xml, ModelHelper.SI + "setPassword", typeof(SyncFixture));
                        Password = new PasswordSet(xml.Element(ModelHelper.SI + "setPassword"));
                    }
                    break;
                default:
                    throw new XmlParseException(String.Format("SyncFixture does not support {0} operation", Operation), xml, typeof(SyncFixture));
            }
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "fixture",
                new XAttribute(ModelHelper.XSI + "type", "SyncFixture"),
                new XAttribute("name", Name),
                new XAttribute("system", System.Name),
                new XAttribute("operation", Operation));
            switch (Operation)
            {
                case "ClearRuns":
                case "ClearPasswordHistory":
                    ret.Add(new XElement(ModelHelper.SI + "clearBefore", ClearBefore));
                    break;
                case "StopMA":
                    ret.Add(new XElement(ModelHelper.SI + "ma", MA));
                    break;
                case "RunMA":
                    ret.Add(new XElement(ModelHelper.SI + "ma", MA));
                    ret.Add(new XElement(ModelHelper.SI + "runProfile", RunProfile));
                    break;
                case "Password":
                    ret.Add(Filter.GetXml(), Password.GetXml());
                    break;
                default:
                    break;
            }

            return ret;
        }

        public object HandleFIMDate(string val)
        {
            return Evaluate.FixtureVal(val,
                new DateUtcEscape(),
                new RuleEscape(),
                new DateUtcCatchAll());
        }

        public static object HandleSyncVal(string val)
        {
            return Evaluate.FixtureVal(val,
                new NullEscape(),
                new IntEscape(),
                new DateUtcEscape(), // Convert local to UTC time
                new RuleEscape(),
                new DecimalEscape());
        }
    }
}
