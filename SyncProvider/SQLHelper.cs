﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SoftwareIDM.FIMTest.Provider.Sync
{
    static class SqlHelper
    {
        public static object Scalar(string connectionString, string query, params SqlParameter[] parameters)
        {
            return Scalar(connectionString, query, IsolationLevel.ReadUncommitted, parameters);
        }

        public static object Scalar(string connectionString, string query, IsolationLevel isolationLevel, params SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                var tran = conn.BeginTransaction(isolationLevel);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Transaction = tran;
                cmd.Parameters.AddRange(parameters);
                var ret = cmd.ExecuteScalar();
                tran.Commit();
                return ret;
            }
        }

        // TODO: Check if necessary to foreach
        public static IEnumerable<SqlDataReader> Query(string connectionString, string query, params SqlParameter[] parameters)
        {
            foreach (var ret in Query(connectionString, query, IsolationLevel.ReadUncommitted, parameters))
            {
                yield return ret;
            }
        }

        public static IEnumerable<SqlDataReader> Query(string connectionString, string query, IsolationLevel isolationLevel, params SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                var tran = conn.BeginTransaction(isolationLevel);
                SqlCommand cmd = new SqlCommand(query, conn, tran);
                cmd.Transaction = tran;
                cmd.Parameters.AddRange(parameters);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    yield return reader;
                }

                reader.Close();
                tran.Commit();
            }
        }
    }

    /// <summary>
    /// Class to escape SQL table and column names. Based on documentation for TSQL QUOTENAME function
    /// </summary>
    static class SqlQuote
    {
        /// <summary>
        /// Quote val with square braces as column name
        /// </summary>
        public static string quote(string val)
        {
            val = val.Replace("]", "]]");
            if (val.StartsWith("[") && val.EndsWith("]]"))
                return val.Substring(0, val.Length - 1);
            else
                return "[" + val + "]";
        }

        /// <summary>
        /// Quote val with square braces. 
        /// If splitdots is true, each part of the '.' separated string will be treated as a separate 
        /// part of the identifier path.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="splitdots"></param>
        /// <returns></returns>
        public static string quote(string val, bool splitdots)
        {
            if (splitdots)
            {
                string[] vals = val.Split('.');
                StringBuilder ret = new StringBuilder();
                for (int i = 0; i < vals.Length; i++)
                {
                    if (vals[i].Length > 0)
                        ret.Append(quote(vals[i]) + ".");
                    else
                        ret.Append(".");
                }
                return ret.ToString().TrimEnd('.');
            }
            else
            {
                return quote(val);
            }
        }
    }
}
