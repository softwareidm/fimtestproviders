﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    public class SyncRuleFunctions : IRuleFunctions
    {
        static bool TestErrorFilter(string value, AttributeValue compare)
        {
            if (value == compare.StringValue)
                return true;
            else if (value.ToLower().Trim() == compare.StringValue)
                return true;
            else if (Regex.IsMatch(value, compare.StringValue, RegexOptions.IgnoreCase))
                return true;

            return false;
        }

        public static AttributeValue SyncError(AttributeValue runDetail, IDictionary<string, AttributeValue> parameters)
        {
            var xml = XElement.Parse(runDetail.StringValue);

            XElement step;
            if (parameters.ContainsKey("stepNumber"))
            {
                step = (from s in xml.Descendants("step-details")
                        where s.Attribute("step-number").Value == parameters["stepNumber"].StringValue
                        select s).First();
                parameters.Remove("stepNumber");
            }
            else
            {
                step = xml.Descendants("step-details").First();
            }

            var returnValue = "error-type";
            if (parameters.ContainsKey("returnValue"))
            {
                returnValue = parameters["returnValue"].StringValue;
                parameters.Remove("returnValue");
            }

            var syncErrors = step.Element("synchronization-errors");
            if (syncErrors.Elements().Count() == 0)
            {
                return CommonRuleFunctions.Null();
            }

            var filteredErrors = syncErrors.Elements();

            foreach (var key in parameters.Keys)
            {
                switch (key.ToLower())
                {
                    case "dn":
                        filteredErrors = (from e in filteredErrors
                                         where 
                                            e.Attributes("dn").Count() > 0 && 
                                            TestErrorFilter(e.Attribute("dn").Value, parameters[key])
                                         select e).ToArray();
                        break;
                    case "direction":
                        filteredErrors = (from e in filteredErrors
                                         where e.Name.LocalName.Split('-')[0] == parameters[key].StringValue.ToLower()
                                         select e).ToArray();
                        break;
                    case "errortype":
                        filteredErrors = (from e in filteredErrors
                                         where TestErrorFilter(e.Element("error-type").Value, parameters[key])
                                         select e).ToArray();
                        break;
                    case "algorithmstep":
                    case "extensioncallsite":
                        filteredErrors = (from e in filteredErrors
                                         where 
                                            e.Elements("algorith-step").Count() > 0 && 
                                            TestErrorFilter(e.Element("algorithm-step").Value, parameters[key])
                                         select e).ToArray();
                        break;
                    case "maname":
                        filteredErrors = (from e in filteredErrors
                                         where 
                                            e.Elements("rules-error-info").Count() > 0 &&
                                            e.Element("rules-error-info").Elements("context").Count() > 0 &&
                                            e.Element("rules-error-info").Element("context").Attributes("ma-name").Count() > 0 &&
                                            TestErrorFilter(
                                                e.Element("rules-error-info").Element("context").Attribute("ma-name").Value,
                                                parameters[key])
                                         select e).ToArray();
                        break;
                    case "destattr":
                    case "destattribute":
                        filteredErrors = (from e in filteredErrors
                                         where
                                            e.Elements("rules-error-info").Count() > 0 &&
                                            e.Element("rules-error-info").Elements("context").Count() > 0 &&
                                            e.Element("rules-error-info").Element("context").Elements("attribute-mapping").Count() > 0 &&
                                            e.Element("rules-error-info").Element("context").Element("attribute-mapping").Attributes("dest-attr").Count() > 0 &&
                                            TestErrorFilter(
                                                e.Element("rules-error-info").Element("context").Element("attribute-mapping").Attribute("dest-attr").Value,
                                                parameters[key])
                                         select e).ToArray();
                        break;
                    case "srcattr":
                    case "srcattribute":
                        filteredErrors = (from e in filteredErrors
                                         where
                                            e.Elements("rules-error-info").Count() > 0 &&
                                            e.Element("rules-error-info").Elements("context").Count() > 0 &&
                                            e.Element("rules-error-info").Element("context").Elements("attribute-mapping").Count() > 0 &&
                                            e.Element("rules-error-info").Element("context").Element("attribute-mapping").Elements("scripted-mapping").Count() > 0 &&
                                            (from src in e.Element("rules-error-info").Element("context")
                                                    .Element("attribute-mapping").Element("scripted-mapping")
                                                    .Elements("src-attribute")
                                                where TestErrorFilter(src.Value, parameters[key])
                                                select src.Value).Count() > 0
                                         select e).ToArray();
                        break;
                    case "extensionname":
                        filteredErrors = (from e in filteredErrors
                                         where 
                                            e.Elements("extension-error-info").Count() > 0 &&
                                            e.Element("extension-error-info").Elements("extension-name").Count() > 0 &&
                                            TestErrorFilter(
                                                e.Element("extension-error-info").Element("extension-name").Value,
                                                parameters[key])
                                         select e).ToArray();
                        break;
                    case "scriptcontext":
                    case "flowrulename":
                    case "extensioncontext":
                        filteredErrors = (from e in filteredErrors
                                         where
                                            e.Elements("extension-error-info").Count() > 0 &&
                                            e.Element("extension-error-info").Elements("extension-context").Count() > 0 &&
                                            TestErrorFilter(
                                                e.Element("extension-error-info").Element("extension-context").Value,
                                                parameters[key])
                                         select e).ToArray();
                        break;
                    case "detail":
                    case "traceback":
                    case "callstack":
                        filteredErrors = (from e in filteredErrors
                                         where
                                            e.Elements("extension-error-info").Count() > 0 &&
                                            e.Element("extension-error-info").Elements("call-stack").Count() > 0 &&
                                            TestErrorFilter(
                                                e.Element("extension-error-info").Element("call-stack").Value,
                                                parameters[key])
                                         select e).ToArray();
                        break;
                }
            }

            if (filteredErrors.Count() == 0)
            {
                return CommonRuleFunctions.Null();
            }

            var error = filteredErrors.First();

            switch (returnValue.ToLower().Replace("-", ""))
            {
                case "dateoccurred":
                    return error.Elements("date-occurred").Count() > 0 ?
                        new AttributeValue(DateTime.Parse(error.Element("date-occurred").Value)) :
                        CommonRuleFunctions.Null();
                case "retrycount":
                    return error.Elements("retry-count").Count() > 0 ?
                        new AttributeValue(long.Parse(error.Element("retry-count").Value)) :
                        CommonRuleFunctions.Null();
                case "algorithstep":
                case "extensioncallsite":
                    return error.Elements("algorithm-step").Count() > 0 ?
                        new AttributeValue(error.Element("algorithm-step").Value) :
                        CommonRuleFunctions.Null();
                case "maname":
                    return error.Elements("rules-error-info").Count() > 0 &&
                           error.Element("rules-error-info").Elements("context").Count() > 0 &&
                           error.Element("rules-error-info").Element("context").Attributes("ma-name").Count() > 0 ?
                           new AttributeValue(error.Element("rules-error-info").Element("context").Attribute("ma-name").Value) :
                           CommonRuleFunctions.Null();
                case "destattr":
                case "destattribute":
                    return error.Elements("rules-error-info").Count() > 0 &&
                           error.Element("rules-error-info").Elements("context").Count() > 0 &&
                           error.Element("rules-error-info").Element("context").Elements("attribute-mapping").Count() > 0 &&
                           error.Element("rules-error-info").Element("context").Element("attribute-mapping").Attributes("dest-attr").Count() > 0 ?
                           new AttributeValue(error.Element("rules-error-info").Element("context").Element("attribute-mapping").Attribute("dest-attr").Value) :
                           CommonRuleFunctions.Null();
                case "srcattr":
                case "srcattrs":
                case "srcattribute":
                    return error.Elements("rules-error-info").Count() > 0 &&
                           error.Element("rules-error-info").Elements("context").Count() > 0 &&
                           error.Element("rules-error-info").Element("context").Elements("attribute-mapping").Count() > 0 &&
                           error.Element("rules-error-info").Element("context").Element("attribute-mapping").Elements("scripted-mapping").Count() > 0 ?
                           AttributeValue.MultiValue(
                               from src in error.Element("rules-error-info").Element("context").Element("attribute-mapping").Element("script-mapping").Elements("src-attribute")
                               select src.Value, MultiBehavior.Append) :
                           CommonRuleFunctions.Null();
                case "flowrulename":
                case "extensioncontext":
                case "scriptcontext":
                    return error.Elements("extension-error-info").Count() > 0 &&
                           error.Element("extension-error-info").Elements("extension-context").Count() > 0 ?
                           new AttributeValue(error.Element("extension-error-info").Element("extension-context").Value) :
                           CommonRuleFunctions.Null();
                case "detail":
                case "traceback":
                case "callstack":
                    return error.Elements("extension-error-info").Count() > 0 &&
                        error.Element("extension-error-info").Elements("call-stack").Count() > 0 ?
                        new AttributeValue(error.Element("extension-error-info").Element("call-stack").Value) :
                        CommonRuleFunctions.Null();
                default: //error-type
                    return new AttributeValue(error.Element("error-type").Value);
            }
        }

        public static AttributeValue DiscoveryError(AttributeValue runDetail, IDictionary<string, AttributeValue> parameters)
        {
            var xml = XElement.Parse(runDetail.StringValue);

            XElement step;
            if (parameters.ContainsKey("stepNumber"))
            {
                step = (from s in xml.Descendants("step-details")
                        where s.Attribute("step-number").Value == parameters["stepNumber"].StringValue
                        select s).First();
            }
            else
            {
                step = xml.Descendants("step-details").First();
            }

            var discoverErrors = step.Element("ma-discovery-errors");
            if (discoverErrors.Elements().Count() == 0)
            {
                return CommonRuleFunctions.Null();
            }

            var errorObject = discoverErrors.Element("ma-object-error");
            if (parameters.ContainsKey("filter") && parameters.ContainsKey("regex"))
            {
                var filter = parameters["filter"].StringValue;
                var regex = new Regex(parameters["regex"].StringValue);
                foreach (var el in discoverErrors.Elements("ma-object-error"))
                {
                    foreach (var desc in el.Descendants(filter))
                    {
                        if (regex.IsMatch(desc.Value))
                        {
                            errorObject = el;
                            break;
                        }
                    }
                }
            }

            if (parameters.ContainsKey("returnValue"))
            {
                return new AttributeValue(errorObject.Descendants(parameters["returnValue"].StringValue).First().Value);
            }
            else
            {
                return new AttributeValue(errorObject.Element("error-type").Value);
            }
        }

        static AttributeValue XLate(string value, string type)
        {
            switch (type)
            {
                case "string":
                    return new AttributeValue(value);
                case "binary":
                    return new AttributeValue(Convert.FromBase64String(value));
                case "integer":
                    return new AttributeValue(Convert.ToInt64(value, 16));
                case "boolean":
                    return new AttributeValue(bool.Parse(value));
                case "reference":
                    return new AttributeValue(new Guid(Convert.FromBase64String(value)));
                default: // reference
                    throw new NotImplementedException(String.Format("Unable to parse type {0} from hologram", type));
            }
        }

        public static AttributeValue HoloAttr(AttributeValue hologram, AttributeValue attribute)
        {
            var xml = XElement.Parse(hologram.StringValue);
            switch (attribute.StringValue.ToLower().Replace("-", ""))
            {
                case "dn":
                    return new AttributeValue(xml.Attribute("dn").Value);
                case "anchor":
                    return new AttributeValue(new Guid(Convert.FromBase64String(xml.Element("anchor").Value)));
                case "parentanchor":
                    if (xml.Elements("parent-anchor").Count() > 0)
                    {
                        return new AttributeValue(new Guid(Convert.FromBase64String(xml.Element("parent-anchor").Value)));
                    }
                    else
                    {
                        return CommonRuleFunctions.Null();
                    }
                case "primaryobjectclass":
                    return new AttributeValue(xml.Element("primary-objectclass").Value);
                case "objectclass":
                    var vals = new List<string>();
                    vals.AddRange(from oc in xml.Element("objectclass").Elements("oc-value")
                                  select oc.Value);
                    return AttributeValue.MultiValue(vals, MultiBehavior.Append);
                default:
                    var attrs = (from a in xml.Elements("attr")
                                 where a.Attribute("name").Value.ToLower().Replace("-", "") == attribute.StringValue
                                 select a);
                    if (attrs.Count() == 0)
                    {
                        var dnattrs = (from a in xml.Elements("dn-attr")
                                       where a.Attribute("name").Value.ToLower().Replace("-", "") == attribute.StringValue
                                       select a);
                        if (dnattrs.Count() == 0)
                        {
                            throw new KeyNotFoundException(String.Format("Unable to find attribute {0} in Hologram", attribute.StringValue));
                        }

                        var dnattr = dnattrs.First();
                        if (dnattr.Elements("dn-value").Count() > 1)
                        {
                            return AttributeValue.MultiValue(from v in dnattr.Elements("dn-value")
                                                      select v.Element("dn").Value, MultiBehavior.Append);
                        }
                        else
                        {
                            return new AttributeValue(dnattr.Element("dn-value").Element("dn").Value);
                        }
                    }
                    else
                    {
                        var attr = attrs.First();

                        if (attr.Elements("value").Count() > 1)
                        {
                            var multi = new AttributeValue();
                            multi.Values = new List<AttributeValue>();
                            multi.Values.AddRange(from v in attr.Elements("value")
                                                  select XLate(v.Value, attr.Attribute("type").Value));
                            return multi;
                        }
                        else
                        {
                            return XLate(attr.Element("value").Value, attr.Attribute("type").Value);
                        }
                    }
            }
        }
    }
}
