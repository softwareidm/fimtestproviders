﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Management;
using System.Text;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    public interface ISyncFilter
    {
        IDictionary<string, AttributeValue> DataWrapper(SyncProvider provider);

        XElement GetXml();
    }

    public class ManagementWrapper : IDictionary<string, AttributeValue>
    {
        ManagementObject Data { get; set; }
        List<string> Properties { get; set; }

        public ManagementWrapper(ManagementObject data, List<string> properties)
        {
            Data = data;
            Properties = properties;
        }

        public AttributeValue this[string key]
        {
            get
            {
                if (Properties.Contains(key))
                {
                    var ret = new AttributeValue(Data.GetPropertyValue(key));
                    return ret;
                }
                else
                {
                    var ret = new AttributeValue(Data.InvokeMethod(key, new object[] { }));
                    return ret;
                }
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #region Unimplemented

        public void Add(string key, AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(string key)
        {
            throw new NotImplementedException();
        }

        public ICollection<string> Keys
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(string key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(string key, out AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public ICollection<AttributeValue> Values
        {
            get { throw new NotImplementedException(); }
        }

        public void Add(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<string, AttributeValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<string, AttributeValue>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class MAFilter : ISyncFilter
    {
        public string MA { get; set; }

        public MAFilter() { }

        public MAFilter(XElement xml)
        {
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", typeof(SyncAssert));
            xml = xml.Element(ModelHelper.SI + "filter");
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "ma", typeof(RunFilter));
            MA = xml.Element(ModelHelper.SI + "ma").Value;
        }

        public IDictionary<string, AttributeValue> DataWrapper(SyncProvider provider)
        {
            var searcher = new ManagementObjectSearcher(
                        provider.SearchPath,
                        String.Format("select * from MIIS_ManagementAgent where Name='{0}'", MA));

            foreach (ManagementObject ma in searcher.Get())
            {
                return new ManagementWrapper(ma, new List<string> { "Guid", "Name", "Type" });
            }

            throw new KeyNotFoundException(String.Format("Unable to find MA with Name '{0}'", MA));
        }


        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "filter",
                new XElement(ModelHelper.SI + "ma", MA));
        }
    }

    public class ServerFilter : ISyncFilter
    {
        public ServerFilter() { }

        public ServerFilter(XElement xml) { }

        public IDictionary<string, AttributeValue> DataWrapper(SyncProvider provider)
        {
            var searcher = new ManagementObjectSearcher(
                        provider.SearchPath,
                        "select * from MIIS_Server");
            foreach (ManagementObject sv in searcher.Get())
            {
                return new ManagementWrapper(sv, new List<string> { "Name" });
            }

            throw new KeyNotFoundException("Unable to find FIM WMI");
        }


        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "filter");
        }
    }

    public class RunFilter : ISyncFilter
    {
        public string MA { get; set; }

        public List<Tuple<string, string>> ValueFilters { get; set; }
        public List<Tuple<string, string, string>> TimeFilters { get; set; }

        public RunFilter() { }
        public RunFilter(XElement xml)
        {
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", typeof(SyncAssert));
            xml = xml.Element(ModelHelper.SI + "filter");
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "ma", typeof(RunFilter));
            MA = xml.Element(ModelHelper.SI + "ma").Value;

            ValueFilters = new List<Tuple<string, string>>();
            TimeFilters = new List<Tuple<string, string, string>>();
            foreach (var el in xml.Elements())
            {
                if (el.Name.LocalName == "ma")
                {
                    continue;
                }

                var name = el.Name.LocalName.Substring(0, 1).ToUpper() + el.Name.LocalName.Substring(1);
                if (name == "RunStartTime" || name == "RunEndTime")
                {
                    ModelHelper.RequiredAttribute(el, "relative", typeof(RunFilter));
                    TimeFilters.Add(new Tuple<string, string, string>(name, el.Attribute("relative").Value, el.Value));
                }
                else
                {
                    ValueFilters.Add(new Tuple<string, string>(name, el.Value));
                }
            }
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "filter",
                new XElement(ModelHelper.SI + "ma", MA));
            foreach (var filter in ValueFilters)
            {
                ret.Add(new XElement(ModelHelper.SI + (filter.Item1.Substring(0, 1).ToLower() + filter.Item1.Substring(1)), filter.Item2));
            }

            foreach (var filter in TimeFilters)
            {
                ret.Add(new XElement(ModelHelper.SI + (filter.Item1.Substring(0, 1).ToLower() + filter.Item1.Substring(1)),
                    new XAttribute("relative", filter.Item2), filter.Item3));
            }

            return ret;
        }

        public IDictionary<string, AttributeValue> DataWrapper(SyncProvider provider)
        {
            var clauses = new List<string> { String.Format("MaName='{0}'", MA) };

            foreach (var filter in ValueFilters)
            {
                clauses.Add(String.Format("{0}='{1}'", filter.Item1, SyncFixture.HandleSyncVal(filter.Item2)));
            }

            foreach (var filter in TimeFilters)
            {
                if (filter.Item2 == "After")
                {
                    clauses.Add(String.Format("{0} > '{1}'", filter.Item1, SyncFixture.HandleSyncVal(filter.Item3)));
                }
                else // Before
                {
                    clauses.Add(String.Format("{0} < '{1}'", filter.Item1, SyncFixture.HandleSyncVal(filter.Item3)));
                }
            }

            var query = String.Format("select * from MIIS_RunHistory where {0}", String.Join(" and ", clauses));

            var searcher = new ManagementObjectSearcher(
                        provider.SearchPath,
                        query);
            foreach (ManagementObject hist in searcher.Get())
            {
                return new ManagementWrapper(hist, new List<string> { 
                    "Key", "MaGuid", "MaName", "RunEndTime", "RunProfile", "RunNumber", "RunStartTime", "RunStatus"
                });
            }

            throw new KeyNotFoundException("Unable to find Run History matching filter");
        }
    }

    public class PasswordHistoryFilter : ISyncFilter
    {
        public List<Tuple<string, string, string>> TimeFilters { get; set; }
        public string CSGuid { get; set; }
        public string MA { get; set; }
        public string DN { get; set; }
        public string Type { get; set; }
        public List<KeyValuePair<string, string>> MVFields { get; set; }

        public PasswordHistoryFilter() { }

        public PasswordHistoryFilter(XElement xml)
        {
            MVFields = new List<KeyValuePair<string, string>>();
            TimeFilters = new List<Tuple<string, string, string>>();

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", typeof(SyncAssert));
            xml = xml.Element(ModelHelper.SI + "filter");

            ModelHelper.RequiredAttribute(xml, "type", typeof(SyncAssert));
            Type = xml.Attribute("type").Value;

            foreach (var el in xml.Elements(ModelHelper.SI + "MIISReceiveTime"))
            {
                ModelHelper.RequiredAttribute(el, "relative", typeof(PasswordHistoryFilter));
                TimeFilters.Add(new Tuple<string, string, string>("MIISReceiveTime", el.Attribute("relative").Value, el.Value));
            }

            if (xml.Elements(ModelHelper.SI + "csGuid").Count() > 0)
            {
                CSGuid = xml.Element(ModelHelper.SI + "csGuid").Value;
            }
            else
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "ma", typeof(SyncAssert));
                MA = xml.Element(ModelHelper.SI + "ma").Value;
                if (xml.Elements(ModelHelper.SI + "dn").Count() > 0)
                {
                    DN = xml.Element(ModelHelper.SI + "dn").Value;
                }
                else
                {
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "mvField", typeof(SyncAssert));
                    foreach (var field in xml.Elements(ModelHelper.SI + "mvField"))
                    {
                        ModelHelper.RequiredAttribute(field, "name", typeof(SyncAssert));
                        MVFields.Add(new KeyValuePair<string, string>(field.Attribute("name").Value, field.Value));
                    }
                }
            }
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "filter",
                new XAttribute("type", Type));

            foreach (var filter in TimeFilters)
            {
                ret.Add(new XElement(ModelHelper.SI + "MIISReceiveTime",
                    new XAttribute("relative", filter.Item2),
                    filter.Item3));
            }

            if (String.IsNullOrEmpty(CSGuid))
            {
                ret.Add(new XElement(ModelHelper.SI + "ma", MA));
                if (!String.IsNullOrEmpty(DN))
                {
                    ret.Add(new XElement(ModelHelper.SI + "dn", DN));
                }
                else
                {
                    foreach (var field in MVFields)
                    {
                        ret.Add(new XElement(ModelHelper.SI + "mvField",
                            new XAttribute("name", field.Key),
                            field.Value));
                    }
                }
            }
            else
            {
                ret.Add(new XElement(ModelHelper.SI + "csGuid", CSGuid));
            }

            return ret;
        }

        public IDictionary<string, AttributeValue> DataWrapper(SyncProvider provider)
        {
            var clauses = new List<string>();

            if (!String.IsNullOrEmpty(CSGuid))
            {
                clauses.Add(String.Format("CsGuid='{0}'", CSFilter.EscapeGuid(CSGuid)));
            }
            else if(!String.IsNullOrEmpty(MA))
            {
                if (!String.IsNullOrEmpty(DN) || MVFields.Count > 0)
                {
                    var csFilter = new CSFilter();
                    csFilter.MA = MA;
                    csFilter.DN = DN;
                    csFilter.MVFields = MVFields;

                    var cs = csFilter.DataWrapper(provider);
                    clauses.Add(String.Format("CsGuid='{0}'", CSFilter.EscapeGuid(cs["Guid"].StringValue)));
                }
                else
                {
                    var maFilter = new MAFilter();
                    maFilter.MA = MA;
                    var ma = maFilter.DataWrapper(provider);
                    clauses.Add(string.Format("MaGuid='{0}'", CSFilter.EscapeGuid(ma["Guid"].StringValue)));
                }
            }

            foreach (var filter in TimeFilters)
            {
                if (filter.Item2 == "After")
                {
                    clauses.Add(String.Format("{0} > '{1}'", filter.Item1, SyncFixture.HandleSyncVal(filter.Item3)));
                }
                else // Before
                {
                    clauses.Add(String.Format("{0} < '{1}'", filter.Item1, SyncFixture.HandleSyncVal(filter.Item3)));
                }
            }

            var query = String.Format("select * from MIIS_PasswordChange{0} where {1}", Type, String.Join(" and ", clauses));

            var searcher = new ManagementObjectSearcher(
                        provider.SearchPath,
                        query);
            foreach (ManagementObject hist in searcher.Get())
            {
                return new ManagementWrapper(hist, new List<string> { 
                    "AttemptDetails", "CsGuid", "Dn", "Guid", "LastAttemptReturnCode", "LastAttemptTime", "MaGuid", "MIISReceiveTime", "OriginatingCsGuid",
                    "ReferenceGuid", "RetryCount", "FinalReturnCode", "ReachedRetryLimit", "SourceServer", "SourceChangeTime"
                });
            }

            throw new KeyNotFoundException("Unable to find Password History matching filter");
        }
    }

    public class CSFilter : ISyncFilter
    {
        public string CSGuid { get; set; }
        public string MA { get; set; }
        public string DN { get; set; }
        public List<KeyValuePair<string, string>> MVFields { get; set; }

        public CSFilter() { }
        
        public CSFilter(XElement xml)
        {
            bool isFix = xml.Name.LocalName == "fixture";
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", isFix ? typeof(SyncFixture) : typeof(SyncAssert));
            xml = xml.Element(ModelHelper.SI + "filter");

            if (xml.Elements(ModelHelper.SI + "csGuid").Count() > 0)
            {
                CSGuid = xml.Element(ModelHelper.SI + "csGuid").Value;
            }
            else
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "ma", isFix ? typeof(SyncFixture) : typeof(SyncAssert));
                MA = xml.Element(ModelHelper.SI + "ma").Value;
                if (xml.Elements(ModelHelper.SI + "dn").Count() > 0)
                {
                    DN = xml.Element(ModelHelper.SI + "dn").Value;
                }
                else
                {
                    MVFields = new List<KeyValuePair<string, string>>();
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "mvField", isFix ? typeof(SyncFixture) : typeof(SyncAssert));
                    foreach (var field in xml.Elements(ModelHelper.SI + "mvField"))
                    {
                        ModelHelper.RequiredAttribute(field, "name", isFix ? typeof(SyncFixture) : typeof(SyncAssert));
                        MVFields.Add(new KeyValuePair<string, string>(field.Attribute("name").Value, field.Value));
                    }
                }
            }
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "filter");

            if (String.IsNullOrEmpty(CSGuid))
            {
                ret.Add(new XElement(ModelHelper.SI + "ma", MA));
                if (!String.IsNullOrEmpty(DN))
                {
                    ret.Add(new XElement(ModelHelper.SI + "dn", DN));
                }
                else
                {
                    foreach (var field in MVFields)
                    {
                        ret.Add(new XElement(ModelHelper.SI + "mvField",
                            new XAttribute("name", field.Key),
                            field.Value));
                    }
                }
            }
            else
            {
                ret.Add(new XElement(ModelHelper.SI + "csGuid", CSGuid));
            }

            return ret;
        }

        public static string EscapeGuid(string guid)
        {
            if (!(guid.StartsWith("{") && guid.EndsWith("}")))
            {
                return "{" + guid.ToUpper() + "}";
            }

            return guid.ToUpper();
        }

        public ManagementObjectSearcher FindCSObjects(SyncProvider provider)
        {
            if (!String.IsNullOrEmpty(CSGuid))
            {
                return new ManagementObjectSearcher(
                        provider.SearchPath,
                        String.Format("select * from MIIS_CSObject WHERE Guid='{0}'", EscapeGuid(CSGuid)));
            }
            else
            {
                if (!String.IsNullOrEmpty(DN))
                {
                    var maGuid = (Guid)Sync.SqlHelper.Scalar(provider.ConnectionString, "SELECT ma_id FROM mms_management_agent WHERE ma_name=@name",
                        new SqlParameter("@name", MA));

                    return new ManagementObjectSearcher(
                        provider.SearchPath,
                        String.Format("select * from MIIS_CSObject where MaGuid='{0}' and DN='{1}'",
                        EscapeGuid(maGuid.ToString()), SyncFixture.HandleSyncVal(DN)));
                }
                else
                {
                    var query =
@"SELECT l.cs_object_id FROM
mms_metaverse mv
LEFT JOIN mms_csmv_link l ON mv.object_id=l.mv_object_id
LEFT JOIN mms_lineage_cross_reference ref ON l.lineage_id=ref.lineage_id
LEFT JOIN mms_management_agent ma ON ref.ma_id=ma.ma_id
WHERE ma.ma_name=@ma AND {0}";
                    var clauses = new List<string>();
                    var para = new List<SqlParameter> { new SqlParameter("@ma", SyncFixture.HandleSyncVal(MA)) };

                    int i = 0;
                    foreach (var pair in MVFields)
                    {
                        clauses.Add(String.Format("{0}=@{1}", Sync.SqlQuote.quote("mv." + pair.Key, true), i));
                        para.Add(new SqlParameter("@" + i.ToString(), SyncFixture.HandleSyncVal(pair.Value)));
                        i++;
                    }

                    query = String.Format(query, String.Join(" AND ", clauses));

                    var csGuid = (Guid)Sync.SqlHelper.Scalar(provider.ConnectionString, query, para.ToArray());
                    if (csGuid == null)
                    {
                        throw new KeyNotFoundException(String.Format("Unable to find CS record matching MV Fields query.", query));
                    }

                    return new ManagementObjectSearcher(
                        provider.SearchPath,
                        String.Format("select * from MIIS_CSObject WHERE Guid='{0}'",
                        EscapeGuid(csGuid.ToString())));
                }
            }
        }

        public IDictionary<string, AttributeValue> DataWrapper(SyncProvider provider)
        {
            var searcher = FindCSObjects(provider);
            foreach (ManagementObject cs in searcher.Get())
            {
                return new ManagementWrapper(cs, new List<string> { 
                    "Account", "UnconfirmedExportHologram", "DN", "Domain", "EscrowedExportHologram", "Guid",
                    "Hologram", "MaGuid", "MaName", "MvGuid", "ObjectType", "PartitionGuid", "PasswordChangeHistory", "PartitionName", "PendingImportHologram",
                    "PartitionName", "PartitionDisplayName", "UnappliedExportHologram", "UserPrincipalName"
                });
            }

            throw new KeyNotFoundException("Unable to find CS Object matching filter");
        }
    }

    public class MVWrapper : IDictionary<string, AttributeValue>
    {
        string ConnectionString { get; set; }
        Guid Guid { get; set; }

        public MVWrapper(Guid guid, string connectionString)
        {
            Guid = guid;
            ConnectionString = connectionString;
        }

        bool HasSingleAttribute(string attribute)
        {
            var query = @"SELECT COUNT(COLUMN_NAME)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'mms_metaverse' AND TABLE_SCHEMA='dbo'
AND COLUMN_NAME=@c";

            return (int)Sync.SqlHelper.Scalar(ConnectionString, query, new SqlParameter("@c", attribute)) == 1;
        }

        public AttributeValue this[string key]
        {
            get
            {
                if (HasSingleAttribute(key))
                {
                    var query = String.Format("SELECT {0} FROM mms_metaverse WHERE object_id=@o", Sync.SqlQuote.quote(key));
                    var val = Sync.SqlHelper.Scalar(ConnectionString, query, new SqlParameter("@o", Guid));
                    return new AttributeValue(val);
                }

                throw new KeyNotFoundException(String.Format("Unable to find MV Field {0}", key));
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #region Unimplemented

        public void Add(string key, AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(string key)
        {
            throw new NotImplementedException();
        }

        public ICollection<string> Keys
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(string key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(string key, out AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public ICollection<AttributeValue> Values
        {
            get { throw new NotImplementedException(); }
        }

        public void Add(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<string, AttributeValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<string, AttributeValue>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    
    public class MVFilter : ISyncFilter
    {
        public List<KeyValuePair<string, string>> MVFields { get; set; }

        public MVFilter() { }
        public MVFilter(XElement xml)
        {
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", typeof(SyncAssert));
            xml = xml.Element(ModelHelper.SI + "filter");

            MVFields = new List<KeyValuePair<string, string>>();
            foreach (var field in xml.Elements(ModelHelper.SI + "mvField"))
            {
                ModelHelper.RequiredAttribute(field, "name", typeof(SyncAssert));
                MVFields.Add(new KeyValuePair<string, string>(field.Attribute("name").Value, field.Value));
            }
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "filter",
                from mv in MVFields
                select new XElement(ModelHelper.SI + "mvField",
                    new XAttribute("name", mv.Key), mv.Value));
        }

        public IDictionary<string, AttributeValue> DataWrapper(SyncProvider provider)
        {
            var query = "SELECT object_id FROM mms_metaverse mv WHERE {0}";
            var clauses = new List<string>();
            var para = new List<SqlParameter>();

            int i = 0;
            foreach (var pair in MVFields)
            {
                clauses.Add(String.Format("{0}=@{1}", Sync.SqlQuote.quote("mv." + pair.Key, true), i));
                para.Add(new SqlParameter("@" + i.ToString(), SyncFixture.HandleSyncVal(pair.Value)));
                i++;
            }

            query = String.Format(query, String.Join(" AND ", clauses));

            var guid = (Guid)Sync.SqlHelper.Scalar(provider.ConnectionString, query, para.ToArray());

            return new MVWrapper(guid, provider.ConnectionString);
        }
    }

    public class SyncAssertCS : SyncAssert
    {
        public SyncAssertCS() : base() { }
        public SyncAssertCS(JObject json)
            : base(json)
        {
            Filter = JsonConvert.DeserializeObject<CSFilter>(json["Filter"].ToString());
        }
    }

    public class SyncAssertMV : SyncAssert
    {
        public SyncAssertMV() : base() { }
        public SyncAssertMV(JObject json)
            : base(json)
        {
            Filter = JsonConvert.DeserializeObject<MVFilter>(json["Filter"].ToString());
        }
    }

    public class SyncAssertRunHistory : SyncAssert
    {
        public SyncAssertRunHistory() : base() { }
        public SyncAssertRunHistory(JObject json)
            : base(json)
        {
            Filter = JsonConvert.DeserializeObject<RunFilter>(json["Filter"].ToString());
        }
    }

    public class SyncAssertMA : SyncAssert
    {
        public SyncAssertMA() : base() { }
        public SyncAssertMA(JObject json)
            : base(json)
        {
            Filter = JsonConvert.DeserializeObject<MAFilter>(json["Filter"].ToString());
        }
    }

    public class SyncAssertPassword : SyncAssert
    {
        public SyncAssertPassword() : base() { }
        public SyncAssertPassword(JObject json)
            : base(json)
        {
            Filter = JsonConvert.DeserializeObject<PasswordHistoryFilter>(json["Filter"].ToString());
        }
    }

    public class SyncAssertServer : SyncAssert
    {
        public SyncAssertServer() : base() { }
        public SyncAssertServer(JObject json)
            : base(json)
        {
            Filter = JsonConvert.DeserializeObject<ServerFilter>(json["Filter"].ToString());
        }
    }

    [JsonObject(ItemTypeNameHandling = TypeNameHandling.Objects)]
    public abstract class SyncAssert : IAssertion
    {
        public string Name { get; set; }
        public string Operation { get; set; }
        
        [JsonIgnore]
        public SyncSystem System { get; set; }

        public ISyncFilter Filter { get; set; }
        public List<AssertField> Fields { get; set; }

        public event ProgressEvent ReportProgress = delegate { };

        public string Type { get; set; }

        public SyncAssert() { }
        public SyncAssert(JObject json)
        {
            Name = json["Name"].ToString();
            Operation = json["Operation"].ToString();
            Type = json["Type"].ToString();
            if (json["Fields"] != null)
            {
                Fields = JsonConvert.DeserializeObject<List<AssertField>>(json["Fields"].ToString());
            }
        }

        public virtual bool Test()
        {
            return ((SyncProvider)System.Provider).TestAssert(this);
        }

        public virtual void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            foreach (var attr in new XName[] { "name", ModelHelper.XSI + "type", "operation", "system" })
            {
                ModelHelper.RequiredAttribute(xml, attr, typeof(Assertion));
            }

            Name = xml.Attribute("name").Value;
            Operation = xml.Attribute("operation").Value;
            if (!systems.ContainsKey(xml.Attribute("system").Value))
            {
                throw new XmlParseException("Assertion references undefined system", xml, typeof(IAssertion));
            }

            System = systems[xml.Attribute("system").Value];
            Type = xml.Attribute(ModelHelper.XSI + "type").Value;

            switch (Type)
            {
                case "SyncAssertCS":
                    Filter = new CSFilter(xml);
                    break;
                case "SyncAssertMV":
                    Filter = new MVFilter(xml);
                    break;
                case "SyncAssertRunHistory":
                    Filter = new RunFilter(xml);
                    break;
                case "SyncAssertMA":
                    Filter = new MAFilter(xml);
                    break;
                case "SyncAssertPassword":
                    Filter = new PasswordHistoryFilter(xml);
                    break;
                case "SyncAssertServer":
                    Filter = new ServerFilter();
                    break;
            }

            Fields = new List<AssertField>();
            foreach (var f in xml.Elements(ModelHelper.SI + "field"))
            {
                var t = AssertFieldType.Value;
                if (f.Attributes("type").Count() > 0)
                {
                    try
                    {
                        t = (AssertFieldType)Enum.Parse(typeof(AssertFieldType), f.Attribute("type").Value);
                    }
                    catch (ArgumentException)
                    {
                        throw new XmlParseException(String.Format("Error parsing field. Invalid type {0}.", f.Attribute("type").Value), xml, typeof(SyncAssert));
                    }
                }

                Fields.Add(new AssertField { Type = t, Value = f.Value, Field = f.Attributes("name").Count() > 0 ? f.Attribute("name").Value : null });
            }
        }

        public virtual XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "assert",
                new XAttribute(ModelHelper.XSI + "type", Type),
                new XAttribute("name", Name),
                new XAttribute("system", System.Name),
                new XAttribute("operation", Operation),
                Filter.GetXml(),
                from field in Fields
                select new XElement(ModelHelper.SI + "field",
                    String.IsNullOrEmpty(field.Field) ? (object)"" : (object)new XAttribute("name", field.Field),
                    new XAttribute("type", Enum.GetName(typeof(AssertFieldType), field.Type)),
                    field.Value));
        }
    }
}
