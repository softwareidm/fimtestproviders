﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml.Linq;
using Microsoft.Win32;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.ClockProvider
{
    static class ClockProvider
    {
        public struct SYSTEMTIME
        {
            public ushort wYear;
            public ushort wMonth;
            public ushort wDayOfWeek;
            public ushort wDay;
            public ushort wHour;
            public ushort wMinute;
            public ushort wSecond;
            public ushort wMilliseconds;

            public void FromDateTime(DateTime time)
            {
                wYear = (ushort)time.Year;
                wMonth = (ushort)time.Month;
                wDayOfWeek = (ushort)time.DayOfWeek;
                wDay = (ushort)time.Day;
                wHour = (ushort)time.Hour;
                wMinute = (ushort)time.Minute;
                wSecond = (ushort)time.Second;
                wMilliseconds = (ushort)time.Millisecond;
            }

            public DateTime ToDateTime()
            {
                return new DateTime(wYear, wMonth, wDay, wHour, wMinute, wSecond, wMilliseconds);
            }

            public static DateTime ToDateTime(SYSTEMTIME time)
            {
                return time.ToDateTime();
            }
        }

        // SetLocalTime C# Signature
        [DllImport("Kernel32.dll")]
        public static extern bool SetLocalTime(ref SYSTEMTIME Time);

        static DateTime HandleVal(string val)
        {
            Regex dateReg = new Regex(@"^\[\s?today([^\]]*)]$");
            if (dateReg.IsMatch(val))
            {
                var m = dateReg.Match(val);
                var ret = DateTime.Now.ToUniversalTime();
                if (!String.IsNullOrEmpty(m.Groups[1].Value))
                {
                    ret = ret.AddDays(double.Parse(m.Groups[1].Value.Replace(" ", "")));
                }

                return ret;
            }
            else if (val == "[reset]")
            {
                return DateTime.MinValue;
            }
            else
            {
                return DateTime.Parse(val);
            }
        }

        static TimeSpan changedBy;
        static string originalTZ;

        static void WriteTimeZone(string timezone)
        {
            Process proc = new Process();
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.FileName = "tzutil";
            proc.StartInfo.Arguments = String.Format("/s \"{0}\"", timezone);
            proc.StartInfo.UseShellExecute = false;
            proc.Start();
            proc.WaitForExit();
        }

        public static void WriteFixture(ClockFixture write)
        {
            if (String.IsNullOrEmpty(originalTZ))
            {
                originalTZ = TimeZone.CurrentTimeZone.IsDaylightSavingTime(DateTime.Now) ? TimeZone.CurrentTimeZone.DaylightName : TimeZone.CurrentTimeZone.StandardName;
            }

            var toDate = HandleVal(write.Datetime);

            if (toDate == DateTime.MinValue)
            {
                toDate = DateTime.Now.ToUniversalTime() - changedBy;
                if (TimeZone.CurrentTimeZone.ToString() != originalTZ)
                {
                    WriteTimeZone(originalTZ);
                    toDate = toDate.ToLocalTime();
                }

                changedBy = TimeSpan.Zero;
            }
            else
            {
                // moving to future results in
                changedBy += (toDate - DateTime.Now.ToUniversalTime());

                if (!String.IsNullOrEmpty(write.Timezone))
                {
                    WriteTimeZone(write.Timezone);
                }
            }

            var st = new SYSTEMTIME();
            st.FromDateTime(toDate.ToLocalTime());
            SetLocalTime(ref st);

        }
    }

    public class ClockFixture : IFixture
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public SyncSystem System
        {
            get { return null; }
            set { throw new NotImplementedException("ClockFixture does not require or support a system parameter"); }
        }

        public string Datetime { get; set; }
        public string Timezone { get; set; }
        public event ProgressEvent ReportProgress = delegate { };

        public string Operation { get; set; }

        public void Load()
        {
            ClockProvider.WriteFixture(this);
        }

        public ClockFixture() { }

        public void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            foreach (var attr in new XName[] { "name", "operation", ModelHelper.XSI + "type" })
            {
                ModelHelper.RequiredAttribute(xml, attr, typeof(ClockFixture));
            }

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "datetime", typeof(ClockFixture));

            this.Operation = xml.Attribute("operation").Value;
            Name = xml.Attribute("name").Value;
            var d = xml.Element(ModelHelper.SI + "datetime");
            Datetime = d.Value;
            if (d.Attributes("timezone").Count() > 0)
            {
                Timezone = d.Attribute("timezone").Value;
            }
            else
            {
                Timezone = null;
            }
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "fixture",
                new XAttribute(ModelHelper.XSI + "type", "ClockFixture"),
                new XAttribute("operation", Operation),
                new XAttribute("name", Name),
                new XAttribute("system", ""),
                new XElement(ModelHelper.SI + "datetime", Datetime));
            if (!String.IsNullOrEmpty(Timezone))
            {
                ret.Element(ModelHelper.SI + "datetime").SetAttributeValue("timezone", Timezone);
            }

            return ret;
        }
    }
}
