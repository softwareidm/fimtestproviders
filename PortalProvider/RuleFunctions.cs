﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    public class PortalRuleFunctions : IRuleFunctions
    {
        public static AttributeValue FileText(AttributeValue fileName)
        {
            if (!File.Exists(fileName.StringValue))
            {
                return new AttributeValue(null);
            }

            return new AttributeValue(File.ReadAllText(fileName.StringValue));
        }

        public static AttributeValue PortalTime(AttributeValue dateTime)
        {
            if (dateTime.DateValue.HasValue)
            {
                return new AttributeValue(dateTime.DateValue.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            }

            return new AttributeValue(null);
        }

        public static AttributeValue ServerEnv(AttributeValue _default, Dictionary<string, AttributeValue> map)
        {
            var server = Environment.MachineName.ToUpper();
            if (map != null && map.ContainsKey(server))
            {
                return map[server];
            }

            return _default;
        }

        public static AttributeValue PortalResolve(AttributeValue query, AttributeValue system)
        {
            var sys = ProviderTypes.Systems[system.StringValue];
            var prov = (PortalProvider)sys.Provider;

            string val = null;
            using (var client = prov.ClientFactory.GetClient(new Credential(), prov.DefaultCredential))
            {
                foreach (var res in client.Enumerate(query.StringValue))
                {
                    val = res.ObjectID.Value;
                    break;
                }
            }

            return new AttributeValue(val);
        }
    }
}
