﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Microsoft.ResourceManagement.Client;
using Microsoft.ResourceManagement.ObjectModel;
using Microsoft.ResourceManagement.ObjectModel.ResourceTypes;
using SoftwareIDM.FIMTest.ProviderModel;
using Newtonsoft.Json.Linq;

namespace SoftwareIDM.FIMTest.Provider
{
    public class RmResourceWrapper : IDictionary<string, AttributeValue>
    {
        RmResource Resource;
        public RmResourceWrapper(RmResource resource)
        {
            Resource = resource;
        }

        public AttributeValue this[string key]
        {
            get
            {
                RmAttributeValue val;
                try
                {
                    val = Resource[key];
                }
                catch (KeyNotFoundException)
                {
                    return new AttributeValue(null);
                }

                if (val.IsMultiValue)
                {
                    return AttributeValue.MultiValue(
                        from v in val.Values
                        select (v.GetType() == typeof(RmReference) ? ((RmReference)v).Value : v), MultiBehavior.Append);
                }
                else
                {
                    if (val.Value != null && val.Value.GetType() == typeof(RmReference))
                    {
                        return new AttributeValue(((RmReference)val.Value).Value);
                    }
                    else
                    {
                        return new AttributeValue(val.Value);
                    }
                }
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #region Unimplemented

        public void Add(string key, AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(string key)
        {
            throw new NotImplementedException();
        }

        public ICollection<string> Keys
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(string key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(string key, out AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public ICollection<AttributeValue> Values
        {
            get { throw new NotImplementedException(); }
        }

        public void Add(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<string, AttributeValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<string, AttributeValue>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class Credential
    {
        public string user { get; set; }
        public string password { get; set; }
        public string domain { get; set; }
    }

    public class ClientFactory
    {
        public string Identity { get; set; }
        public string Url { get; set; }
        public bool UseAppConfig { get; set; }
        DefaultClient CurrentClient { get; set; }

        public ClientFactory()
        {

        }

        public ClientFactory(XElement xml)
        {
            if (xml.Elements(ModelHelper.SI + "useAppConfig").Count() > 0)
            {
                UseAppConfig = true;
            }
            else
            {
                foreach (var el in new string[] { "upnIdentity", "url" })
                {
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + el, typeof(PortalProvider));
                }

                Identity = xml.Element(ModelHelper.SI + "upnIdentity").Value;
                Url = xml.Element(ModelHelper.SI + "url").Value;
                if (!Url.EndsWith("/"))
                {
                    Url = Url + "/";
                }

                if (!Regex.IsMatch(Url, "^https?://"))
                {
                    throw new XmlParseException("Service url must take form http(s)://host:port/", xml.Element(ModelHelper.SI + "url"), typeof(PortalProvider));
                }

                if (!Identity.Contains('@') && !Identity.Contains('\\'))
                {
                    throw new XmlParseException("Service identity must take form user@domain or domain\\user", xml.Element(ModelHelper.SI + "upnIdentity"), typeof(PortalProvider));
                }
            }
        }

        public Binding GetContextBinding(long maxReceiveSize)
        {
            var ret = new WSHttpContextBinding();
            ret.MaxReceivedMessageSize = maxReceiveSize;

            ret.Security.Mode = SecurityMode.Message;

            ret.Security.Message.EstablishSecurityContext = false;
            ret.Security.Message.NegotiateServiceCredential = true;
            ret.Security.Message.AlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Default;
            ret.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

            return ret;
        }

        public EndpointAddress GetAddress(string subUrl)
        {
            return new EndpointAddress(
                new Uri(String.Format("{0}ResourceManagementService/{1}", Url, subUrl)),
                EndpointIdentity.CreateUpnIdentity(Identity));
        }

        public DefaultClient GetClient(Credential obj, Credential def)
        {
            var net = new NetworkCredential();
            if (!String.IsNullOrEmpty(obj.user))
            {
                net = new NetworkCredential(obj.user, obj.password, obj.domain);
            }
            else if (!String.IsNullOrEmpty(def.user))
            {
                net = new NetworkCredential(def.user, def.password, def.domain);
            }

            DefaultClient client;
            if (UseAppConfig)
            {
                client = new DefaultClient();
            }
            else
            {
                var mexBinding = new WSHttpBinding(SecurityMode.None);
                mexBinding.MaxReceivedMessageSize = 965536;

                client = new DefaultClient(
                    GetContextBinding(565536),
                    GetAddress("Resource"),
                    GetContextBinding(565536),
                    GetAddress("ResourceFactory"),
                    GetContextBinding(13565536),
                    GetAddress("Enumeration"),
                    mexBinding,
                    GetAddress("MEX"),
                    GetContextBinding(565536),
                    GetAddress("Alternate"));
            }

            client.ClientCredential = net;
            client.RefreshSchema();
            CurrentClient = client;
            return client;
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "service");
            if (UseAppConfig)
            {
                ret.Add(new XElement(ModelHelper.SI + "useAppConfig", "true"));
            }
            else
            {
                ret.Add(new XElement(ModelHelper.SI + "upnIdentity", Identity),
                        new XElement(ModelHelper.SI + "url", Url));
            }

            return ret;
        }
    }

    class ReferenceCompare : ICompareHandler
    {
        DefaultClient Client;
        public ReferenceCompare(DefaultClient client)
        {
            Client = client;
        }

        public Regex Pattern
        {
            get { return new Regex(@"^\[/.+]$"); }
        }

        public bool Test(string compare, AttributeValue value)
        {
            compare = compare.Substring(1, compare.Length - 2);

            bool found = false;
            bool ret = false;
            foreach (var res in Client.Enumerate(compare))
            {
                found = true;
                if (res.ObjectID.Value == value.StringValue)
                {
                    ret = true;
                }
            }

            if (!found)
            {
                throw new ArgumentException(String.Format("Unable to find object(s) matching filter '{0}'", compare));
            }

            return ret;
        }
    }

    public class PortalProvider : IProvider
    {
        public Credential DefaultCredential { get; set; }
        public ClientFactory ClientFactory { get; set; }

        public PortalProvider()
        {
            DefaultCredential = new Credential();
            ClientFactory = new ClientFactory();
        }

        public JObject ObjectTypes()
        {
            var ret = new JArray();
            var client = ClientFactory.GetClient(new Credential(), DefaultCredential);

            foreach (var resource in client.Enumerate("/ObjectTypeDescription"))
            {
                ret.Add(new JObject(
                    new JProperty("Name", (string)resource["Name"].Value),
                    new JProperty("DisplayName", resource.DisplayName)));
            }

            return new JObject(new JProperty("objects", ret));
        }

        public JObject ObjectAttributes(object json)
        {
            var objectType = json.ToString();
            var ret = new JArray();
            var client = ClientFactory.GetClient(new Credential(), DefaultCredential);

            foreach (var resource in client.Enumerate("/BindingDescription[BoundObjectType=/ObjectTypeDescription[Name='" + objectType + "']]"))
            {
                var attr = client.Get((RmReference)resource["BoundAttributeType"].Value);
                ret.Add(new JObject(
                    new JProperty("Name", (string)attr["Name"].Value),
                    new JProperty("StringRegex", attr["StringRegex"].Value),
                    new JProperty("IntegerMaximum", attr["IntegerMaximum"].Value),
                    new JProperty("IntegerMinimum", attr["IntegerMinimum"].Value),
                    new JProperty("Multivalued", attr["Multivalued"].Value),
                    new JProperty("DataType", (string)attr["DataType"].Value),
                    new JProperty("Required", resource["Required"].Value),
                    new JProperty("DisplayName", resource.DisplayName)));
            }

            return new JObject(new JProperty("attributes", ret));
        }

        void Add(DefaultClient client, PortalFixture fix)
        {
            var filter = PortalFixture.HandlePortalVal(fix.Add.Filter, client, false).ToString();
            // convert to update if the object already exists
            foreach (var res in client.Enumerate(filter))
            {
                var fields = new List<KeyValuePair<string, string>>();
                foreach (var f in fix.Fields)
                {
                    fields.Add(new KeyValuePair<string, string>(f.Key, f.Value));
                }

                if (!String.IsNullOrEmpty(fix.Add.DisplayName))
                {
                    fields.Add(new KeyValuePair<string, string>("DisplayName", 
                        PortalFixture.HandlePortalVal(fix.Add.DisplayName, client, false).ToString()));
                }
                    
                if (!String.IsNullOrEmpty(fix.Add.Description))
                {
                    fields.Add(new KeyValuePair<string, string>("Description",
                        PortalFixture.HandlePortalVal(fix.Add.Description, client, false).ToString()));
                }
                
                var multis = new List<MultiField>();
                multis.AddRange(fix.Multifields);

                var update = new PortalFixture
                {
                    Operation = "Update",
                    Filter = fix.Add.Filter,
                    Fields = fields,
                    Multifields = multis,
                    System = fix.System
                };

                Update(client, update);
                return;
            }

            var resource = new RmResource();
            resource.ObjectType = fix.Add.ObjectType;
            resource.DisplayName = fix.Add.DisplayName;
            
            if (!String.IsNullOrEmpty(fix.Add.Description))
            {
                resource.Description = fix.Add.Description;
            }

            foreach (var field in fix.Fields)
            {
                var val = PortalFixture.HandlePortalVal(field.Value, client, false);
                resource[field.Key] = new RmAttributeValueSingle((IComparable)val);
            }

            foreach (var field in fix.Multifields)
            {
                var fields = new List<IComparable>();
                foreach (var val in field.Values)
                {
                    var add = PortalFixture.HandlePortalVal(val, client, true);
                    if (add.GetType() == typeof(List<RmReference>))
                    {
                        fields.AddRange(((List<RmReference>)add).ToArray());
                    }
                    else
                    {
                        fields.Add((IComparable)add);
                    }
                }

                resource[field.Name] = new RmAttributeValueMulti(fields);
            }

            try
            {
                client.Create(resource);

                if (fix.FailureExpected || fix.ApprovalExpected)
                {
                    throw new Exception("Fixture load did not fail as expected.");
                }
            }
            catch (Exception e)
            {
                if (e.Message == "Fixture load did not fail as expected.")
                {
                    throw;
                }

                if (!(e.Message == "Permission is required" && fix.ApprovalExpected) && !fix.FailureExpected)
                {
                    throw;
                }
            }
        }

        void Update(DefaultClient client, PortalFixture fix)
        {
            var found = false;
            var filter = PortalFixture.HandlePortalVal(fix.Filter, client, false).ToString();
            foreach (var resource in client.Enumerate(filter))
            {
                found = true;
                RmResourceChanges changes = new RmResourceChanges(resource);
                changes.BeginChanges();
                foreach (var field in fix.Fields)
                {
                    if (field.Key == "DisplayName" && resource.DisplayName != field.Value)
                    {
                        resource.DisplayName = field.Value;
                    }
                    else if (field.Key == "Description" && resource.Description != field.Value)
                    {
                        resource.Description = field.Value;
                    }
                    else if (field.Key == "Locale" && resource.Locale != field.Value)
                    {
                        resource.Locale = field.Value;
                    }
                    else
                    {
                        var val = (IComparable)PortalFixture.HandlePortalVal(field.Value, client, false);
                        if (resource.Keys.Contains(new RmAttributeName(field.Key)) && resource[field.Key].Value != null)
                        {
                            if (resource[field.Key].Value.ToString() != val.ToString())
                            {
                                resource[field.Key] = new RmAttributeValueSingle(val);
                            }
                        }
                        else
                        {
                            resource[field.Key] = new RmAttributeValueSingle(val);
                        }
                    }
                }

                foreach (var field in fix.Multifields)
                {
                    var multiVals = new List<IComparable>();
                    foreach (var val in field.Values)
                    {
                        var add = PortalFixture.HandlePortalVal(val, client, true);
                        if (add.GetType() == typeof(List<RmReference>))
                        {
                            multiVals.AddRange(((List<RmReference>)add).ToArray());
                        }
                        else
                        {
                            if (add != null && !String.IsNullOrEmpty(add.ToString()))
                            {
                                multiVals.Add((IComparable)add.ToString());
                            }
                        }
                    }

                    bool contains;
                    switch (field.Mode)
                    {
                        case MultiFieldMode.Append:
                            if (resource.Keys.Contains(new RmAttributeName(field.Name)) && resource[field.Name].Values.Count > 0)
                            {
                                var vals = resource[field.Name].Values;
                                foreach (var val in vals)
                                {
                                    contains = false;
                                    foreach (var v in multiVals)
                                    {
                                        if (val != null && !String.IsNullOrEmpty(val.ToString()) &&
                                            v.ToString() == val.ToString())
                                        {
                                            contains = true;
                                            break;
                                        }
                                    }

                                    if (!contains)
                                    {
                                        multiVals.Add(val);
                                    }
                                }
                            }

                            resource[field.Name] = new RmAttributeValueMulti(multiVals);
                            break;
                        case MultiFieldMode.Replace:
                            resource[field.Name] = new RmAttributeValueMulti(multiVals);
                            break;
                        case MultiFieldMode.Remove:
                            if (resource.Keys.Contains(new RmAttributeName(field.Name)) && resource[field.Name].Values.Count > 0)
                            {
                                var newVals = new List<IComparable>();
                                var vals = resource[field.Name].Values;
                                foreach (var v in multiVals)
                                {
                                    var existingVals = resource[field.Name].Values;
                                    foreach (var val in existingVals)
                                    {
                                        contains = false;
                                        foreach (var test in multiVals)
                                        {
                                            if (val != null && !String.IsNullOrEmpty(val.ToString()) &&
                                                test.ToString() == val.ToString())
                                            {
                                                contains = true;
                                                break;
                                            }
                                        }

                                        if (!contains)
                                        {
                                            newVals.Add(val);
                                        }
                                    }
                                }

                                resource[field.Name] = new RmAttributeValueMulti(newVals);
                            }
                            break;
                    }
                }

                if (!found && !fix.FailureExpected)
                {
                    throw new ArgumentException("Unable to find object matching filter: {0}", filter);
                }

                try
                {
                    if (changes.GetChanges().Count > 0)
                    {
                        client.Put(changes);
                        changes.AcceptChanges();

                        if (fix.FailureExpected || fix.ApprovalExpected)
                        {
                            throw new Exception("Fixture load did not fail as expected.");
                        }
                    }
                }
                catch (Exception e)
                {
                    if (e.Message == "Fixture load did not fail as expected.")
                    {
                        throw;
                    }

                    if (!(e.Message == "Permission is required" && fix.ApprovalExpected) && !fix.FailureExpected)
                    {
                        changes.DiscardChanges();
                        throw;
                    }
                }
            }
        }

        void Delete(DefaultClient client, PortalFixture fix)
        {
            var dels = new List<RmReference>();

            var filter = PortalFixture.HandlePortalVal(fix.Filter, client, false).ToString();
            foreach (var res in client.Enumerate(filter))
            {
                dels.Add(res.ObjectID);
            }

            foreach (var res in dels)
            {
                try
                {
                    client.Delete(res);
                }
                catch (Exception e)
                {
                    if (!(e.Message == "Permission is required" && fix.ApprovalExpected))
                    {
                        throw;
                    }
                }
            }
        }

        void Approve(DefaultClient client, PortalFixture fix)
        {
            var filter = PortalFixture.HandlePortalVal(fix.Filter, client, false).ToString();

            foreach (RmResource resource in client.Enumerate(filter))
            {
                RmApproval approval = resource as RmApproval;
                if (approval != null)
                {
                    EndpointAddress address = new EndpointAddress(new Uri(approval.ApprovalEndpointAddress),
                        EndpointIdentity.CreateUpnIdentity(ClientFactory.Identity));
                    // if operation is Reject it will pass false to the Approve method
                    client.Approve(approval, fix.Operation == "Approve", ClientFactory.GetContextBinding(65536), address);
                    // sleep 5 seconds so FIM can process approval
                    System.Threading.Thread.Sleep(5000);
                }
            }
        }

        public void WriteFixture(IFixture write)
        {
            var fix = (PortalFixture)write;
            using (var client = ClientFactory.GetClient(fix.Credential, DefaultCredential))
            {
                switch (fix.Operation)
                {
                    case "Add":
                        Add(client, fix);
                        break;
                    case "Update":
                        Update(client, fix);
                        break;
                    case "Delete":
                        Delete(client, fix);
                        break;
                    case "Approve":
                    case "Reject":
                        Approve(client, fix);
                        break;
                    default:
                        throw new NotImplementedException(String.Format("This provider does not support fixture operation {0}", fix.Operation));
                }
            }
        }

        public bool TestAssert(IAssertion assertion)
        {
            if (!(assertion is PortalAssert))
            {
                throw new ArgumentException("PortalProvider only accepts PortalAssertions");
            }

            var assert = (PortalAssert)assertion;


            using (var client = ClientFactory.GetClient(assert.Credential, DefaultCredential))
            {
                var filter = PortalFixture.HandlePortalVal(assert.Filter, client, false).ToString();
                bool exists;
                switch (assert.Operation)
                {
                    case "Exists":
                        foreach (var result in client.Enumerate(filter))
                        {
                            return true;
                        }

                        return false;
                    case "NotExists":
                        foreach (var result in client.Enumerate(filter))
                        {
                            return false;
                        }

                        return true;
                    case "HasValue":
                        exists = false;
                        foreach (var result in client.Enumerate(filter))
                        {
                            exists = true;
                            foreach (var field in assert.Fields)
                            {
                                if (!Evaluate.TestAssertVal(field, new RmResourceWrapper(result), this, new NullCompare(), new DateCompare(), new ReferenceCompare(client)))
                                {
                                    throw new FormatException(String.Format("Field {0} does not match {1}",
                                        field.Field, field.Value));
                                }
                            }

                            foreach (var multi in assert.MultiFields)
                            {
                                if (!Evaluate.TestAssertVal(multi.Value, new RmResourceWrapper(result), this, new NullCompare(), new DateCompare(), new ReferenceCompare(client)))
                                {
                                    throw new FormatException(String.Format("Field {0} does not match {1}",
                                        multi.Key, multi.Value.Value));
                                }
                            }
                        }

                        if (!exists)
                        {
                            throw new IndexOutOfRangeException(
                                String.Format("Assert '{0}' with Filter {1} Does not match an object in the Portal.",
                                    assert.Name, assert.Filter));
                        }

                        return true;
                    case "NotHasValue":
                        exists = false;
                        foreach (var result in client.Enumerate(filter))
                        {
                            exists = true;
                            foreach (var field in assert.Fields)
                            {
                                if (Evaluate.TestAssertVal(field, new RmResourceWrapper(result), this, new NullCompare(), new DateCompare(), new ReferenceCompare(client)))
                                {
                                    throw new FormatException(String.Format("Field {0} matches {1}",
                                        field.Field, field.Value));
                                }
                            }

                            foreach (var multi in assert.MultiFields)
                            {
                                if (Evaluate.TestAssertVal(multi.Value, new RmResourceWrapper(result), this, new NullCompare(), new DateCompare(), new ReferenceCompare(client)))
                                {
                                    throw new FormatException(String.Format("Field {0} matches {1}",
                                        multi.Key, multi.Value.Value));
                                }
                            }
                        }

                        if (!exists)
                        {
                            throw new IndexOutOfRangeException(
                                String.Format("Assert '{0}' with Filter {1} Does not match an object in the Portal.",
                                    assert.Name, filter));
                        }

                        return true;
                    default:
                        return false;
                }
            }
        }

        public void Initialize(XElement xml)
        {
            if (xml.Elements(ModelHelper.SI + "defaultCredential").Count() > 0)
            {
                var el = xml.Element(ModelHelper.SI + "defaultCredential");
                foreach (var attr in new string[] { "user", "password", "domain" })
                {
                    ModelHelper.RequiredAttribute(el, attr, typeof(PortalProvider));
                }

                DefaultCredential = new Credential
                {
                    user = el.Attribute("user").Value,
                    password = el.Attribute("password").Value,
                    domain = el.Attribute("domain").Value
                };
            }

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "service", typeof(PortalProvider));
            ClientFactory = new ClientFactory(xml.Element(ModelHelper.SI + "service"));
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "provider",
                new XAttribute(ModelHelper.XSI + "type", "PortalProvider"), ClientFactory.GetXml());
            if (!String.IsNullOrEmpty(DefaultCredential.user))
            {
                ret.Add(new XElement(ModelHelper.SI + "defaultCredential",
                    new XAttribute("user", DefaultCredential.user),
                    new XAttribute("password", DefaultCredential.password),
                    new XAttribute("domain", DefaultCredential.domain)));
            }

            return ret;
        }
    }
}
