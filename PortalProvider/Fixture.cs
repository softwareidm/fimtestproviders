﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Microsoft.ResourceManagement.Client;
using Microsoft.ResourceManagement.ObjectModel;
using Microsoft.ResourceManagement.ObjectModel.ResourceTypes;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    public enum MultiFieldMode
    {
        Append,
        Replace,
        Remove
    }

    public class Add
    {
        public string ObjectType { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string Filter { get; set; }
        bool hasFilter = false;

        public Add() { }

        public Add(XElement xml)
        {
            foreach (var el in new string[] { "ObjectType", "DisplayName" })
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + el, typeof(PortalFixture));
            }

            DisplayName = xml.Element(ModelHelper.SI + "DisplayName").Value;
            ObjectType = xml.Element(ModelHelper.SI + "ObjectType").Value;

            if (xml.Elements(ModelHelper.SI + "Description").Count() > 0)
            {
                Description = xml.Element(ModelHelper.SI + "Description").Value;
            }

            if (xml.Elements(ModelHelper.SI + "filter").Count() > 0)
            {
                Filter = xml.Element(ModelHelper.SI + "filter").Value;
                hasFilter = true;
            }
            else
            {
                Filter = String.Format("/{0}[DisplayName='{1}']", ObjectType, DisplayName.Replace("'", "&apos;").Replace("\"", "&quot;"));
            }
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "add",
                new XElement(ModelHelper.SI + "DisplayName", DisplayName),
                new XElement(ModelHelper.SI + "ObjectType", ObjectType));

            if (hasFilter)
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "filter", Filter));
            }

            if (!String.IsNullOrEmpty(Description))
            {
                ret.Add(new XElement(ModelHelper.SI + "Description", Description));
            }

            return ret;
        }
    }

    public class MultiField
    {
        public MultiFieldMode Mode { get; set; }
        public string Name { get; set; }
        public List<string> Values { get; set; }

        public MultiField()
        {
            Values = new List<string>();
        }

        public MultiField(XElement xml)
        {
            Values = new List<string>();
            ModelHelper.RequiredAttribute(xml, "name", typeof(PortalFixture));
            Name = xml.Attribute("name").Value;
            if (xml.Attributes("type").Count() > 0)
            {
                Mode = (MultiFieldMode)Enum.Parse(typeof(MultiFieldMode), xml.Attribute("type").Value);
            }

            Values.AddRange(
                from v in xml.Elements(ModelHelper.SI + "value")
                select v.Value);
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "multifield",
                new XAttribute("name", Name),
                new XAttribute("type", Mode.ToString()),
                from v in Values
                select new XElement(ModelHelper.SI + "value", v));
        }
    }

    public class PortalFixture : Fixture
    {
        public Credential Credential { get; set; }

        public List<MultiField> Multifields { get; set; }

        public string Filter { get; set; }
        public bool ApprovalExpected { get; set; }
        public bool FailureExpected { get; set; }
        public Add Add { get; set; }

        public PortalFixture()
            : base()
        {
            Multifields = new List<MultiField>();
            Credential = new Credential();
        }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);

            if (xml.Elements(ModelHelper.SI + "credential").Count() > 0)
            {
                var el = xml.Element(ModelHelper.SI + "credential");
                foreach (var attr in new string[] { "user", "password", "domain" })
                {
                    ModelHelper.RequiredAttribute(el, attr, typeof(PortalProvider));
                }

                Credential.user = el.Attribute("user").Value;
                Credential.password = el.Attribute("password").Value;
                Credential.domain = el.Attribute("domain").Value;
            }

            if (this.Operation == "Add")
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "add", typeof(PortalFixture));
                Add = new Add(xml.Element(ModelHelper.SI + "add"));
            }
            else
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", typeof(PortalFixture));
                Filter = xml.Element(ModelHelper.SI + "filter").Value;
            }

            if (xml.Elements(ModelHelper.SI + "approvalExpected").Count() > 0)
            {
                var val = xml.Element(ModelHelper.SI + "approvalExpected").Value;
                ApprovalExpected = val == "true" || val == "1";
            }

            if (xml.Elements(ModelHelper.SI + "failureExpected").Count() > 0)
            {
                var val = xml.Element(ModelHelper.SI + "failureExpected").Value;
                FailureExpected = val == "true" || val == "1";
            }

            Multifields.AddRange(from f in xml.Elements(ModelHelper.SI + "multifield")
                                 select new MultiField(f));
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            if (!String.IsNullOrEmpty(Credential.user))
            {
                ret.Add(new XElement(ModelHelper.SI + "credential",
                    new XAttribute("user", Credential.user),
                    new XAttribute("password", Credential.password),
                    new XAttribute("domain", Credential.domain)));
            }

            if (ApprovalExpected)
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "approvalExpected", "true"));
            }

            if (FailureExpected)
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "failureExpected", "true"));
            }

            if (this.Operation == "Add")
            {
                ret.AddFirst(this.Add.GetXml());
            }
            else
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "filter", Filter));
            }

            if (Multifields.Count > 0)
            {
                ret.Add(
                    from m in Multifields
                    select m.GetXml());
            }

            return ret;
        }

        public static object HandlePortalVal(string val, DefaultClient client, bool allowMultiRef)
        {
            var ret = Fixture.HandleVal(val);
            if (ret == null)
            {
                val = "";
            }
            else
            {
                val = ret.ToString();
            }

            if (val.StartsWith("[") && val.EndsWith("]"))
            {
                if (Regex.IsMatch(val, @"(?<!\\){{(.*?)}}"))
                {
                    val = val.Substring(1, val.Length - 2);
                    var matches = Regex.Matches(val, @"(?<!\\){{(.*?)}}");
                    foreach (Match m in matches)
                    {
                        var ruleRegex = new Regex(@"^Rule\((.+)\)$");
                        if (ruleRegex.IsMatch(m.Groups[1].Value))
                        {
                            var rule = new Rule(ruleRegex.Match(m.Groups[1].Value).Groups[1].Value, null);
                            val = val.Replace(m.Value, rule.GetValue(new Dictionary<string, AttributeValue>()).StringValue);
                        }
                        else
                        {
                            foreach (var res in client.Enumerate(m.Groups[1].Value))
                            {
                                val = val.Replace(m.Value, res.ObjectID.Value);
                                break;
                            }
                        }
                    }

                    if (Regex.IsMatch(val, @"(?<!\\)\\{{"))
                    {
                        val = Regex.Replace(val, @"(?<!\\)\\{{", "{{");
                    }

                    ret = val;
                }
                else
                {
                    if (allowMultiRef)
                    {
                        ret = new List<RmReference>();
                    }

                    foreach (var res in client.Enumerate(val.Substring(1, val.Length - 2)))
                    {
                        if (allowMultiRef)
                        {
                            ((List<RmReference>)ret).Add(res.ObjectID);
                        }
                        else
                        {
                            ret = res.ObjectID;
                        }
                    }

                    if (ret == null || (ret.GetType() == typeof(List<RmReference>) && ((List<RmReference>)ret).Count == 0))
                    {
                        throw new ArgumentException(String.Format("Unable to find object(s) matching filter '{0}'", val.Substring(1, val.Length - 2)));
                    }
                }
            }

            if (ret is string && ((string)ret).StartsWith("\\["))
            {
                ret = ((string)ret).Substring(1);
            }

            return ret;
        }
    }


    public class PortalAssert : Assertion
    {
        public List<KeyValuePair<string, AssertField>> MultiFields { get; set; }
        public string Filter { get; set; }
        public Credential Credential { get; set; }

        public PortalAssert()
            : base()
        {
            MultiFields = new List<KeyValuePair<string, AssertField>>();
            Credential = new Credential();
        }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);

            if (xml.Elements(ModelHelper.SI + "credential").Count() > 0)
            {
                var el = xml.Element(ModelHelper.SI + "credential");
                foreach (var attr in new string[] { "user", "password", "domain" })
                {
                    ModelHelper.RequiredAttribute(el, attr, typeof(PortalProvider));
                }

                Credential.user = el.Attribute("user").Value;
                Credential.password = el.Attribute("password").Value;
                Credential.domain = el.Attribute("domain").Value;
            }

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", typeof(PortalAssert));
            Filter = xml.Element(ModelHelper.SI + "filter").Value;
            foreach (var f in xml.Elements(ModelHelper.SI + "multifield"))
            {
                var t = AssertFieldType.Value;
                if (f.Attributes("type").Count() > 0)
                {
                    try
                    {
                        t = (AssertFieldType)Enum.Parse(typeof(AssertFieldType), f.Attribute("type").Value);
                    }
                    catch (ArgumentException)
                    {
                        throw new XmlParseException(String.Format("Error parsing Assertion {0}. Invalid type {1}.", Name, f.Attribute("type").Value), xml, typeof(PortalAssert));
                    }
                }

                MultiFields.Add(new KeyValuePair<string, AssertField>(f.Attribute("name").Value, new AssertField { Type = t, Value = f.Value, Field = f.Attribute("name").Value }));
            }
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            ret.AddFirst(new XElement(ModelHelper.SI + "filter", Filter));
            if (!String.IsNullOrEmpty(Credential.user))
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "credential",
                    new XAttribute("user", Credential.user),
                    new XAttribute("password", Credential.password),
                    new XAttribute("domain", Credential.domain)));
            }

            foreach (var multi in MultiFields)
            {
                var add = new XElement(ModelHelper.SI + "multifield",
                    new XAttribute("name", multi.Key),
                    multi.Value.Value);
                if (multi.Value.Type == AssertFieldType.Regex)
                {
                    add.Add(new XAttribute("type", "Regex"));
                }

                ret.Add(add);
            }

            return ret;
        }
    }
}
