﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

//------------------------------------------------------------------------------
// General information 
//------------------------------------------------------------------------------
[assembly: AssemblyTitle("Microsoft.ResourceManagement.ObjectModel")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("Microsoft.ResourceManagement.ObjectModel")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//------------------------------------------------------------------------------
// COM
//------------------------------------------------------------------------------
[assembly: ComVisible(false)]
[assembly: Guid("c367f435-ce28-4d89-bd19-e6c895ced76a")]

//------------------------------------------------------------------------------
// Version information 
//------------------------------------------------------------------------------
// 1.0.0.0 - 30/06/2010
// - First release
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

//------------------------------------------------------------------------------
// Miscellaneous information 
//------------------------------------------------------------------------------
[assembly: CLSCompliant(true)]
