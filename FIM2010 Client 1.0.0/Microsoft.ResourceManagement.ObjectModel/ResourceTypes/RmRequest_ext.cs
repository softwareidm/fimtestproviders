﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
//using Microsoft.ResourceManagement.WebServices.WSResourceManagement;

namespace Microsoft.ResourceManagement.ObjectModel.ResourceTypes {
    
    /// <summary>
    /// Manual addition to the Request class.
    /// </summary>
    partial class RmRequest {

        /// <summary>
        /// Gets the request parameters as a list of <see cref="RequestParameter"/>
        /// objects.
        /// </summary>
        /// <returns></returns>
        public IList<object> GetRequestParameters() {
            Assembly assembly;
            try
            {
                assembly = Assembly.Load(new AssemblyName("Microsoft.ResourceManagement"));
            }
            catch
            {
                var path = System.IO.Path.Combine(
                    new System.IO.FileInfo(Assembly.GetExecutingAssembly().Location).Directory.FullName,
                    "Microsoft.ResourceManagement.dll");
                assembly = Assembly.LoadFrom(path);
            }
            
            var requestParameterType = assembly.GetType("Microsoft.ResourceManagement.WebServices.WSResourceManagement.RequestParameter");
            XmlSerializer serializer = new XmlSerializer(requestParameterType);
            var t = typeof(List<>).MakeGenericType(requestParameterType);
            IList<object> ret = (IList<object>)t.GetConstructor(new System.Type[]{}).Invoke(new object[]{});

            foreach (string value in this.RequestParameter) { 
                StringReader reader = new StringReader(value);
                var parameter = serializer.Deserialize(reader);
                if (null != parameter) {
                    ret.Add(parameter);
                }
            }
            return ret;
        }

    }
}
