﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

//------------------------------------------------------------------------------
// General information 
//------------------------------------------------------------------------------
[assembly: AssemblyTitle("Microsoft.ResourceManagement.Client")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("Microsoft.ResourceManagement.Client")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2009, Inline Instantiation Copyright © SoftwareIDM 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//------------------------------------------------------------------------------
// COM
//------------------------------------------------------------------------------
[assembly: ComVisible(false)]
[assembly: Guid("96836ab4-10b1-4840-ab30-8a4a0f2f5548")]

//------------------------------------------------------------------------------
// Version information 
//------------------------------------------------------------------------------
// 1.0.0.0 - 30/06/2010
// 1.0.1.104 - 12/19/2012
// - First release
[assembly: AssemblyVersion("1.0.1.104")]
[assembly: AssemblyFileVersion("1.0.1.104")]

//------------------------------------------------------------------------------
// Miscellaneous information 
//------------------------------------------------------------------------------
[assembly: CLSCompliant(true)]

// make internal methods visible to test assembly
[assembly: InternalsVisibleTo("Microsoft.ResourceManagement.Client.Test")]
