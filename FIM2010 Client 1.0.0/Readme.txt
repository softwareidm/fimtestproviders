﻿This client is *UNSUPPORTED* sample code that demonstrates how a person could communicate with the FIM web service interface.
This client is NOT SUPPORTED or RECOMMENDED FOR USE IN PRODUCTION.

This client is designed to work with the Forefront Identity Manager 2010 RC1 release.

Look at the test cases for example usage.  To use in your own client, ensure that the app.config file exists in your project and that you've
included both the ObjectModel and Client DLLs.

Common problems:

> "Support Provider Interface (SSPI) authentication failed. The server may not be running in an account with identity 'host/fimdev'. If the server is running in a service account (Network Service for example), specify the account's ServicePrincipalName as the identity in the EndpointAddress for the server. If the server is running in a user account, specify the account's UserPrincipalName as the identity in the EndpointAddress for the server."
This issue is related to your environment and configuration.  Ensure the following are true:

1) You are using the correct credentials
2) You specified the service's endpoint identity in the application configuration file.
3) The FIM Service and FIM Client can communicate with the Active Directory Domain Controller.

> To set the credentials, edit the Credential.cs file in the ClientTest project.

> To set the service endpoint identity, change the app.config file endpoint elements.  For example:
<userPrincipalName value="fabrikam\FIMService"/>

See http://msdn.microsoft.com/en-us/library/ms733130.aspx for more information.

> Adding members to groups fails to work
As a best practice, ensure the client has a refreshed schema.  By default the client assumes all attributes are single-value strings.