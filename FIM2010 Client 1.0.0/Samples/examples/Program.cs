﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Microsoft.ResourceManagement.Client;
using Microsoft.ResourceManagement.ObjectModel;
using Microsoft.ResourceManagement.ObjectModel.ResourceTypes;

namespace examples {
    class Program {
        static void Main(string[] args) {

            using (DefaultClient client = new DefaultClient()) {
                // set credentials and refresh schema
                //client.ClientCredential = new System.Net.NetworkCredential("user", "pwd", "domain");
                client.ClientCredential = new System.Net.NetworkCredential();
                client.RefreshSchema();

                foreach (RmPerson person in client.Enumerate("/Person[DisplayName='Gandalf the Grey']"))
                {
                    // create the object to track changes to the resource
                    RmResourceChanges changes = new RmResourceChanges(person);
                    changes.BeginChanges();
                    try
                    {
                        // change something in the resource
                        person.DisplayName = "Gandalf the White";
                        // modify the resource on the server
                        client.Put(changes);
                        // the operation succeeded: accept the changes.
                        changes.AcceptChanges();
                        // NOTE: after calling AcceptChanges the RmResourceChanges 
                        // object does not contain any more changes to propagate to
                        // the server.
                    }
                    catch
                    {
                        // an error occurred, so the resource was not modified; 
                        // rollback the changes.
                        changes.DiscardChanges();
                        throw;
                    }
                }
                /*
                // Enumerate the approvals and approve specifying an SPN.
                foreach (RmResource resource in client.Enumerate(string.Format("/Approval[ApprovalStatus='Pending']"))) {
                    RmApproval approval = resource as RmApproval;
                    if (approval != null) {
                        EndpointAddress address = new EndpointAddress(
                            new Uri(approval.ApprovalEndpointAddress),
                            EndpointIdentity.CreateSpnIdentity("FIMService/FIMMachine"));
                        Console.WriteLine("Approving request {0} on endpoint {1}: ", approval.ObjectID, approval.ApprovalEndpointAddress);
                        try {
                            client.Approve(approval, true, address);
                            Console.WriteLine("Success.");
                        } catch (Exception exc) {
                            Console.WriteLine("Failed: {0}", exc);
                        }
                    }
                }
                */
            }
        }
    }
}
