﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ResourceManagement.ObjectModel;
using Microsoft.ResourceManagement.ObjectModel.ResourceTypes;

class RmResourceAndStronglyTypedResources {

    public void RmResourceExample() {

        // create a generic resource
        RmResource resource = new RmResource();
        // set its type
        resource.ObjectType = "Person";
        // set one of its attributes: note that you have to use one of the 
        // "generic" attribute value classes:
        resource["AccountName"] = new RmAttributeValueSingle("guest");

        // Do the same thing with a strongly-typed resource. No need to set
        // the ObjectType property.
        RmPerson person = new RmPerson();
        // Set the AccountName. The strongly-typed resource knows that the
        // property is a single-valued string:
        person.AccountName = "guest";

    }


}
