﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleQAGateQuestionAsker
{
    public class AskPasswordResetQuestionArgs : EventArgs
    {
        public KeyValuePair<int, String> question;
    }
}
