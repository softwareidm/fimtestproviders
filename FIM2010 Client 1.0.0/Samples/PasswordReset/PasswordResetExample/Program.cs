﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ResourceManagement.Client.WsTrust;
using Microsoft.IdentityModel.Protocols.WSTrust;
using Microsoft.ResourceManagement.Client;
using Microsoft.ResourceManagement.ObjectModel.ResourceTypes;
using Microsoft.ResourceManagement.ObjectModel;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Xml.Serialization;
using SampleQAGateQuestionAsker;

namespace PasswordResetApplication
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Get a new DefaultClient

            using (DefaultClient client = new DefaultClient())
            {
                client.RefreshSchema();

                //Create a new QuestionAsker
                QAGateQuestionAsker questionAsker = new QAGateQuestionAsker();

                //Register the AskQuestions method of the questionAsker as a delegate to the client
                client.questionHandler += new QAGateQuestionsHandler(questionAsker.AskQuestions);

                //Create a new QuestionAsker in our Application to hahdle IO
                QuestionAsker clientAsker = new QuestionAsker();

                //Register the AskQuestion method of our clientAsker to ask questions for the questionAsker
                questionAsker.AskPasswordResetQuestion += new AskPasswordResetQuestionHandler(clientAsker.AskQuestion);


                String userDomain;
                String userName;

                Console.WriteLine("We will reset the password for a user.");
                Console.Write("Enter the user's domain (NETBIOS): ");
                userDomain = Console.ReadLine();
                Console.Write("Enter the user's samAccountName: ");
                userName = Console.ReadLine();
                
                client.ResetPassword(userDomain + "\\" + userName);
                
            }
        }

    }

 
    class QuestionAsker
    {
        public void AskQuestion(object sender, AskPasswordResetQuestionArgs e)
        {
            string answer;

            QAGateQuestionAsker asker = (QAGateQuestionAsker)sender;
            Console.WriteLine(e.question.Value);
            answer = Console.ReadLine();

            KeyValuePair<int, String> answerPair = new KeyValuePair<int, String>(e.question.Key,answer);

            asker.addAnswer(answerPair);
        }

    }
}
