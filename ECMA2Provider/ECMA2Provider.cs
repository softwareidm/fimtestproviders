﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.FIMTest.ProviderModel;
using SoftwareIDM.FIMTest.Provider;

namespace SoftwareIDM.FIMTest.Provider.ECMA2
{
    public class ProvConfigParameter
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public SecureString SecureValue { get; set; }

        public ConfigParameter Get()
        {
            if (SecureValue != null)
            {
                return new ConfigParameter(Name, SecureValue);
            }

            return new ConfigParameter(Name, Value);
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "param",
                new XAttribute("name", Name),
                new XAttribute("isSecure", SecureValue == null),
                Value
            );
        }
    }

    public class ECMA2RunConfig
    {
        public int BatchSize { get; set; }

        public int PageSize { get; set; }

        public string PartitionDN { get; set; }
        
        public Guid PartitionId { get; set; }
        
        public string PartitionName { get; set; }

        public OperationType ExportType { get; set; }

        public OperationType ImportType { get; set; }
        
        public List<string> IncludeNodes { get; set; }
        
        public List<string> ExcludeNodes { get; set; }

        public ECMA2RunConfig()
        {
            BatchSize = 1;
            PageSize = 1;
        }

        public ECMA2RunConfig(XElement xml)
        {
            ModelHelper.RequiredAttribute(xml, "batchSize", typeof(ECMA2RunConfig));
            ModelHelper.RequiredAttribute(xml, "exportType", typeof(ECMA2RunConfig));
            BatchSize = int.Parse(xml.Attribute("batchSize").Value);
            PageSize = int.Parse(xml.Attribute("pageSize").Value);
            ExportType = (OperationType)Enum.Parse(typeof(OperationType), xml.Attribute("exportType").Value);
            ImportType = (OperationType)Enum.Parse(typeof(OperationType), xml.Attribute("importType").Value);
            IncludeNodes = new List<string>();
            ExcludeNodes = new List<string>();
            if (xml.Attributes("partitionId").Count() > 0)
            {
                PartitionId = Guid.Parse(xml.Attribute("partitionId").Value);
            }
            else
            {
                PartitionId = Guid.NewGuid();
            }

            if (xml.Attributes("partitionDN").Count() > 0)
            {
                PartitionDN = xml.Attribute("partitionDN").Value;
            }
            else
            {
                PartitionDN = "";
            }

            if (xml.Attributes("partitionName").Count() > 0)
            {
                PartitionName = xml.Attribute("partitionName").Value;
            }
            else
            {
                PartitionName = "";
            }

            if (xml.Elements(ModelHelper.SI + "includes").Count() > 0)
            {
                IncludeNodes.AddRange(from i in xml.Element(ModelHelper.SI + "includes").Elements(ModelHelper.SI + "include")
                                      select i.Value);
            }

            if (xml.Elements(ModelHelper.SI + "excludes").Count() > 0)
            {
                ExcludeNodes.AddRange(from i in xml.Element(ModelHelper.SI + "excludes").Elements(ModelHelper.SI + "exclude")
                                      select i.Value);
            }
        }

        public OpenExportConnectionRunStep GetExportOpen()
        {
            if (PartitionDN != null)
            {
                return new OpenExportConnectionRunStep(
                    Partition.Create(PartitionId, PartitionDN, PartitionName),
                    BatchSize,
                    ExportType,
                    (from i in IncludeNodes
                     select HierarchyNode.Create(i, i)).ToList(),
                    (from e in ExcludeNodes
                     select HierarchyNode.Create(e, e)).ToList());
            }
            else
            {
                return new OpenExportConnectionRunStep();
            }
        }

        public OpenImportConnectionRunStep GetImportOpen(string customData)
        {
            if (PartitionDN != null)
            {
                return new OpenImportConnectionRunStep(
                    Partition.Create(PartitionId, PartitionDN, PartitionName),
                    ImportType,
                    PageSize,
                    customData,
                    (from i in IncludeNodes
                     select HierarchyNode.Create(i, i)).ToList(),
                    (from e in ExcludeNodes
                     select HierarchyNode.Create(e, e)).ToList());
            }
            else
            {
                return new OpenImportConnectionRunStep();
            }
        }

        public XElement GetXml()
        {
            if (PartitionDN == null)
            {
                return null;
            }

            return new XElement(ModelHelper.SI + "runProfile",
                new XAttribute("batchSize", BatchSize),
                new XAttribute("pageSize", PageSize),
                new XAttribute("exportType", ExportType.ToString()),
                new XAttribute("importType", ImportType.ToString()),
                new XAttribute("partitionId", PartitionId),
                new XAttribute("partitionDN", PartitionDN),
                new XAttribute("partitionName", PartitionName),
                new XElement(ModelHelper.SI + "includes",
                    from i in IncludeNodes
                    select new XElement(ModelHelper.SI + "include", i)
                ),
                new XElement(ModelHelper.SI + "excludes",
                    from e in ExcludeNodes
                    select new XElement(ModelHelper.SI + "exclude", e)
                )
            );
        }
    }

    public class ECMA2Provider : IProvider
    {
        public string DllPath { get; set; }

        public string TypeName { get; set; }

        public Schema Schema { get; set; }

        public List<ProvConfigParameter> ConfigParameters { get; set; }

        public ECMA2RunConfig RunProfile { get; set; }

        Dictionary<string, ECMA2Wrapper> Data { get; set; }

        public void WriteFixture(IFixture write)
        {
            var fix = (ECMA2Fixture)write;

            var assem = Assembly.LoadFrom(DllPath);
            var ecma2Type = assem.GetType(TypeName);

            var ecma2 = (IMAExtensible2CallExport)Activator.CreateInstance(ecma2Type);

            var configParameters = new ConfigCollection();
            foreach (var c in ConfigParameters)
            {
                configParameters.Add(c.Get());
            }

            if (Schema == null)
            {
                Schema = ((IMAExtensible2GetSchema)ecma2).GetSchema(configParameters);
            }

            ecma2.OpenExportConnection(configParameters, Schema, RunProfile.GetExportOpen());
            ecma2.PutExportEntries(new List<CSEntryChange> { new CSEntryChangeProv(fix, this) });
            ecma2.CloseExportConnection(new CloseExportConnectionRunStep());
        }

        public bool TestAssert(IAssertion assertion)
        {
            if (!(assertion is ECMA2Assert))
            {
                throw new ArgumentException("ECMA2Provider only accepts ECMA2Assertions");
            }

            var assert = (ECMA2Assert)assertion;

            if (Data == null || assert.ForceImport)
            {
                Data = new Dictionary<string, ECMA2Wrapper>();
                var assem = Assembly.LoadFrom(DllPath);
                var ecma2Type = assem.GetType(TypeName);

                var ecma2 = (IMAExtensible2CallImport)Activator.CreateInstance(ecma2Type);

                var configParameters = new ConfigCollection();
                foreach (var c in ConfigParameters)
                {
                    configParameters.Add(c.Get());
                }

                if (Schema == null)
                {
                    Schema = ((IMAExtensible2GetSchema)ecma2).GetSchema(configParameters);
                }

                var res = ecma2.OpenImportConnection(configParameters, Schema, RunProfile.GetImportOpen(null));
                string cDat = res.CustomData;
                while (true)
                {
                    var ires = ecma2.GetImportEntries(new GetImportEntriesRunStep(new List<CSEntryChange>(), cDat));
                    cDat = ires.CustomData;

                    foreach (var entry in ires.CSEntries)
                    {
                        Data[entry.DN] = new ECMA2Wrapper(entry);
                    }

                    if (!ires.MoreToImport)
                    {
                        break;
                    }
                }
            }

            switch (assert.Operation)
            {
                case "Exists":
                    return Data.ContainsKey(ECMA2Fixture.HandleVal(assert.DN).ToString());
                case "NotExists":
                    return !Data.ContainsKey(ECMA2Fixture.HandleVal(assert.DN).ToString());
                case "HasValue":
                    var entry = Data[ECMA2Fixture.HandleVal(assert.DN).ToString()];
                    // iterate fields, fail if any don't match
                    foreach (var field in assert.Fields)
                    {
                        if (!Evaluate.TestAssertVal(field, entry, this))
                        {
                            throw new FormatException(String.Format("Field {0}:{1} does not match {2}",
                                    field.Field, entry[field.Field].Value, field.Value));
                        }
                    }

                    // iterate multifields, fail if any don't contain the value
                    foreach (var multi in assert.MultiFields)
                    {
                        if (!Evaluate.TestAssertVal(multi, entry, this))
                        {
                            throw new FormatException(String.Format("Field {0} does not match {1}",
                                    multi.Field, multi.Value));
                        }
                    }

                    return true;
            }

            return false;
        }

        public void Initialize(XElement xml)
        {
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "dllPath", typeof(ECMA2Provider));
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "typeName", typeof(ECMA2Provider));
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "config", typeof(ECMA2Provider));
            DllPath = xml.Element(ModelHelper.SI + "dllPath").Value;
            TypeName = xml.Element(ModelHelper.SI + "typeName").Value;

            if (xml.Elements(ModelHelper.SI + "runProfile").Count() > 0)
            {
                RunProfile = new ECMA2RunConfig(xml.Element(ModelHelper.SI + "runProfile"));
            }
            else
            {
                RunProfile = new ECMA2RunConfig();
            }

            foreach (var p in xml.Element(ModelHelper.SI + "config").Elements(ModelHelper.SI + "param"))
            {
                ModelHelper.RequiredAttribute(p, "name", typeof(ECMA2Provider));
                var add = new ProvConfigParameter();
                add.Name = p.Attribute("name").Value;
                add.Value = p.Value;
                if (p.Attributes("isSecure").Count() > 0 &&
                    (p.Attribute("isSecure").Value.ToLower() == "true" || p.Attribute("isSecure").Value == "1"))
                {
                    add.SecureValue = new SecureString();
                    foreach (var c in add.Value)
                    {
                        add.SecureValue.AppendChar(c);
                    }
                }

                ConfigParameters.Add(add);
            }
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "provider",
                new XAttribute(ModelHelper.XSI + "type", "ECMA2Provider"),
                new XElement(ModelHelper.SI + "dllPath", DllPath),
                new XElement(ModelHelper.SI + "typeName", TypeName),
                RunProfile.GetXml(),
                new XElement(ModelHelper.SI + "config",
                    from p in ConfigParameters
                    select p.GetXml()
                )
            );
        }
    }

    public class ECMA2Assert : Assertion
    {
        public string DN { get; set; }
        public List<AssertField> MultiFields { get; set; }
        public bool ForceImport { get; set; }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);
            DN = xml.Element(ModelHelper.SI + "DN").Value;

            if (xml.Elements(ModelHelper.SI + "forceImport").Count() > 0)
            {
                ForceImport = bool.Parse(xml.Element(ModelHelper.SI + "forceImport").Value);
            }

            foreach (var f in xml.Elements(ModelHelper.SI + "multifield"))
            {
                var t = AssertFieldType.Value;
                if (f.Attributes("type").Count() > 0)
                {
                    try
                    {
                        t = (AssertFieldType)Enum.Parse(typeof(AssertFieldType), f.Attribute("type").Value);
                    }
                    catch (ArgumentException)
                    {
                        throw new XmlParseException(String.Format("Error parsing Assertion {0}. Invalid type {1}.", Name, f.Attribute("type").Value), xml, typeof(ECMA2Assert));
                    }
                }

                MultiFields.Add(new AssertField { Type = t, Value = f.Value, Field = f.Attribute("name").Value });
            }
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            ret.AddFirst(new XElement(ModelHelper.SI + "forceImport", ForceImport));
            ret.AddFirst(new XElement(ModelHelper.SI + "DN", DN));

            
            foreach (var multi in MultiFields)
            {
                var add = new XElement(ModelHelper.SI + "multifield",
                    new XAttribute("name", multi.Field),
                    multi.Value);
                if (multi.Type == AssertFieldType.Regex)
                {
                    add.Add(new XAttribute("type", "Regex"));
                }

                ret.Add(add);
            }

            return ret;
        }
    }

    public class ECMA2Fixture : Fixture
    {
        public string DN { get; set; }
        public string RDN { get; set; }
        public string ObjectType { get; set; }
        public ObjectModificationType ModificationType { get; set; }

        public List<MultiField> Multifields { get; set; }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);

            ModificationType = (ObjectModificationType)Enum.Parse(typeof(ObjectModificationType), Operation);

            if (!new List<string> { "Add", "Update", "Delete", "Replace" }.Contains(Operation))
            {
                throw new XmlParseException(String.Format("ECMA2Fixture does not support {0} operation", Operation), xml, typeof(ECMA2Fixture));
            }

            if (System.Provider.GetType() != typeof(ECMA2Provider))
            {
                throw new XmlParseException("ECMA2Fixture requires an ECMA2Provider system", xml, typeof(ECMA2Fixture));
            }

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "DN", typeof(ECMA2Fixture));
            DN = xml.Element(ModelHelper.SI + "DN").Value;

            if (xml.Elements(ModelHelper.SI + "RDN").Count() > 0)
            {
                RDN = xml.Element(ModelHelper.SI + "DN").Value;
            }
            else
            {
                RDN = DN;
            }

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "objectType", typeof(ECMA2Fixture));
            DN = xml.Element(ModelHelper.SI + "objectType").Value;

            foreach (var f in xml.Elements(ModelHelper.SI + "multifield"))
            {
                var add = new MultiField(f);
                Multifields.Add(add);
            }
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            ret.Add(new XElement(ModelHelper.SI + "DN", DN));
            if (!String.IsNullOrEmpty(RDN))
            {
                ret.Add(new XElement(ModelHelper.SI + "RDN", RDN));
            }

            ret.Add(new XElement(ModelHelper.SI + "objectType", ObjectType));

            return ret;
        }
    }
}
