﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Security;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.FIMTest.ProviderModel;
using SoftwareIDM.FIMTest.Provider;

namespace SoftwareIDM.FIMTest.Provider.ECMA2
{
    public class AnchorCollection : KeyedCollection<string, AnchorAttribute>
    {
        protected override string GetKeyForItem(AnchorAttribute item)
        {
            return item.Name;
        }
    }

    public class SchemaCollection : KeyedCollection<string, SchemaType>
    {
        protected override string GetKeyForItem(SchemaType item)
        {
            return item.Name;
        }
    }


    public class ChangeCollection : KeyedCollection<string, AttributeChange>
    {
        protected override string GetKeyForItem(AttributeChange item)
        {
            return item.Name;
        }
    }

    public class ConfigCollection : KeyedCollection<string, ConfigParameter>
    {
        protected override string GetKeyForItem(ConfigParameter item)
        {
            return item.Name;
        }
    }

    public class SchemaAttrCollection : KeyedCollection<string, SchemaAttribute>
    {
        protected override string GetKeyForItem(SchemaAttribute item)
        {
            return item.Name;
        }
    }

    public class CSEntryChangeProv : CSEntryChange
    {
        static readonly Dictionary<ObjectModificationType, AttributeModificationType> AttributeModificationRules = new Dictionary<ObjectModificationType, AttributeModificationType>()
        {
          { ObjectModificationType.Add, AttributeModificationType.Add },
          { ObjectModificationType.Delete, AttributeModificationType.Delete },
          { ObjectModificationType.Replace, AttributeModificationType.Add },
          { ObjectModificationType.Update, AttributeModificationType.Update }
        };

        KeyedCollection<string, AnchorAttribute> anchorAttributes;
        public override KeyedCollection<string, AnchorAttribute> AnchorAttributes
        {
            get { return anchorAttributes; }
        }

        KeyedCollection<string, AttributeChange> attributeChanges;
        public override KeyedCollection<string, AttributeChange> AttributeChanges
        {
            get { return attributeChanges; }
        }

        public override IList<string> ChangedAttributeNames
        {
            get
            {
                return (from v in attributeChanges
                 select v.Name).ToList();
            }
        }

        public override string DN { get; set; }

        public override MAExportError ErrorCodeExport { get; set; }

        public override MAImportError ErrorCodeImport { get; set; }

        Guid identifier = Guid.NewGuid();
        public override Guid Identifier
        {
            get { return identifier; }
        }

        public override ObjectModificationType ObjectModificationType { get; set; }

        public override string ObjectType { get; set; }

        string rdn;
        public override string RDN
        {
            get { return rdn; }
        }

        public CSEntryChangeProv(ECMA2Fixture fixture, ECMA2Provider provider)
        {
            DN = fixture.DN;
            rdn = fixture.RDN;
            ObjectModificationType = fixture.ModificationType;

            anchorAttributes = new AnchorCollection();
            foreach (var attr in provider.Schema.Types[fixture.ObjectType].AnchorAttributes)
            {
                foreach (var f in fixture.Fields)
                {
                    if (f.Key == attr.Name)
                    {
                        anchorAttributes.Add(new AnchorAttributeProv(attr.Name, f.Value));
                    }
                }
            }

            attributeChanges = new ChangeCollection();
            foreach (var f in fixture.Fields)
            {
                attributeChanges.Add(new AttributeChangeProv(f.Key, f.Value, AttributeModificationRules[ObjectModificationType]));
            }

            foreach (var f in fixture.Multifields)
            {
                attributeChanges.Add(new AttributeChangeProv(f.Name, f.Values, AttributeModificationRules[ObjectModificationType]));
            }
        }
    }

    public class ECMA2Wrapper : IDictionary<string, AttributeValue>
    {
        CSEntryChange data;

        public ECMA2Wrapper(CSEntryChange csentry)
        {
            data = csentry;
        }

        public AttributeValue this[string key]
        {
            get
            {
                if (key == "DN")
                {
                    return new AttributeValue(data.DN);
                }
                else if (key == "RDN")
                {
                    return new AttributeValue(data.RDN);
                }
                else if (key == "ObjectType")
                {
                    return new AttributeValue(data.ObjectType);
                }

                if (data.AttributeChanges.Contains(key))
                {
                    var field = data.AttributeChanges[key];
                    if (!field.IsMultiValued)
                    {
                        return new AttributeValue((from v in field.ValueChanges
                                                   where v.ModificationType != ValueModificationType.Delete
                                                   select v).First().Value);
                    }
                    else
                    {
                        return AttributeValue.MultiValue(from v in field.ValueChanges
                                                         where v.ModificationType != ValueModificationType.Delete
                                                         select (IComparable)v.Value, MultiBehavior.Replace);
                    }
                }

                return null;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #region Unimplemented
        public void Add(string key, AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(string key)
        {
            throw new NotImplementedException();
        }

        public ICollection<string> Keys
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(string key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(string key, out AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public ICollection<AttributeValue> Values
        {
            get { throw new NotImplementedException(); }
        }

        public void Add(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<string, AttributeValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<string, AttributeValue>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class AnchorAttributeProv : AnchorAttribute
    {
        string _name;
        object _value;

        public AnchorAttributeProv(string name, string value)
        {
            _name = name;
            _value = Fixture.HandleVal(value);
        }

        public override Microsoft.MetadirectoryServices.AttributeType DataType
        {
            get
            {
                return (Microsoft.MetadirectoryServices.AttributeType)Enum.Parse(
                    typeof(Microsoft.MetadirectoryServices.AttributeType),
                    new AttributeValue(_value).DataType.ToString()
                );
            }
        }

        public override string Name
        {
            get { return _name; }
        }

        public override object Value
        {
            get { return _value; }
        }
    }

    public class AttributeChangeProv : AttributeChange
    {
        string _name;
        object _value;
        bool _multi;
        AttributeModificationType _modType;
        List<object> _values;

        public AttributeChangeProv(string name, string value, AttributeModificationType modType)
        {
            _name = name;
            _value = Fixture.HandleVal(value);
            _modType = modType;
        }

        public AttributeChangeProv(string name, List<string> values, AttributeModificationType modType)
        {
            _name = name;
            _multi = true;
            _modType = modType;
            _values = new List<object>();
            foreach (var val in values)
            {
                _values.Add(Fixture.HandleVal(val));
            }
        }

        public override Microsoft.MetadirectoryServices.AttributeType DataType
        {
            get
            {
                if (_multi)
                {
                    if (_values.Count == 0)
                    {
                        return Microsoft.MetadirectoryServices.AttributeType.String;
                    }

                    return (Microsoft.MetadirectoryServices.AttributeType)Enum.Parse(
                    typeof(Microsoft.MetadirectoryServices.AttributeType),
                    new AttributeValue(_values[0]).DataType.ToString()
                );
                }

                return (Microsoft.MetadirectoryServices.AttributeType)Enum.Parse(
                    typeof(Microsoft.MetadirectoryServices.AttributeType),
                    new AttributeValue(_value).DataType.ToString()
                );
            }
        }

        public override bool IsMultiValued
        {
            get { return _multi; }
        }

        public override AttributeModificationType ModificationType
        {
            get { return _modType; }
        }

        public override string Name
        {
            get { return _name; }
        }

        public override IList<ValueChange> ValueChanges
        {
            get
            {
                if (!_multi)
                {
                    return new List<ValueChange> {
                        ModificationType == AttributeModificationType.Delete ? ValueChange.CreateValueDelete(_value) : ValueChange.CreateValueAdd(_value)
                    };
                }
                else
                {
                    return (from v in _values
                            select ModificationType == AttributeModificationType.Delete ? ValueChange.CreateValueDelete(v) : ValueChange.CreateValueAdd(v)).ToList();
                }
            }
        }
    }

    public class AttributeProv : Attrib
    {
        public AttributeProv(string name, string value)
        {
            _name = name;
            _multi = false;
            _value = Fixture.HandleVal(value);
        }

        public AttributeProv(string name, List<string> values)
        {
            _name = name;
            _multi = true;
            _values = new List<object>();
            foreach (var val in values)
            {
                _values.Add(Fixture.HandleVal(val));
            }
        }

        string _name;
        object _value;
        List<object> _values;
        bool _multi;

        public override byte[] BinaryValue
        {
            get { return new AttributeValue(_value).BinaryValue; }
            set { _value = value; }
        }

        public override bool BooleanValue
        {
            get { return new AttributeValue(_value).BooleanValue; }
            set { _value = value; }
        }

        public override Microsoft.MetadirectoryServices.AttributeType DataType
        {
            get {
                return (Microsoft.MetadirectoryServices.AttributeType)Enum.Parse(
                    typeof(Microsoft.MetadirectoryServices.AttributeType),
                    new AttributeValue(_value).DataType.ToString()
                );
            }
        }

        public override void Delete()
        {
            _value = null;
        }

        public override long IntegerValue
        {
            get { return new AttributeValue(_value).IntegerValue; }
            set { _value = value; }
        }

        public override bool IsMultivalued
        {
            get { return _multi; }
        }

        public override bool IsPresent
        {
            get { return _value != null; }
        }

        public override ManagementAgent LastContributingMA
        {
            get { return null; }
        }

        public override DateTime LastContributionTime
        {
            get { return DateTime.UtcNow; }
        }

        public override string Name
        {
            get { return _name; }
        }

        public override ReferenceValue ReferenceValue
        {
            get
            {
                if (_value is ReferenceValue)
                {
                    return (ReferenceValue)_value;
                }

                return new ReferenceValueProv(_value);
            }
            set
            {
                _value = value;
            }
        }

        public override string StringValue
        {
            get
            {
                return _value.ToString();
            }
            set
            {
                _value = value;
            }
        }

        public override string Value
        {
            get
            {
                return _value.ToString();
            }
            set
            {
                _value = value;
            }
        }

        public override ValueCollection Values
        {
            get
            {
                return new ValueCollectionProv(_values);
            }
            set
            {
                _value = value;
            }
        }
    }

    public class ReferenceValueProv : ReferenceValue
    {
        public ReferenceValueProv(object dn)
        {
            _dn = dn;
        }

        public ReferenceValueProv(params string[] dn)
        {
            _dn = String.Join("", dn);
        }

        object _dn;

        public override ReferenceValue Concat(string DN)
        {
            return new ReferenceValueProv(_dn.ToString(), DN);
        }

        public override ReferenceValue Concat(ReferenceValue DN)
        {
            return new ReferenceValueProv(_dn.ToString(), DN.ToString());
        }

        public override int Depth
        {
            get { throw new NotImplementedException(); }
        }

        public override ReferenceValue Parent(int skipLevels)
        {
            throw new NotImplementedException();
        }

        public override ReferenceValue Parent()
        {
            throw new NotImplementedException();
        }

        public override ReferenceValue Subcomponents(int startingComponent, int endingComponent)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return _dn.ToString();
        }

        public override string this[int componentIndex]
        {
            get { throw new NotImplementedException(); }
        }

        public override Microsoft.MetadirectoryServices.AttributeType DataType
        {
            get { throw new NotImplementedException(); }
        }

        public override bool Equals(object obj)
        {
            return new AttributeValue(_dn).Equals(new AttributeValue(obj));
        }

        public override int GetHashCode()
        {
            return _dn.GetHashCode();
        }

        public override long ToInteger()
        {
            throw new InvalidOperationException("cannot convert DN to integer");
        }

        public override byte[] ToBinary()
        {
            throw new InvalidOperationException("cannot convert DN to binary");
        }

        public override bool ToBoolean()
        {
            throw new InvalidOperationException("cannot convert DN to boolean");
        }
    }

    public class ValueProv : Value
    {
        public ValueProv(object val)
        {
            _value = val;
        }

        object _value;
        public override Microsoft.MetadirectoryServices.AttributeType DataType
        {
            get { return (Microsoft.MetadirectoryServices.AttributeType)Enum.Parse(typeof(Microsoft.MetadirectoryServices.AttributeType), new AttributeValue(_value).DataType.ToString()); }
        }

        public override bool Equals(object obj)
        {
            return new AttributeValue(_value).Equals(new AttributeValue(obj));
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override byte[] ToBinary()
        {
            return new AttributeValue(_value).BinaryValue;
        }

        public override bool ToBoolean()
        {
            return new AttributeValue(_value).BooleanValue;
        }

        public override long ToInteger()
        {
            return new AttributeValue(_value).IntegerValue;
        }

        public override string ToString()
        {
            return new AttributeValue(_value).ToString();
        }

        public object Value { get { return _value; } set { _value = value; } }
    }

    public class ValueCollectionProv : ValueCollection
    {

        List<Value> _data = new List<Value>();

        public ValueCollectionProv()
        {

        }

        public ValueCollectionProv(List<object> data)
        {
            _data = (from v in data
                     select (Value)new ValueProv(v)).ToList();
        }

        public override void Add(ValueCollection val)
        {
            _data.AddRange(((ValueCollectionProv)val)._data);
        }

        public override void Add(Value val)
        {
            _data.Add(val);
        }

        public override void Add(byte[] val)
        {
            _data.Add(new ValueProv(val));
        }

        public override void Add(long val)
        {
            _data.Add(new ValueProv(val));
        }

        public override void Add(string val)
        {
            _data.Add(new ValueProv(val));
        }

        public override void Clear()
        {
            _data.Clear();
        }

        public override bool Contains(Value val)
        {
            return _data.Contains(((ValueProv)val).Value);
        }

        public override bool Contains(byte[] val)
        {
            return _data.Contains(new ValueProv(val));
        }

        public override bool Contains(long val)
        {
            return _data.Contains(new ValueProv(val));
        }

        public override bool Contains(string val)
        {
            return _data.Contains(new ValueProv(val));
        }

        public override int Count
        {
            get { return _data.Count; }
        }

        public override ValueCollectionEnumerator GetEnumerator()
        {
            return new ValueCollectionEnumeratorProv(_data);
        }

        public override void Remove(Value val)
        {
            _data.Remove(val);
        }

        public override void Remove(byte[] val)
        {
            _data.Remove(new ValueProv(val));
        }

        public override void Remove(long val)
        {
            _data.Remove(new ValueProv(val));
        }

        public override void Remove(string val)
        {
            _data.Remove(new ValueProv(val));
        }

        public override void Set(bool val)
        {
            throw new NotImplementedException();
        }

        public override long[] ToIntegerArray()
        {
            throw new NotImplementedException();
        }

        public override string[] ToStringArray()
        {
            throw new NotImplementedException();
        }

        public override Value this[int index]
        {
            get { return new ValueProv(_data[index]); }
        }
    }

    public class ValueCollectionEnumeratorProv : ValueCollectionEnumerator
    {
        List<Value>.Enumerator enumer;
        List<Value> _data;

        public ValueCollectionEnumeratorProv(List<Value> data)
        {
            _data = data;
            enumer = _data.GetEnumerator();
        }

        public override Value Current
        {
            get { return enumer.Current; }
        }

        public override bool MoveNext()
        {
            return enumer.MoveNext();
        }

        public override void Reset()
        {
            enumer = _data.GetEnumerator();
        }
    }
}
