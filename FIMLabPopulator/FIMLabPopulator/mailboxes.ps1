﻿
if ((Get-PSSnapin -Name Microsoft.Exchange.Management.PowerShell.Admin -ErrorAction SilentlyContinue) -eq $null)
{
	Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin
}

$groups = Import-Csv .\ad_export\create_group_mail.txt -delimiter "`t"
foreach($group in $groups){
	Enable-DistributionGroup -Identity $group.dn -Alias $group.nickname
	Set-DistributionGroup $group.nickname -EmailAddressPolicyEnabled $false -PrimarySMTPAddress $group.mail
	break
}

$users = Import-Csv .\ad_export\create_user_mail.txt -delimiter "`t"
foreach($user in $users){
	Enable-Mailbox -Identity $user.dn -Database $user.database -Alias $user.nickname
	Set-Mailbox $user.nickname -EmailAddressPolicyEnabled $false -PrimarySmtpAddress $user.mail
	break
}
