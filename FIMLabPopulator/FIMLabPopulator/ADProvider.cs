﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace FIMLabPopulator
{

    public interface IProvider
    {
        void Write(IFixture write);
    }

    public interface IFixture
    {
        string name { get; set; }
        IProvider provider { get; set; }
        FixtureType type { get; set; }

        void Load();
    }

    public enum FixtureType
    {
        Add,
        Update,
        Delete,
        Erase,
        Move
    }


    public class Systems : ModelDictionary<ConnectSystem>
    {
        public Systems(XElement xml)
        {
            data = new Dictionary<string, ConnectSystem>();
            Add(xml);

            if (xml.Attributes("path").Count() > 0)
            {
                foreach (string path in xml.Attribute("path").Value.Split(';'))
                {
                    Add(XElement.Load(path.Trim()));
                }
            }
        }

        public void Add(XElement xml)
        {
            foreach (var sys in xml.Elements("system"))
            {
                data.Add(sys.Attribute("name").Value, new ConnectSystem(sys));
            }
        }
    }

    public class ConnectSystem
    {
        public IProvider provider { get; set; }
        public string name { get; set; }

        public ConnectSystem(XElement xml)
        {
            name = xml.Attribute("name").Value;

            if (xml.Elements("ADProvider").Count() > 0)
            {
                provider = new ADProvider(xml.Element("ADProvider"));
            }
        }
    }

    public struct Acl
    {
        public string guid { get; set; }
        public int flags { get; set; }
        public int aceType { get; set; }
        public string trustee { get; set; }
        public int accessMask { get; set; }
    }

    public class ADFixture : IFixture
    {
        public string name { get; set; }
        public IProvider provider { get; set; }
        public FixtureType type { get; set; }

        // AD specific attributes
        public string DN { get; set; }
        public string NewDN { get; set; }
        public string schemaClassName { get; set; }
        public string sAMAccountName { get; set; }
        public string unicodePwd { get; set; }
        public int userAccountControl { get; set; }
        public int groupType { get; set; }

        public Dictionary<string, object> fields { get; set; }
        public Dictionary<string, List<object>> multifields { get; set; }
        public List<Acl> acls { get; set; }

        public ADFixture()
        {
            fields = new Dictionary<string, object>();
            multifields = new Dictionary<string, List<object>>();
            acls = new List<Acl>();
        }

        const string SidSearchFormat = "{0}<SID={1}>";

        public ADFixture(ADProvider provider, SearchResult result, 
            List<string> readFields, List<string> readMultiFields, List<string> readAcls)
        {
            this.provider = provider;
            fields = new Dictionary<string, object>();
            multifields = new Dictionary<string, List<object>>();
            acls = new List<Acl>();

            var entry = result.GetDirectoryEntry();
            var p = entry.Path;
            DN = entry.Path.Substring(Regex.Match(entry.Path, "CN=|OU=|DC=").Index);
            schemaClassName = entry.SchemaClassName;
            if (schemaClassName.ToLower() == "user")
            {
                unicodePwd = "";
                userAccountControl = int.Parse(entry.Properties["userAccountControl"].Value.ToString());
                sAMAccountName = entry.Properties["sAMAccountName"].Value as string;
            } 
            else if (schemaClassName.ToLower() == "group")
            {
                groupType = int.Parse(entry.Properties["groupType"].Value.ToString());
                sAMAccountName = entry.Properties["sAMAccountName"].Value as string;
            }

            foreach (string field in readFields)
            {
                if (new List<string> { "unicodePwd", "userAccountControl", "sAMAccountName", "groupType", "cn" }.Contains(field))
                {
                    continue;
                }

                else if (field == "accountExpires")
                {
                    var exp = (long)result.Properties[field][0];
                    if (exp == long.MaxValue)
                    {
                        continue;
                    }

                    fields[field] = exp;
                    continue;
                }

                fields[field] = entry.Properties[field].Value;
            }

            foreach (string field in readMultiFields)
            {
                multifields[field] = new List<object>();
                foreach (object f in entry.Properties[field])
                {
                    multifields[field].Add(f);
                }
            }

            foreach (var acl in readAcls)
            {
                ActiveDs.SecurityDescriptor sd = (ActiveDs.SecurityDescriptor)entry.Properties["ntSecurityDescriptor"].Value;
                ActiveDs.AccessControlList dacl = (ActiveDs.AccessControlList)sd.DiscretionaryAcl;
                foreach (ActiveDs.AccessControlEntry ace in dacl)
                {
                    if (ace.ObjectType != null && ace.ObjectType.ToUpper() == acl.ToUpper() && 
                        ace.Trustee != "NT AUTHORITY\\SELF")
                    {
                        try
                        {
                            string trustee;
                            if (ace.Trustee.StartsWith("S-1-"))
                            {
                                var e = String.IsNullOrEmpty(provider.user) ?
                                    new DirectoryEntry(String.Format(SidSearchFormat, provider.connectionString, ace.Trustee)) :
                                    new DirectoryEntry(String.Format(SidSearchFormat, provider.connectionString, ace.Trustee),
                                        provider.user, provider.password);

                                trustee = e.Properties["distinguishedName"].Value.ToString();
                            }
                            else
                            {
                                trustee = ace.Trustee;
                            }

                            acls.Add(new Acl
                            {
                                guid = acl,
                                trustee = trustee,
                                flags = ace.Flags,
                                aceType = ace.AceType,
                                accessMask = ace.AccessMask
                            });
                        }
                        catch (DirectoryServicesCOMException e)
                        {
                            Console.WriteLine("Error: {0} for {1}", e.Message, ace.Trustee);
                        }
                    }
                }
            }
        }

        string Format(object val)
        {
            if (val == null)
            {
                return "[null]";
            }

            var ret = val.ToString();
            ret = ret.Replace(((ADProvider)provider).dnRoot, "[dnRoot]");

            return ret;
        }

        public XElement GetXML()
        {
            var ret = new XElement("ADFixture", new XAttribute("name", DN), new XAttribute("type", "Add"),
                new XElement("DN", Format(DN)), new XElement("schemaClassName", schemaClassName));
            if (schemaClassName.ToLower() == "user")
            {
                ret.Add(new XElement("sAMAccountName", sAMAccountName));
                ret.Add(new XElement("userAccountControl", userAccountControl));
                ret.Add(new XElement("unicodePwd", ""));
            }
            else if (schemaClassName.ToLower() == "group")
            {
                ret.Add(new XElement("sAMAccountName", sAMAccountName));
                ret.Add(new XElement("groupType", groupType));
            }

            foreach (var field in fields.Keys)
            {
                if (fields[field] == null)
                {
                    continue;
                }

                ret.Add(new XElement("field", new XAttribute("name", field), Format(fields[field])));
            }

            foreach (var field in multifields.Keys)
            {
                ret.Add(new XElement("multifield", new XAttribute("name", field),
                    from m in multifields[field] select new XElement("value", Format(m))));
            }

            foreach (var acl in acls)
            {
                ret.Add(new XElement("acl",
                    new XAttribute("guid", acl.guid),
                    new XAttribute("trustee", Format(acl.trustee)),
                    new XAttribute("flags", acl.flags),
                    new XAttribute("acetype", acl.aceType),
                    new XAttribute("accessMask", acl.accessMask)));
            }

            return ret;
        }

        public ADFixture(XElement xml, ADProvider provider)
        {
            type = (FixtureType)Enum.Parse(typeof(FixtureType), xml.Attribute("type").Value);
            this.provider = provider;

            fields = new Dictionary<string, object>();
            multifields = new Dictionary<string, List<object>>();
            acls = new List<Acl>();

            DN = xml.Element("DN").Value;
            if (DN.Contains("[dnRoot]"))
            {
                DN = DN.Replace("[dnRoot]", provider.dnRoot);
            }

            if (xml.Elements("NewDN").Count() > 0)
            {
                NewDN = xml.Element("NewDN").Value;
            }

            if (xml.Elements("schemaClassName").Count() > 0)
            {
                schemaClassName = xml.Element("schemaClassName").Value;
            }

            if (xml.Elements("sAMAccountName").Count() > 0)
            {
                sAMAccountName = xml.Element("sAMAccountName").Value;
            }

            if (xml.Elements("groupType").Count() > 0)
            {
                var t = xml.Element("groupType").Value;
                if (Regex.IsMatch(t, "-?\\d+"))
                {
                    groupType = int.Parse(t);
                }
                else
                {
                    switch (t.ToLower())
                    {
                        case "global distribution":
                            groupType = 2;
                            break;
                        case "domain local distribution":
                            groupType = 4;
                            break;
                        case "universal distribution":
                            groupType = 8;
                            break;
                        case "global security":
                            groupType = -2147483646;
                            break;
                        case "domain local security":
                            groupType = -2147483644;
                            break;
                        default: // "universal security"
                            groupType = -2147483640;
                            break;
                    }
                }
            }

            if (xml.Elements("unicodePwd").Count() > 0)
            {
                unicodePwd = xml.Element("unicodePwd").Value;
            }

            if (xml.Elements("userAccountControl").Count() > 0)
            {
                try
                {
                    userAccountControl = int.Parse(xml.Element("userAccountControl").Value);
                }
                catch
                {
                    throw new FormatException(String.Format("Invalid format for userAccountControl: {0}", xml.Element("userAccountControl").Value));
                }
            }

            foreach (var f in xml.Elements("field"))
            {
                fields.Add(f.Attribute("name").Value, HandleVal(f.Value));
            }

            foreach (var f in xml.Elements("multifield"))
            {
                var add = new List<object>();
                add.AddRange(from m in f.Elements("value")
                             select HandleVal(m.Value));
                multifields.Add(f.Attribute("name").Value, add);
            }

            foreach (var a in xml.Elements("acl"))
            {
                acls.Add(new Acl
                {
                    guid = a.Attribute("guid").Value,
                    trustee = (string)HandleVal(a.Attribute("trustee").Value),
                    flags = int.Parse(a.Attribute("flags").Value),
                    aceType = int.Parse(a.Attribute("acetype").Value),
                    accessMask = int.Parse(a.Attribute("accessMask").Value)
                });
            }
        }

        public void PreCreate()
        {
            ((ADProvider)provider).ThinCreate(this);
        }

        public void Load()
        {
            provider.Write(this);
        }

        object HandleVal(string val)
        {
            if (val.ToLower() == "[null]")
            {
                return null;
            }

            val = val.Replace("[dnRoot]", ((ADProvider)provider).dnRoot);

            return val;
        }
    }


    public abstract class ModelDictionary<T> : IDictionary<string, T>
    {
        protected Dictionary<string, T> data { get; set; }

        public void Add(string key, T value)
        {
            data.Add(key, value);
        }

        public bool ContainsKey(string key)
        {
            return data.ContainsKey(key);
        }

        public ICollection<string> Keys
        {
            get { return data.Keys; }
        }

        public bool Remove(string key)
        {
            return data.Remove(key);
        }

        public bool TryGetValue(string key, out T value)
        {
            return data.TryGetValue(key, out value);
        }

        public ICollection<T> Values
        {
            get { return data.Values; }
        }

        public T this[string key]
        {
            get
            {
                try
                {
                    return data[key];
                }
                catch (KeyNotFoundException)
                {
                    throw new KeyNotFoundException(String.Format("Unable to find {0} named {1}.",
                        typeof(T).Name, key));
                }
            }
            set
            {
                data[key] = value;
            }
        }

        public void Add(KeyValuePair<string, T> item)
        {
            data.Add(item.Key, item.Value);
        }

        public void Clear()
        {
            data.Clear();
        }

        public bool Contains(KeyValuePair<string, T> item)
        {
            return data.Contains(item);
        }

        public void CopyTo(KeyValuePair<string, T>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return data.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(KeyValuePair<string, T> item)
        {
            return data.Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<string, T>> GetEnumerator()
        {
            return data.GetEnumerator();
        }

        global::System.Collections.IEnumerator global::System.Collections.IEnumerable.GetEnumerator()
        {
            return data.GetEnumerator();
        }
    }

    public class ADProvider : IProvider
    {
        public string connectionString { get; set; }
        public string user { get; set; }
        public string password { get; set; }
        public string dnRoot { get; set; }

        public ADProvider(XElement xml)
        {
            connectionString = xml.Element("connectionString").Value;
            if (xml.Elements("user").Count() > 0 && xml.Elements("password").Count() > 0)
            {
                user = xml.Element("user").Value;
                password = xml.Element("password").Value;
            }

            if (xml.Elements("dnRoot").Count() > 0)
            {
                dnRoot = xml.Element("dnRoot").Value;
            }
        }

        public void ThinCreate(ADFixture fixture)
        {
            Regex parseDN = new Regex(@"^((?:CN|OU)=.*?)(?<!\\),(.*)$");
            var res = parseDN.Match(fixture.DN).Groups;
            string cn = res[1].Value;
            string container = res[2].Value;
            DirectoryEntry parent = this.user == null ?
                new DirectoryEntry(this.connectionString + container) :
                new DirectoryEntry(this.connectionString + container, this.user, this.password);

            if (!DirectoryEntry.Exists(parent.Path))
            {
                var addContainer = new ADFixture
                {
                    DN = container,
                    schemaClassName = "OrganizationalUnit",
                    type = FixtureType.Add
                };

                ThinCreate(addContainer);
                ThinCreate(fixture);
                return;
            }

            // Use existing object if already created.
            DirectoryEntry add;
            if (!DirectoryEntry.Exists(this.connectionString + fixture.DN))
            {
                add = parent.Children.Add(cn, fixture.schemaClassName);
            }
            else
            {
                add = parent.Children.Find(cn, fixture.schemaClassName);
            }

            parent.Dispose();

            if (!String.IsNullOrEmpty(fixture.sAMAccountName))
            {
                add.Properties["sAMAccountName"].Value = fixture.sAMAccountName;
            }

            try
            {
                //const long PASSWD_NOTREQD = 0x0020;
                if (fixture.schemaClassName.ToLower() == "user")
                {
                    add.CommitChanges();
                    add.Invoke("SetPassword", new Object[] { fixture.unicodePwd });
                    add.Properties["userAccountControl"].Value = fixture.userAccountControl;
                }
                else if (fixture.schemaClassName.ToLower() == "group")
                {
                    if (fixture.fields.ContainsKey("mail") && !String.IsNullOrEmpty(fixture.fields["mail"] as string))
                    {
                        // make universal group if mail enabled
                        fixture.groupType = fixture.groupType < 0 ? -2147483640 : 8;
                    }

                    add.Properties["groupType"].Value = fixture.groupType;
                }
                add.CommitChanges();
                add.Dispose();
            }
            catch (Exception f)
            {
                Console.WriteLine("Error {0} for {1}", f.Message, fixture.DN);
            }
        }

        void CreateObject(ADFixture fixture)
        {
            Regex parseDN = new Regex(@"^((?:CN|OU)=.*?)(?<!\\),(.*)$");
            var res = parseDN.Match(fixture.DN).Groups;
            string cn = res[1].Value;
            string container = res[2].Value;
            DirectoryEntry parent = this.user == null ?
                new DirectoryEntry(this.connectionString + container) :
                new DirectoryEntry(this.connectionString + container, this.user, this.password);

            if (!DirectoryEntry.Exists(parent.Path))
            {
                var addContainer = new ADFixture
                {
                    DN = container,
                    schemaClassName = "OrganizationalUnit",
                    type = FixtureType.Add
                };

                CreateObject(addContainer);
                CreateObject(fixture);
                return;
            }

            // Use existing object if already created.
            DirectoryEntry add;
            if (!DirectoryEntry.Exists(this.connectionString + fixture.DN))
            {
                add = parent.Children.Add(cn, fixture.schemaClassName);
            }
            else
            {
                add = parent.Children.Find(cn, fixture.schemaClassName);
            }

            parent.Dispose();

            if (!String.IsNullOrEmpty(fixture.sAMAccountName))
            {
                add.Properties["sAMAccountName"].Value = fixture.sAMAccountName;
            }

            foreach (string prop in fixture.fields.Keys)
            {
                if (new List<string> { "unicodePwd", "groupType", "cn", "homeMDB", "mail", "mailNickname" }.Contains(prop))
                {
                    continue;
                }

                add.Properties[prop].Value = fixture.fields[prop];
            }

            foreach (string prop in fixture.multifields.Keys)
            {
                add.Properties[prop].Clear();
                foreach (string val in fixture.multifields[prop])
                {
                    add.Properties[prop].Add(val);
                }
            }

            //const long PASSWD_NOTREQD = 0x0020;
            if (fixture.schemaClassName.ToLower() == "user")
            {
                //add.Properties["userAccountControl"].Value = fixture.userAccountControl | PASSWD_NOTREQD;
                add.CommitChanges();
                add.Invoke("SetPassword", new Object[] { fixture.unicodePwd });
                add.Properties["userAccountControl"].Value = fixture.userAccountControl;
            }
            else if (fixture.schemaClassName.ToLower() == "group")
            {
                add.Properties["groupType"].Value = fixture.groupType;
            }

            add.CommitChanges();
            add.Dispose();
        }

        Dictionary<string, bool> TestedNoExists = new Dictionary<string, bool>();
        private bool DNExists(object path)
        {
            if (path != null && path.ToString().Contains(dnRoot))
            {
                if (TestedNoExists.ContainsKey(path as string))
                {
                    return false;
                }

                var test = this.user == null ?
                    new DirectoryEntry(this.connectionString + path.ToString()) :
                    new DirectoryEntry(this.connectionString + path.ToString(), this.user, this.password);
                try
                {
                    var t = test.NativeObject;
                }
                catch
                {
                    TestedNoExists.Add(path as string, true);
                    Console.WriteLine("Object {0} doesn't exist", path);
                    return false;
                }

                test.Dispose();
            }

            return true;
        }

        void UpdateObject(ADFixture fixture)
        {
            DirectoryEntry entry = this.user == null ?
                new DirectoryEntry(this.connectionString + fixture.DN) :
                new DirectoryEntry(this.connectionString + fixture.DN, this.user, this.password);
            
            try
            {
                var t = entry.NativeObject;
            }
            catch (DirectoryServicesCOMException f)
            {
                Console.WriteLine("Error {0} retrieving {1}", f.Message, fixture.DN);
                return;
            }

            if (!String.IsNullOrEmpty(fixture.NewDN))
            {
                Regex parseDN = new Regex(@"^((?:CN|OU)=.*?)(?<!\\),(.*)$");
                var res = parseDN.Match(fixture.NewDN).Groups;
                string cn = res[1].Value;
                string container = res[2].Value;
                DirectoryEntry parent = this.user == null ?
                    new DirectoryEntry(this.connectionString + container) :
                    new DirectoryEntry(this.connectionString + container, this.user, this.password);

                entry.MoveTo(parent, cn);
                parent.Dispose();
            }

            if (!String.IsNullOrEmpty(fixture.unicodePwd))
            {
                entry.Invoke("SetPassword", new Object[] { fixture.unicodePwd });
            }

            if (!String.IsNullOrEmpty(fixture.sAMAccountName))
            {
                entry.Properties["sAMAccountName"].Value = fixture.sAMAccountName;
            }

            if (fixture.userAccountControl > 0)
            {
                entry.Properties["userAccountControl"].Value = fixture.userAccountControl;
            }
            
            foreach (string prop in fixture.fields.Keys)
            {
                if (new List<string> { "groupType", "unicodePwd", "cn", "homeMDB", "mailNickname", "mail" }.Contains(prop))
                {
                    continue;
                }

                if (fixture.fields[prop] == null || fixture.fields[prop] as string == "")
                {
                    continue;
                }

                entry.Properties[prop].Value = fixture.fields[prop];
            }

            foreach (string prop in fixture.multifields.Keys)
            {
                entry.Properties[prop].Clear();
                foreach (string val in fixture.multifields[prop])
                {
                    entry.Properties[prop].Add(val);
                }
            }
            try
            {
                entry.CommitChanges();
            }
            catch (Exception f)
            {
                if (f.Message.ToLower().Contains("there is no such object on the server"))
                {
                    foreach (string prop in fixture.fields.Keys)
                    {
                        if (new List<string> { "groupType", "unicodePwd", "cn", "homeMDB", "mailNickname", "mail" }.Contains(prop))
                        {
                            continue;
                        }

                        if (!DNExists(fixture.fields[prop]))
                        {
                            continue;
                        }

                        entry.Properties[prop].Value = fixture.fields[prop];
                    }

                    foreach (string prop in fixture.multifields.Keys)
                    {
                        entry.Properties[prop].Clear();
                        foreach (string val in fixture.multifields[prop])
                        {
                            if (!DNExists(val))
                            {
                                continue;
                            }

                            entry.Properties[prop].Add(val);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Error {0} for {1}", f.Message, fixture.DN);
                }
            }

            if (fixture.acls.Count > 0)
            {
                var acls = new Dictionary<string, Dictionary<string, int>>();

                ActiveDs.SecurityDescriptor sd = (ActiveDs.SecurityDescriptor)entry.Properties["ntSecurityDescriptor"].Value;
                ActiveDs.AccessControlList dacl = (ActiveDs.AccessControlList)sd.DiscretionaryAcl;
                foreach (ActiveDs.AccessControlEntry a in dacl)
                {
                    if (!String.IsNullOrEmpty(a.ObjectType))
                    {
                        var hold = new object[] { a.AccessMask, a.AceFlags, a.AceType, a.Flags, a.InheritedObjectType, a.ObjectType, a.Trustee };
                        if (!acls.ContainsKey(a.ObjectType.ToUpper()))
                        {
                            acls[a.ObjectType.ToUpper()] = new Dictionary<string, int>();
                        }

                        if (!acls[a.ObjectType.ToUpper()].ContainsKey(a.Trustee.ToUpper()))
                        {
                            acls[a.ObjectType.ToUpper()].Add(a.Trustee.ToUpper(), a.Flags);
                        }
                    }
                }

                foreach (var acl in fixture.acls)
                {
                    var trustee = acl.trustee;
                    if (Regex.IsMatch(acl.trustee, "CN=|OU="))
                    {
                        var e = String.IsNullOrEmpty(user) ?
                            new DirectoryEntry(connectionString + trustee) : new DirectoryEntry(connectionString + trustee, user, password);
                        try
                        {
                            trustee = new SecurityIdentifier((byte[])e.Properties["objectSid"][0], 0).Value;
                        }
                        catch (Exception f)
                        {
                            Console.WriteLine("Object {0} doesn't exist", trustee);
                        }
                    }

                    if (!acls.ContainsKey(acl.guid.ToUpper()) || !acls[acl.guid].ContainsKey(trustee.ToUpper()) ||
                        acls[acl.guid.ToUpper()][trustee.ToUpper()] != acl.flags)
                    {
                        var ace = new ActiveDs.AccessControlEntryClass();
                        ace.Trustee = trustee;
                        ace.AceType = acl.aceType;
                        ace.AccessMask = acl.accessMask;
                        ace.ObjectType = acl.guid;
                        ace.Flags = acl.flags;
                        dacl.AddAce(ace);
                    }
                }
                try
                {
                    sd.DiscretionaryAcl = dacl;
                    entry.Properties["ntSecurityDescriptor"].Value = sd;
                    entry.CommitChanges();
                }
                catch (Exception f)
                {
                    Console.WriteLine("Error {0} for {1}", f.Message, fixture.DN);
                }

                entry.Dispose();
            }
        }

        void DeleteObject(ADFixture fixture)
        {
            DirectoryEntry entry = this.user == null ?
                new DirectoryEntry(this.connectionString + fixture.DN) :
                new DirectoryEntry(this.connectionString + fixture.DN, this.user, this.password);
            try
            {
                entry.DeleteTree();
            }
            catch (DirectoryServicesCOMException e)
            {
                if (!e.Message.Contains("no such object"))
                    throw e;
            }

            entry.Dispose();
        }

        public void Write(IFixture write)
        {
            if (!(write is ADFixture))
            {
                throw new Exception("ADProvider must take ADFixture");
            }

            switch (write.type)
            {
                case FixtureType.Add:
                    CreateObject((ADFixture)write);
                    break;
                case FixtureType.Update:
                case FixtureType.Move:
                    UpdateObject((ADFixture)write);
                    break;
                case FixtureType.Delete:
                case FixtureType.Erase:
                    DeleteObject((ADFixture)write);
                    break;
            }
        }
    }
}
