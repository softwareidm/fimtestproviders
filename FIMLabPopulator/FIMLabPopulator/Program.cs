﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml.Linq;
using System.DirectoryServices;

namespace FIMLabPopulator
{
    class Program
    {
        static void Usage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("FIMLabPopulator <xmlfile> <import/export>");
        }

        static IEnumerable<XElement> ReadAD(ADProvider provider, string objectClass, 
            List<string> excluded, List<string> fields, List<string> multifields, List<string> acls)
        {
            var entry = !String.IsNullOrEmpty(provider.user) ?
                new DirectoryEntry(provider.connectionString + provider.dnRoot, provider.user, provider.password) :
                new DirectoryEntry(provider.connectionString + provider.dnRoot);

            DirectorySearcher search;
            if (objectClass.ToLower() == "user")
            {
                search = new DirectorySearcher(entry, String.Format("(objectClass={0})", objectClass), new string[] { "accountExpires" });
            }
            else
            {
                search = new DirectorySearcher(entry, String.Format("(objectClass={0})", objectClass));
            }

            search.PageSize = 500;
            search.SearchScope = SearchScope.Subtree;
            search.ReferralChasing = ReferralChasingOption.All;

            bool skip = false;
            foreach (SearchResult result in search.FindAll())
            {
                foreach (var exclude in excluded)
                {
                    if (result.Path.Contains(exclude))
                    {
                        skip = true;
                        break;
                    }
                }

                if (objectClass.ToLower() == "group")
                {
                    if ((int)result.GetDirectoryEntry().Properties["groupType"].Value == -2147483643)
                        skip = true;
                }
                
                if (skip)
                {
                    skip = false;
                    continue;
                }

                yield return new ADFixture(provider, result, fields, multifields, acls).GetXML();
            }
        }

        static void Export(XElement xml)
        {
            Console.WriteLine("Exporting Active Directory");
            var export = xml.Element("export");
            var systems = new Systems(xml.Element("systems"));
            var provider = (ADProvider)systems[export.Attribute("provider").Value].provider;
            var path = export.Attribute("path").Value;
            var excluded = new List<string>();
            excluded.AddRange(from p in export.Element("excludedPaths").Elements("path") select p.Value);

            var fileList = new List<string>();

            if (Directory.Exists(path))
            {
                var to = String.Format("{0}_old_{1}", path, DateTime.Now.ToString("MM-dd-yyyy_H-mm"));
                Directory.Move(path, to);
                Directory.CreateDirectory(path);
            }
            else
            {
                Directory.CreateDirectory(path);
            }

            foreach (var obj in export.Elements("object"))
            {
                var fields = new List<string>();
                var multifields = new List<string>();
                var acls = new List<string>();
                var kind = obj.Attribute("class").Value;
                int counter = 0;
                int filecounter = 0;

                Console.WriteLine("Exporting {0}s", kind);

                fields.AddRange(from a in obj.Elements("field") select a.Value);
                multifields.AddRange(from a in obj.Elements("multifield") select a.Value);
                acls.AddRange(from a in obj.Elements("acl") select a.Value);

                var file = File.CreateText(String.Format("{0}\\{1}_{2}.xml", path, kind, filecounter));
                file.WriteLine("<fixtures>");
                fileList.Add(String.Format("{0}_{1}.xml", kind, filecounter));

                foreach (var read in ReadAD(provider, kind, excluded, fields, multifields, acls))
                {
                    // write to file
                    file.WriteLine(read.ToString());

                    counter++;
                    if (counter % 100 == 0)
                    {
                        Console.Write(".");
                    }

                    if (counter % 300 == 0)
                    {
                        file.WriteLine("</fixtures>");
                        file.Flush();
                        file.Close();
                        // split file
                        filecounter++;
                        file = File.CreateText(String.Format("{0}\\{1}_{2}.xml", path, kind, filecounter));
                        file.WriteLine("<fixtures>");
                        fileList.Add(String.Format("{0}_{1}.xml", kind, filecounter));
                    }
                }

                file.WriteLine("</fixtures>");
                file.Flush();
                file.Close();
                Console.WriteLine("\r\nExported {0} objects", counter);
            }

            var final = new XElement("fimunit", new XElement("fixtures", new XAttribute("path", String.Join("; ", fileList))));
            final.Save(path + "\\fixtures.xml");
        }

        static void Import(XElement xml)
        {
            Console.WriteLine("Importing Active Directory");

            //const string userMB = "Enable-Mailbox -Identity '{0}' -Database '{1}' -Alias '{2}' -PrimarySmtpAddress '{3}'";
            //const string groupMB = "Enable-DistributionGroup -Identity '{0}' -Alias '{1}' -PrimarySmtpAddress '{2}'";

            var import = xml.Element("import");
            var defaultPwd = import.Element("defaultPassword").Value;
            var systems = new Systems(xml.Element("systems"));
            var provider = (ADProvider)systems[import.Attribute("provider").Value].provider;
            var path = import.Attribute("path").Value;
            var fixtures = XElement.Load(path + "\\fixtures.xml");
            StreamWriter usr = null;
            StreamWriter list = null;
            string mdb = "";
            if (import.Elements("exchange").Count() > 0)
            {
                usr = File.CreateText(path + "\\create_user_mail.txt");
                usr.WriteLine("{0}\t{1}\t{2}\t{3}", "dn", "database", "nickname", "mail");
                list = File.CreateText(path + "\\create_group_mail.txt");
                list.WriteLine("{0}\t{1}\t{2}", "dn", "nickname", "mail");
                mdb = import.Element("exchange").Value;
            }
            
            foreach (var file in from f in fixtures.Element("fixtures").Attribute("path").Value.Split(';')
                                 select f.Trim())
            {
                Console.WriteLine("\r\nLoading from {0}", file);
                var fixtureXml = XElement.Load(path + "\\" + file);
                var counter = 0;
                Console.WriteLine("Creating in Directory");
                foreach (var fix in fixtureXml.Elements("ADFixture"))
                {
                    var fixture = new ADFixture(fix, provider);
                    if (fixture.schemaClassName.ToLower() == "user")
                    {
                        fixture.unicodePwd = defaultPwd;
                    }

                    //fixture.PreCreate();
                    if (import.Elements("exchange").Count() > 0 && fixture.fields.ContainsKey("mailNickname") && fixture.fields.ContainsKey("mail"))
                    {
                        if (fixture.schemaClassName.ToLower() == "user")
                        {
                            usr.WriteLine("{0}\t{1}\t{2}\t{3}", fixture.DN, mdb, fixture.fields["mailNickname"], fixture.fields["mail"]);
                        }
                        else if (fixture.schemaClassName.ToLower() == "group")
                        {
                            list.WriteLine("{0}\t{1}\t{2}", fixture.DN, fixture.fields["mailNickname"], fixture.fields["mail"]);
                        }
                    }

                    counter++;
                    if (counter % 100 == 0)
                    {
                        Console.Write(".");
                    }
                }
            }

            usr.Flush();
            usr.Close();
            list.Flush();
            list.Close();
            return;
            
            foreach (var file in from f in fixtures.Element("fixtures").Attribute("path").Value.Split(';')
                                 select f.Trim())
            {
                Console.WriteLine("\r\nLoading from {0}", file);
                var fixtureXml = XElement.Load(path + "\\" + file);
                var counter = 0;
                Console.WriteLine("\r\nSetting Attributes");
                counter = 0;
                foreach (var fix in fixtureXml.Elements("ADFixture"))
                {
                    var fixture = new ADFixture(fix, provider);
                    fixture.type = FixtureType.Update;
                    fixture.Load();

                    counter++;
                    if (counter % 100 == 0)
                    {
                        Console.Write(".");
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            var argList = new List<string>();
            argList.AddRange(args);
            Console.WriteLine("FIMLabPopulator: tool to export AD settings to xml fixture files");
            Console.WriteLine();
            if (argList.Count == 0 || argList.Contains("-h") || argList.Contains("--help"))
            {
                Usage();
                return;
            }

            Console.WriteLine("Reading XML from {0}\r\n", argList[0]);

            var xml = XElement.Load(argList[0]);
            var direction = argList[1];

            if (direction == "export")
            {
                Export(xml);
            }
            else if (direction == "import")
            {
                Import(xml);
            }
            else
            {
                Usage();
            }
        }
    }
}
