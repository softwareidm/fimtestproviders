﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.SyncLogProvider
{
    public class SyncLogProvider : IProvider
    {
        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "provider",
                new XAttribute(ModelHelper.XSI + "type", "SyncLogProvider"),
                new XElement(ModelHelper.SI + "maDataLocation", MADataLocation),
                new XElement(ModelHelper.SI + "ma", MA),
                new XElement(ModelHelper.SI + "file", File));
        }

        public string MADataLocation { get; set; }

        public string MA { get; set; }

        public string File { get; set; }

        public void Initialize(XElement xml)
        {
            foreach (var el in new string[] { "ma", "file" })
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + el, typeof(SyncLogProvider));
            }

            MA = xml.Element(ModelHelper.SI + "ma").Value;
            File = xml.Element(ModelHelper.SI + "file").Value;
            MADataLocation = @"C:\Program Files\Microsoft Forefront Identity Manager\2010\Synchronization Service\MaData";
            if (xml.Elements(ModelHelper.SI + "maDataLocation").Count() > 0)
            {
                MADataLocation = xml.Element(ModelHelper.SI + "maDataLocation").Value;
            }
        }

        public bool TestAssert(IAssertion assertion)
        {
            throw new NotImplementedException();
        }

        public static XNamespace xn = XNamespace.Get("http://www.microsoft.com/mms/mmsml/v2");

        /// <summary>
        /// Edits to the output MMSML should be made from this method and it's children
        /// </summary>
        public void WriteFixture(IFixture write)
        {
            var fixture = (SyncLogFixture)write;
            string file = Path.Combine(MADataLocation, MA, File);
            XElement xml;
            if (System.IO.File.Exists(file))
            {
                xml = XElement.Load(file);
            }
            else
            {
                xml = new XElement(xn + "mmsml", new XElement(xn + "directory-entries"), new XAttribute("step-type", "delta-import"));
            }

            // search if DN exists
            var search = Fixture.HandleVal(fixture.DN).ToString();
            XElement found = null;
            foreach (var entry in xml.Element(xn + "directory-entries").Elements("delta"))
            {
                if (entry.Attribute("dn").Value == search)
                {
                    found = entry;
                    break;
                }
            }

            var writeEl = fixture.GetMml(found, xml.Attribute("step-type").Value == "delta-import" ? FileModType.DeltaImport : FileModType.FullImport);
            if (found != null)
            {
                found.ReplaceAll(writeEl.Nodes());
                writeEl = found;
            }
            else
            {
                xml.Add(writeEl);
            }

            xml.Save(file);
        }
    }

    public enum AttrType
    {
        String,
        Integer,
        Binary,
        Boolean,
        Reference
    }
    
    public enum AMod
    {
        Unconfigured,
        Add,
        Update,
        Delete,
        Replace
    }
    
    public enum FileModType
    {
        FullImport,
        DeltaImport,
        Export
    }

    public class MultiField
    {
        public AttrType AType { get; set; } = AttrType.String;

        public AMod Operation { get; set; } = AMod.Unconfigured;

        public string Name { get; set; }

        public List<string> Values { get; set; } = new List<string>();

        public MultiField(XElement xml)
        {
            ModelHelper.RequiredAttribute(xml, "name", typeof(SyncLogFixture));
            Name = xml.Attribute("name").Value;
            Values.AddRange(from v in xml.Elements(ModelHelper.SI + "value")
                           select v.Value);

            if (xml.Attributes("type").Count() > 0)
            {
                AType = (AttrType)Enum.Parse(typeof(AttrType), xml.Attribute("type").Value, true);
            }
            if (xml.Attributes("operation").Count() > 0)
            {
                Operation = (AMod)Enum.Parse(typeof(AMod), xml.Attribute("type").Value, true);
            }
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "attribute",
                new XAttribute("name", Name),
                //new XAttribute("operation", Operation.ToString()),
                new XAttribute("type", AType.ToString()),
                from v in Values
                select new XElement(ModelHelper.SI + "value", v));
        }

        public XElement GetMml(string fixtureOperation, FileModType fixtureFileMod)
        {
            XElement ret = null;
            if (AType == AttrType.Reference)
            {
                ret = new XElement(SyncLogProvider.xn + "dn-attr",
                new XAttribute("name", Name),
                new XAttribute("multivalued", "true"),
                from v in Values
                select new XElement(SyncLogProvider.xn + "dn-value", Fixture.HandleVal(v)));
            }
            else
            {
                ret = new XElement(SyncLogProvider.xn + "attr",
                new XAttribute("name", Name),
                new XAttribute("type", AType.ToString().ToLower()),
                new XAttribute("multivalued", "true"),
                from v in Values
                select new XElement(SyncLogProvider.xn + "value", Fixture.HandleVal(v)));
            }

            if (fixtureOperation != "Add" && fixtureFileMod == FileModType.DeltaImport)
            {
                ret.Add(new XAttribute("operation", Operation.ToString().ToLower()));
            }

            return ret;
        }
    }

    public class Field
    {
        public AttrType AType { get; set; } = AttrType.String;

        /// <summary>
        /// Should only be provided on a delta-import operation for a object mod type other than Add
        /// </summary>
        public AMod Operation { get; set; } = AMod.Unconfigured;

        public string Name { get; set; }

        public string Value { get; set; }

        public Field(XElement xml)
        {
            ModelHelper.RequiredAttribute(xml, "name", typeof(SyncLogFixture));
            Name = xml.Attribute("name").Value;
            Value = xml.Value;
            if (xml.Attributes("type").Count() > 0)
            {
                AType = (AttrType)Enum.Parse(typeof(AttrType), xml.Attribute("type").Value, true);
            }
            if (xml.Attributes("operation").Count() > 0)
            {
                Operation = (AMod)Enum.Parse(typeof(AMod), xml.Attribute("type").Value, true);
            }
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "attribute",
                new XAttribute("name", Name),
                new XAttribute("operation", Operation.ToString()),
                new XAttribute("type", AType.ToString()),
                Value);
        }

        public XElement GetMml(string fixtureOperation, FileModType fixtureFileMod)
        {
            XElement ret = null;
            if (AType == AttrType.Reference)
            {
                ret = new XElement(SyncLogProvider.xn + "dn-attr",
                new XAttribute("name", Name),
                new XAttribute("multivalued", "false"),
                new XElement(SyncLogProvider.xn + "dn-value", Fixture.HandleVal(Value)));
            }
            else
            {
                ret = new XElement(SyncLogProvider.xn + "attr",
                new XAttribute("name", Name),
                new XAttribute("type", AType.ToString().ToLower()),
                new XAttribute("multivalued", "false"),
                new XElement(SyncLogProvider.xn + "value", Fixture.HandleVal(Value)));
            }

            if (fixtureOperation != "Add" && fixtureFileMod == FileModType.DeltaImport)
            {
                ret.Add(new XAttribute("operation", Operation.ToString().ToLower()));
            }

            return ret;
        }
    }

    public class SyncLogFixture : Fixture
    {
        public string DN { get; set; }

        public string Anchor { get; set; }

        public string PrimaryObjectClass { get; set; }

        public string AttributeMode { get; set; } = "Merge";

        public List<String> ObjectClass { get; set; } = new List<string>();

        public List<Field> Attributes { get; set; } = new List<Field>();

        public List<MultiField> MultiAttributes { get; set; } = new List<MultiField>();

        public SyncLogFixture() : base() { }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "dn", typeof(SyncLogFixture));
            DN = xml.Element(ModelHelper.SI + "dn").Value;

            if (this.Operation == "Add" || this.Operation == "Replace")
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "anchor", typeof(SyncLogFixture));
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "primaryObjectClass", typeof(SyncLogFixture));
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "objectClass", typeof(SyncLogFixture));
            }

            if (xml.Elements(ModelHelper.SI + "anchor").Count() > 0)
            {
                Anchor = xml.Element(ModelHelper.SI + "anchor").Value;
            }

            if (xml.Elements(ModelHelper.SI + "primaryObjectClass").Count() > 0)
            {
                PrimaryObjectClass = xml.Element(ModelHelper.SI + "primaryObjectClass").Value;
            }

            foreach (var oc in xml.Elements(ModelHelper.SI + "objectClass"))
            {
                ObjectClass.Add(oc.Value);
            }

            ModelHelper.RequiredElement(xml, ModelHelper.SI + "attributeMode", typeof(SyncLogFixture));
            AttributeMode = xml.Element(ModelHelper.SI + "attributeMode").Value;

            Attributes.AddRange(from a in xml.Elements(ModelHelper.SI + "attribute")
                                select new Field(a));

            MultiAttributes.AddRange(from a in xml.Elements(ModelHelper.SI + "multiattribute")
                                     select new MultiField(a));

        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            ret.Add(new XElement(ModelHelper.SI + "dn", DN));

            if (!String.IsNullOrEmpty(Anchor))
            {
                ret.Add(new XElement(ModelHelper.SI + "anchor", Anchor));
            }

            if (!String.IsNullOrEmpty(PrimaryObjectClass))
            {
                ret.Add(new XElement(ModelHelper.SI + "primaryObjectClass", PrimaryObjectClass));
                ret.Add(from o in ObjectClass
                        select new XElement(ModelHelper.SI + "objectClass", o));
            }

            ret.Add(from a in Attributes
                    select a.GetXml());

            ret.Add(from a in MultiAttributes
                    select a.GetXml());

            return ret;
        }

        public XElement GetMml(XElement existing, FileModType fileStepType)
        {
            var ret = new XElement(SyncLogProvider.xn + "delta",
                new XAttribute("dn", HandleVal(DN)),
                new XAttribute("operation", Operation.ToLower()));

            // set anchor
            if (Anchor != null)
            {
                ret.Add(new XElement(SyncLogProvider.xn + "anchor",
                    new XAttribute("encoding", "base64"),
                    HandleVal(Anchor)));
            }
            else if (existing != null && existing.Elements(SyncLogProvider.xn + "anchor").Count() > 0)
            {
                ret.Add(new XElement(SyncLogProvider.xn + "anchor",
                    new XAttribute("encoding", "base64"),
                    existing.Element(SyncLogProvider.xn + "anchor").Value));
            }

            // object class
            if (PrimaryObjectClass != null)
            {
                ret.Add(new XElement(SyncLogProvider.xn + "primary-objectclass", HandleVal(PrimaryObjectClass)));
            }
            else if (existing != null && existing.Elements(SyncLogProvider.xn + "primary-objectclass").Count() > 0)
            {
                ret.Add(new XElement(SyncLogProvider.xn + "primary-objectclass",
                    existing.Element(SyncLogProvider.xn + "primary-objectclass").Value));
            }

            if (ObjectClass.Count > 0)
            {
                ret.Add(new XElement(SyncLogProvider.xn + "objectclass",
                    from o in ObjectClass
                    select new XElement(SyncLogProvider.xn + "oc-value", HandleVal(o))));
            }
            else if (existing != null && existing.Elements(SyncLogProvider.xn + "objectclass").Count() > 0)
            {
                ret.Add(new XElement(SyncLogProvider.xn + "objectclass",
                    from o in existing.Element(SyncLogProvider.xn + "objectclass").Elements(SyncLogProvider.xn + "oc-value")
                    select new XElement(SyncLogProvider.xn + "oc-value", o.Value)));
            }
            else if (PrimaryObjectClass != null)
            {
                ret.Add(new XElement(SyncLogProvider.xn + "objectclass",
                    new XElement(SyncLogProvider.xn + "oc-value", HandleVal(PrimaryObjectClass))));
            }

            var attrs = new Dictionary<string, XElement>();

            // set attributes
            if (AttributeMode == "Merge" && existing != null)
            {
                // attribute mode is merge, cache the existing attributes
                foreach (var attr in existing.Elements(SyncLogProvider.xn + "dn-attr"))
                {
                    attrs[attr.Attribute("name").Value] = attr;
                }

                foreach (var attr in existing.Elements(SyncLogProvider.xn + "attr"))
                {
                    attrs[attr.Attribute("name").Value] = attr;
                }
            }

            // copy in all the new attributes
            foreach (var attr in Attributes)
            {
                attrs[attr.Name] = attr.GetMml(Operation, fileStepType);
            }

            foreach (var attr in MultiAttributes)
            {
                attrs[attr.Name] = attr.GetMml(Operation, fileStepType);
            }

            // add them to the return XML
            ret.Add(attrs.Values.ToArray());

            return ret;
        }
    }
}
