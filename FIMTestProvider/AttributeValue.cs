﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftwareIDM.FIMTest.ProviderModel
{
    public enum AttributeType
    {
        String,
        Boolean,
        Binary,
        Integer,
        Decimal,
        Date,
        Guid,
        MultiValue,
        Provider
    }

    /// <summary>
    /// This class provides a lazy binding representation of a value for use with the rule parser.
    /// An AttributeValue can be constructed either by giving it a literal value such as a string or an int,
    /// or by giving it a Rule, which will be evaluated when one of the typed Value accessors is invoked.
    /// </summary>
    public class AttributeValue : IEquatable<AttributeValue>, IComparable<AttributeValue>
    {
        public AttributeValue() {
            this._DataType = AttributeType.String;
        }

        /// <summary>
        /// Construct with a value generating Rule
        /// </summary>
        /// <param name="rule">Rule to be evaluated</param>
        public AttributeValue(Rule rule)
            : base()
        {
            this.rule = rule;
        }

        object value;
        /// <summary>
        /// Construct with a literal value, which is type converted as appropriate to one of the
        /// FIM supported primitive values. This constructor does not support multi-valued objects.
        /// </summary>
        /// <param name="val">Value to represent</param>
        public AttributeValue(object val)
        {
            value = val;
            if (val == null)
            {
                this._DataType = AttributeType.String;
            }
            else if (val is bool)
            {
                this._DataType = AttributeType.Boolean;
            }
            else if (val is long)
            {
                this._DataType = AttributeType.Integer;
            }
            else if (val is int)
            {
                this._DataType = AttributeType.Integer;
                value = (long)(int)val;
            }
            else if (val is double)
            {
                this._DataType = AttributeType.Decimal;
            }
            else if (val is float)
            {
                this._DataType = AttributeType.Decimal;
                value = (double)(float)val;
            }
            else if (val is DateTime)
            {
                this._DataType = AttributeType.Date;
            }
            else if (val is byte[])
            {
                this._DataType = AttributeType.Binary;
            }
            else if (val is List<AttributeValue>)
            {
                this.Values = (List<AttributeValue>)val;
                this._DataType = AttributeType.MultiValue;
            }
            else if (val is Guid)
            {
                this._DataType = AttributeType.Guid;
            }
            else
            {
                this._DataType = AttributeType.String;
            }
        }

        #region build multivalue

        public static AttributeValue MultiValue(IEnumerable<IComparable> values, MultiBehavior behavior)
        {
            var ret = new AttributeValue();
            ret._DataType = AttributeType.MultiValue;
            ret.MultivalueBehavior = behavior;
            ret.Values = new List<AttributeValue>();
            ret.Values.AddRange(from v in values
                                select new AttributeValue(v));
            return ret;
        }

        #endregion

        /// <summary>
        /// Type coercing equals comparison e.g. AttributeValue("1.0").Equals(AttributeValue(1.0) returns true.
        /// </summary>
        /// <param name="compare">AttributeValue to compare to</param>
        /// <returns>Equality result</returns>
        public bool Equals(AttributeValue compare)
        {
            if (Value == null)
                return compare.Value == null;
            else if (compare.Value == null)
                return false;

            if (DataType == AttributeType.MultiValue)
            {
                if (compare.DataType != AttributeType.MultiValue)
                {
                    if (Values.Count == 1)
                    {
                        return Values[0].Equals(compare);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (compare.Values.Count != Values.Count)
                    {
                        return false;
                    }

                    for (int i = 0; i < Values.Count; i++)
                    {
                        if (!Values[i].Equals(compare.Values[i]))
                        {
                            return false;
                        }
                    }

                    return true;
                }
            }

            switch (DataType)
            {
                case AttributeType.Binary:
                    return Convert.ToBase64String(BinaryValue) == Convert.ToBase64String(compare.BinaryValue);
                case AttributeType.Boolean:
                    return BooleanValue == compare.BooleanValue;
                case AttributeType.Integer:
                    return IntegerValue == compare.IntegerValue;
                case AttributeType.Decimal:
                    return DoubleValue == compare.DoubleValue;
                case AttributeType.Date:
                    return DateValue == compare.DateValue;
                case AttributeType.Guid:
                    return GuidValue == compare.GuidValue;
                default:
                    return StringValue == compare.StringValue;
            }
        }

        private int CompareBin(byte[] me, byte[] other)
        {
            if (me.Length > other.Length)
            {
                var hold = new byte[me.Length];
                other.CopyTo(hold, me.Length - other.Length);
                other = hold;
            }
            else if (other.Length > me.Length)
            {
                var hold = new byte[other.Length];
                me.CopyTo(hold, other.Length - me.Length);
                me = hold;
            }

            var ret = 0;
            for (int i = 0; i < me.Length; i++)
            {
                if (me[i] != other[i])
                {
                    ret = me[i].CompareTo(other[i]);
                    break;
                }
            }

            return ret;
        }

        public int CompareTo(AttributeValue compare)
        {
            if (Value == null)
            {
                if (compare.Value == null)
                    return 0;
                else
                    return int.MinValue;
            }

            if (compare.Value == null)
            {
                return int.MaxValue;
            }

            switch (DataType)
            {
                case AttributeType.Binary:
                    switch (compare.DataType)
                    {
                        case AttributeType.Binary:
                        case AttributeType.Integer:
                        case AttributeType.String:
                        case AttributeType.MultiValue:
                            return CompareBin(BinaryValue, compare.BinaryValue);
                        default:
                            throw new FormatException(String.Format("Cannot perform comparison between Binary and {0} AttributeValues", compare.DataType));
                    }
                    
                case AttributeType.Boolean:
                    switch (compare.DataType)
                    {
                        case AttributeType.Boolean:
                        case AttributeType.Integer:
                        case AttributeType.String:
                        case AttributeType.Decimal:
                        case AttributeType.MultiValue:
                            return BooleanValue.CompareTo(compare.BooleanValue);
                        default:
                            throw new FormatException(String.Format("Cannot perform comparison between Boolean and {0} AttributeValues", compare.DataType));
                    }
                case AttributeType.Integer:
                    switch (compare.DataType)
                    {
                        case AttributeType.Boolean:
                        case AttributeType.Integer:
                        case AttributeType.Decimal:
                        case AttributeType.String:
                        case AttributeType.Date:
                        case AttributeType.MultiValue:
                            return IntegerValue.CompareTo(compare.IntegerValue);
                        default:
                            throw new FormatException(String.Format("Cannot perform comparison between Integer and {0} AttributeValues", compare.DataType));
                    }
                case AttributeType.Decimal:
                    switch (compare.DataType)
                    {
                        case AttributeType.Integer:
                        case AttributeType.Decimal:
                        case AttributeType.String:
                        case AttributeType.MultiValue:
                            return DoubleValue.CompareTo(compare.DoubleValue);
                        default:
                            throw new FormatException(String.Format("Cannot perform comparison between Decimal and {0} AttributeValues", compare.DataType));
                    }
                    
                case AttributeType.Date:
                    switch (compare.DataType)
                    {
                        case AttributeType.Integer:
                        case AttributeType.Date:
                        case AttributeType.String:
                        case AttributeType.MultiValue:
                            return DateValue.Value.CompareTo(compare.DateValue.Value);
                        default:
                            throw new FormatException(String.Format("Cannot perform comparison between Date and {0} AttributeValues", compare.DataType));
                    }
                case AttributeType.Guid:
                    switch (compare.DataType)
                    {
                        case AttributeType.Guid:
                        case AttributeType.Binary:
                        case AttributeType.String:
                        case AttributeType.MultiValue:
                            return GuidValue.CompareTo(compare.GuidValue);
                        default:
                            throw new FormatException(String.Format("Cannot perform comparison between Guid and {0} AttributeValues", compare.DataType));
                    }
                case AttributeType.String:
                    switch (compare.DataType)
                    {
                        case AttributeType.Binary:
                            return CompareBin(BinaryValue, compare.BinaryValue);
                        case AttributeType.Boolean:
                            return BooleanValue.CompareTo(compare.BooleanValue);
                        case AttributeType.Date:
                            return DateValue.Value.CompareTo(compare.DateValue);
                        case AttributeType.Decimal:
                            return DoubleValue.CompareTo(compare.DoubleValue);
                        case AttributeType.Integer:
                            return IntegerValue.CompareTo(compare.IntegerValue);
                        default:
                            return StringValue.CompareTo(compare.StringValue);
                    }
                case AttributeType.MultiValue:
                    if (Values.Count > 0)
                    {
                        return Values[0].CompareTo(compare);
                    }
                    else
                    {
                        if (compare.Value == null || compare.DataType == AttributeType.MultiValue && compare.Values != null && compare.Values.Count == 0)
                            return 0;
                        else
                            return int.MinValue;
                    }

                default:
                    throw new FormatException(String.Format("Cannot perform comparison of {0} AttributeValue", compare.DataType)); 
            }
        }

        private void LateBind()
        {
            if (!_DataType.HasValue)
            {
                if (rule != null)
                {
                    var val = rule.LateBind();
                    if (val.DataType == AttributeType.MultiValue)
                    {
                        Values = val.Values;
                        _DataType = AttributeType.MultiValue;
                    }
                    else
                    {
                        switch (val.DataType)
                        {
                            case AttributeType.Binary:
                                BinaryValue = val.BinaryValue;
                                break;
                            case AttributeType.Boolean:
                                BooleanValue = val.BooleanValue;
                                break;
                            case AttributeType.Integer:
                                IntegerValue = val.IntegerValue;
                                break;
                            case AttributeType.Decimal:
                                DoubleValue = val.DoubleValue;
                                break;
                            case AttributeType.Date:
                                DateValue = val.DateValue;
                                break;
                            case AttributeType.Guid:
                                GuidValue = val.GuidValue;
                                break;
                            default:
                                StringValue = val.StringValue;
                                break;
                        }
                    }
                }
                else
                {
                    _DataType = AttributeType.String;
                }
            }
        }

        AttributeType? _DataType = null;
        /// <summary>
        /// Enumeration representing the current Type that's being stored in the attribute value.
        /// </summary>
        public AttributeType DataType
        {
            get
            {
                LateBind();
                return _DataType.Value;
            }
        }

        /// <summary>
        /// Return the underlying value as an object with type coercion.
        /// </summary>
        public object Value
        {
            get
            {
                switch (DataType)
                {
                    case AttributeType.Date:
                        return this.DateValue;
                    case AttributeType.Integer:
                        return this.IntegerValue;
                    case AttributeType.Decimal:
                        return this.DoubleValue;
                    case AttributeType.Boolean:
                        return this.BooleanValue;
                    case AttributeType.Binary:
                        return this.BinaryValue;
                    case AttributeType.Guid:
                        return this.GuidValue;
                    case AttributeType.MultiValue:
                        return this.Values;
                    default:
                        return this.StringValue;
                }
            }
        }

        public object Coerce(AttributeType target)
        {
            if (target == AttributeType.Provider || DataType == AttributeType.Provider)
            {
                throw new ArgumentException("Conversion to from IProvider not supported");
            }

            switch (target)
            {
                case AttributeType.Binary:
                    if (value == null)
                        return null;

                    switch (DataType)
                    {
                        case AttributeType.Binary:
                            return (byte[])value;
                        case AttributeType.Boolean:
                            return (bool)value ? new byte[] { 1 } : new byte[] { 0 };
                        case AttributeType.Date:
                            return BitConverter.GetBytes(((DateTime)value).ToFileTime());
                        case AttributeType.Decimal:
                            return BitConverter.GetBytes((double)value);
                        case AttributeType.Integer:
                            return BitConverter.GetBytes((long)value);
                        case AttributeType.Guid:
                            return ((Guid)value).ToByteArray();
                        case AttributeType.MultiValue:
                            if (_Values.Count > 0)
                            {
                                return _Values[0].BinaryValue;
                            }
                            else
                            {
                                return null;
                            }
                        default:
                            try
                            {
                                return Convert.FromBase64String((string)value);
                            }
                            catch { }
                            return UTF8Encoding.GetEncoding(0).GetBytes((string)value);
                    }
                case AttributeType.Boolean:
                    if (value == null)
                        return false;
                    switch (DataType)
                    {
                        case AttributeType.Binary:
                            return ((byte[])value)[0] > 0;
                        case AttributeType.Boolean:
                            return (bool)value;
                        case AttributeType.Date:
                            return ((DateTime)value) != DateTime.MinValue;
                        case AttributeType.Decimal:
                            return (double)value > 0;
                        case AttributeType.Integer:
                            return (long)value > 0;
                        case AttributeType.Guid:
                            return !((Guid)value).Equals(Guid.Empty);
                        case AttributeType.MultiValue:
                            if (_Values.Count > 0)
                            {
                                return _Values[0].BooleanValue;
                            }
                            else
                            {
                                return false;
                            }
                        default:
                            return !String.IsNullOrEmpty((string)value) && ((string)value).ToLower() != "false";
                    }
                case AttributeType.Date:
                    if (value == null)
                        return null;
                    switch (DataType)
                    {
                        case AttributeType.Binary:
                            throw new ArgumentException("Conversion from Binary Array to Date not supported");
                        case AttributeType.Boolean:
                            throw new ArgumentException("Conversion from Boolean to Date not supported");
                        case AttributeType.Date:
                            return (DateTime)value;
                        case AttributeType.Decimal:
                            throw new ArgumentException("Conversion from Decimal to Date not supported");
                        case AttributeType.Integer:
                            return DateTime.FromFileTime((long)value);
                        case AttributeType.Guid:
                            throw new ArgumentException("Conversion from Guid to Date is not supported");
                        case AttributeType.MultiValue:
                            if (_Values.Count > 0)
                            {
                                return _Values[0].DateValue;
                            }
                            else
                            {
                                return null;
                            }
                        default:
                            return DateTime.Parse((string)value);
                    }
                case AttributeType.Decimal:
                    if (value == null)
                        return null;
                    switch (DataType)
                    {
                        case AttributeType.Binary:
                            return BitConverter.ToDouble((byte[])value, 0);
                        case AttributeType.Boolean:
                            return (bool)value ? 1.0 : 0.0;
                        case AttributeType.Date:
                            throw new ArgumentException("Conversion from Date to Decimal not supported");
                        case AttributeType.Decimal:
                            return (double)value;
                        case AttributeType.Integer:
                            return (double)(long)value;
                        case AttributeType.Guid:
                            throw new ArgumentException("Conversion from Guid to Decimal is not supported");
                        case AttributeType.MultiValue:
                            if (_Values.Count > 0)
                            {
                                return _Values[0].DoubleValue;
                            }
                            else
                            {
                                return null;
                            }
                        default:
                            return double.Parse((string)value);
                    }
                case AttributeType.Integer:
                    if (value == null)
                        return null;
                    switch (DataType)
                    {
                        case AttributeType.Binary:
                            return BitConverter.ToInt64((byte[])value, 0);
                        case AttributeType.Boolean:
                            return (long)((bool)value ? 1 : 0);
                        case AttributeType.Date:
                            return ((DateTime)value).ToFileTime();
                        case AttributeType.Decimal:
                            return (long)(double)value;
                        case AttributeType.Integer:
                            return (long)value;
                        case AttributeType.Guid:
                            throw new ArgumentException("Conversion from Guid to Integer is not supported");
                        case AttributeType.MultiValue:
                            if (_Values.Count > 0)
                            {
                                return _Values[0].IntegerValue;
                            }
                            else
                            {
                                return null;
                            }
                        default:
                            return long.Parse((string)value);
                    }
                case AttributeType.Guid:
                    if (value == null)
                        return null;
                    switch (DataType)
                    {
                        case AttributeType.Binary:
                            return new Guid((byte[])value);
                        case AttributeType.Boolean:
                            throw new ArgumentException("Conversion from Boolean to Guid is not supported");
                        case AttributeType.Date:
                            throw new ArgumentException("Conversion from Date to Guid is not supported");
                        case AttributeType.Decimal:
                            throw new ArgumentException("Conversion from Decimal to Guid is not supported");
                        case AttributeType.Guid:
                            return (Guid)value;
                        case AttributeType.Integer:
                            throw new ArgumentException("Conversion from Integer to Guid is not supported");
                        case AttributeType.MultiValue:
                            if (_Values.Count > 0)
                            {
                                return _Values[0].GuidValue;
                            }
                            else
                            {
                                return null;
                            }
                        default: // String
                            return Guid.Parse((string)value);
                    }
                    
                case AttributeType.MultiValue:
                    if (value == null)
                        return null;
                    switch (DataType)
                    {
                        case AttributeType.Binary:
                            return new List<AttributeValue> { new AttributeValue(this.BinaryValue) };
                        case AttributeType.Boolean:
                            return new List<AttributeValue> { new AttributeValue(this.BooleanValue) };
                        case AttributeType.Date:
                            return new List<AttributeValue> { new AttributeValue(this.DateValue) };
                        case AttributeType.Decimal:
                            return new List<AttributeValue> { new AttributeValue(this.DoubleValue) };
                        case AttributeType.Guid:
                            return new List<AttributeValue> { new AttributeValue(this.GuidValue) };
                        case AttributeType.Integer:
                            return new List<AttributeValue> { new AttributeValue(this.IntegerValue) };
                        case AttributeType.MultiValue:
                            return this._Values;
                        default:
                            return new List<AttributeValue> { new AttributeValue(this.StringValue) };
                    }
                default: // String
                    if (value == null)
                        return null;
                    switch (DataType)
                    {
                        case AttributeType.Binary:
                            return Convert.ToBase64String((byte[])value);
                        case AttributeType.Boolean:
                            return ((bool)value).ToString().ToLower();
                        case AttributeType.Date:
                            return ((DateTime)value).ToString();
                        case AttributeType.Decimal:
                            return ((double)value).ToString();
                        case AttributeType.Guid:
                            return ((Guid)value).ToString();
                        case AttributeType.Integer:
                            return ((long)value).ToString();
                        case AttributeType.MultiValue:
                            if (_Values.Count > 0)
                            {
                                return _Values[0].StringValue;
                            }
                            else
                            {
                                return null;
                            }
                        default:
                            return (string)value;
                    }
            }
        }

        Rule rule = null;
        public Rule Rule { get { return rule; } }
        /*
        IProvider provider = null;
        public IProvider Provider
        {
            get
            {
                LateBind();
                return provider;
            }
            set
            {
                _DataType = AttributeType.Provider;
                provider = value;
                this.value = provider;
            }
        }
        */
        List<AttributeValue> _Values = null;
        /// <summary>
        /// Multi-valued collection, only supports collections of strings.
        /// </summary>
        public List<AttributeValue> Values
        {
            get
            {
                LateBind();
                return (List<AttributeValue>)Coerce(AttributeType.MultiValue);
            }
            set
            {
                _DataType = AttributeType.MultiValue;
                _Values = value;
                this.value = _Values;
            }
        }

        public string StringValue
        {
            get
            {
                LateBind();
                return (string)Coerce(AttributeType.String);
            }
            set
            {
                _DataType = AttributeType.String;
                this.value = value;
            }
        }

        public Guid GuidValue
        {
            get
            {
                LateBind();
                return (Guid)Coerce(AttributeType.Guid);
            }
            set
            {
                _DataType = AttributeType.Guid;
                this.value = value;
            }
        }

        public DateTime? DateValue
        {
            get
            {
                LateBind();
                var ret = Coerce(AttributeType.Date);
                if (ret == null)
                    return null;
                return (DateTime)ret;
            }
            set
            {
                _DataType = AttributeType.Date;
                this.value = value;
            }
        }

        public bool BooleanValue
        {
            get
            {
                LateBind();
                return (bool)Coerce(AttributeType.Boolean);
            }
            set
            {
                _DataType = AttributeType.Boolean;
                this.value = value;
            }
        }

        public byte[] BinaryValue
        {
            get
            {
                LateBind();
                return (byte[])Coerce(AttributeType.Binary);
            }
            set
            {
                _DataType = AttributeType.Binary;
                this.value = value;
            }
        }

        public long IntegerValue
        {
            get
            {
                LateBind();
                return (long)Coerce(AttributeType.Integer);
            }
            set
            {
                _DataType = AttributeType.Integer;
                this.value = value;
            }
        }

        public double DoubleValue
        {
            get
            {
                LateBind();
                return (double)Coerce(AttributeType.Integer);
            }
            set
            {
                _DataType = AttributeType.Decimal;
                this.value = value;
            }
        }

        public MultiBehavior MultivalueBehavior = MultiBehavior.Append;
        /// <summary>
        /// Shortcut to return the string value with the Trim method called on it.
        /// This is a useful shortcut, because it automatically returns "" for a Null string.
        /// </summary>
        /// <param name="trim">chars to trim, passed directly to String.Trim()</param>
        /// <returns>Trimmed String</returns>
        public string TrimmedString(params char[] trim)
        {
            if (StringValue == null || String.IsNullOrEmpty(StringValue.Trim(trim)))
            {
                return "";
            }

            return StringValue.Trim(trim);
        }

        public override string ToString()
        {
            switch (DataType)
            {
                case AttributeType.Date:
                    return DateValue.HasValue ? "" : DateValue.ToString();
                case AttributeType.Integer:
                    return IntegerValue.ToString();
                case AttributeType.Decimal:
                    return DoubleValue.ToString();
                case AttributeType.Boolean:
                    return BooleanValue.ToString();
                case AttributeType.Binary:
                    return BinaryValue == null ? "" : Convert.ToBase64String(BinaryValue);
                case AttributeType.Guid:
                    return GuidValue == null ? "" : GuidValue.ToString();
                case AttributeType.MultiValue:
                    var ret = new StringBuilder();
                foreach (var val in Values)
                {
                    ret.Append(val.ToString() + "\r\n");
                }

                return ret.ToString();
                default:
                    return StringValue == null ? "" : StringValue;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public enum MultiBehavior
    {
        Append,
        Replace,
        Remove
    }
}
