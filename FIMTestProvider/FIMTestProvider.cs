﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SoftwareIDM.FIMTest.ProviderModel
{
    public static class RelativePath
    {
        public static string Combine(string parent, string path)
        {
            if (Regex.IsMatch(path.Trim(), @"^[A-Z]:|^\\\\[^\\]+\\", RegexOptions.IgnoreCase))
            {
                return path;
            }
            else
            {
                if (parent.EndsWith(".xml"))
                {
                    if (!parent.Contains("\\"))
                    {
                        parent = ".\\" + parent;
                    }

                    parent = Regex.Replace(parent, @"\\[^\\]+$", "");
                }

                return Path.Combine(parent, path.Trim());
            }
        }
    }

    public class XmlParseException : Exception
    {
        public XElement xml { get; set; }
        public Type ParseType { get; set; }

        public XmlParseException(string message)
            : base(message) { }

        public XmlParseException(string message, XElement xml, Type ParseType)
            : base(message)
        {
            this.xml = xml;
            this.ParseType = ParseType;
        }
    }

    public static class ModelHelper
    {
        public static void RequiredAttribute(XElement xml, XName attribute, Type type)
        {
            if (xml.Attributes(attribute).Count() == 0)
            {
                if (attribute.Namespace.Equals(ModelHelper.SI))
                {
                    attribute = attribute.LocalName;
                }

                throw new XmlParseException(String.Format("Error parsing {0}. Missing attribute {1}", type.Name, attribute), xml, type);
            }
        }
        public static void RequiredElement(XElement xml, XName element, Type type)
        {
            if (xml.Elements(element).Count() == 0)
            {
                throw new XmlParseException(String.Format("Error parsing {0}. Missing element {1}", type.Name, element.LocalName), xml, type);
            }
        }

        public static XNamespace XSI { get { return XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance"); } }
        public static XNamespace SI { get { return XNamespace.Get("http://softwareidm.com/fimtest"); } }
    }

    public static class ProviderTypes
    {
        static Dictionary<string, Type> classes;
        static Dictionary<string, MethodInfo> ruleFunctions;

        public static IDictionary<string, SyncSystem> Systems { get; set; }

        static void AddDll(string file)
        {
            if (classes == null)
            {
                classes = new Dictionary<string, Type>();
            }

            if (ruleFunctions == null)
            {
                ruleFunctions = new Dictionary<string, MethodInfo>();
            }

            var interfaces = new string[] {
                "SoftwareIDM.FIMTest.ProviderModel.IProvider",
                "SoftwareIDM.FIMTest.ProviderModel.IFixture",
                "SoftwareIDM.FIMTest.ProviderModel.IAssertion"
            };

            var assembly = Assembly.LoadFrom(file);
            foreach (var t in assembly.GetTypes())
            {
                foreach (string face in interfaces)
                {
                    if (t.GetInterface(face, true) != null)
                    {
                        if (classes.ContainsKey(t.Name.ToString().ToLower()))
                        {
                            classes.Remove(t.Name.ToString().ToLower());
                        }

                        classes.Add(t.Name.ToString().ToLower(), t);
                    }
                }

                if (t.GetInterface("SoftwareIDM.FIMTest.ProviderModel.IRuleFunctions") != null)
                {
                    var methods = t.GetMethods(BindingFlags.Static | BindingFlags.Public);
                    foreach (var m in methods)
                    {
                        if (ruleFunctions.ContainsKey(m.Name))
                        {
                            ruleFunctions.Remove(m.Name);
                        }

                        ruleFunctions.Add(m.Name, m);
                    }
                }
            }
        }
        
        static string filePath;
        public static string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;

                var a = Assembly.GetEntryAssembly();
                if (a != null)
                {
                    var l = a.Location;

                    var info = new FileInfo(a.Location);
                    if (Directory.Exists(Path.Combine(info.Directory.FullName, "Providers")))
                    {
                        foreach (var file in info.Directory.EnumerateDirectories("Providers").First().EnumerateFiles("*.dll", SearchOption.AllDirectories))
                        {
                            AddDll(file.FullName);
                        }
                    }
                }

                if (!String.IsNullOrEmpty(filePath))
                {
                    foreach (var file in Directory.EnumerateFiles(filePath, "*.dll", SearchOption.AllDirectories))
                    {
                        AddDll(file);
                    }
                }
            }
        }

        public static Dictionary<string, Type> Classes
        {
            get
            {
                return classes;
            }
        }

        public static Dictionary<string, MethodInfo> RuleFunctions
        {
            get
            {
                return ruleFunctions;
            }
        }
    }

    public class SyncSystem
    {
        public IProvider Provider { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public SyncSystem() { }

        public SyncSystem(XElement xml)
        {
            ModelHelper.RequiredAttribute(xml, "name", typeof(SyncSystem));
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "provider", typeof(SyncSystem));
            var provider = xml.Element(ModelHelper.SI + "provider");
            ModelHelper.RequiredAttribute(provider, ModelHelper.XSI + "type", typeof(SyncSystem));

            Name = xml.Attribute("name").Value;
            var kind = provider.Attribute(ModelHelper.XSI + "type").Value;

            if (!ProviderTypes.Classes.ContainsKey(kind.ToLower()))
            {
                throw new XmlParseException(String.Format("Error parsing system. Class {0} not loaded.", kind), provider, typeof(SyncSystem));
            }

            var con = ProviderTypes.Classes[kind.ToLower()].GetConstructor(new Type[] { });
            Provider = (IProvider)con.Invoke(new object[] { });
            Provider.Initialize(provider);
        }

        public SyncSystem(JObject json)
        {
            Name = json["Name"].ToString();
            var t = json["Provider"]["$type"].ToString();
            var type = ProviderTypes.Classes[t.ToLower()];
            Provider = (IProvider)JsonConvert.DeserializeObject(json["Provider"].ToString(), type);
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "system",
                new XAttribute("name", Name),
                Provider.GetXml());
        }

        public JObject GetJson()
        {
            var ret = new JObject(
                new JProperty("Name", Name),
                new JProperty("Provider", JObject.Parse(JsonConvert.SerializeObject(Provider))));
            if (((JObject)ret["Provider"]).Property("$type") == null)
            {
                ((JObject)ret["Provider"]).Add(new JProperty("$type", Provider.GetType().Name));
            }

            return ret;
        }
    }

    public interface IRuleFunctions { }

    public enum MessageLevel
    {
        Normal = 0,
        Verbose,
        Warning,
        Error
    }

    public class ProgressEventArgs : EventArgs
    {
        public string Message { get; set; }

        public MessageLevel Level { get; set; }

        public ProgressEventArgs(MessageLevel level, string message, params object[] args)
        {
            this.Level = level;
            this.Message = string.Format(message, args);
        }

        public ProgressEventArgs(string message, params object[] args)
        {
            this.Message = String.Format(message, args);
        }
    }

    public delegate void ProgressEvent(object Sender, ProgressEventArgs e);

    public interface IFixture
    {
        string Name { get; set; }
        SyncSystem System { get; set; }
        string Operation { get; set; }
        string Path { get; set; }
        event ProgressEvent ReportProgress;

        void Load();
        void Initialize(XElement xml, IDictionary<string, SyncSystem> systems);
        XElement GetXml();
    }

    public interface IProvider
    {
        void WriteFixture(IFixture write);
        bool TestAssert(IAssertion assertion);
        void Initialize(XElement xml);
        XElement GetXml();
    }

    public interface IAssertion
    {
        string Name { get; set; }
        string Operation { get; set; }
        SyncSystem System { get; set; }

        event ProgressEvent ReportProgress;

        bool Test();
        void Initialize(XElement xml, IDictionary<string, SyncSystem> systems);
        XElement GetXml();
    }

    public enum AssertFieldType
    {
        Value,
        Regex,
        Rule
    }

    public struct AssertField
    {
        public AssertFieldType Type { get; set; }
        public string Value { get; set; }
        public string Field { get; set; }

        public AssertField(AssertFieldType type, string value)
            : this()
        {
            Type = type;
            Value = value;
        }

        public AssertField(AssertFieldType type, string field, string value)
            : this()
        {
            Type = type;
            Field = field;
            Value = value;
        }
    }


    public abstract class Assertion : IAssertion
    {
        public string Name { get; set; }
        public string Operation { get; set; }
        public SyncSystem System { get; set; }

        public event ProgressEvent ReportProgress = delegate { };

        public List<KeyValuePair<string, string>> Memos { get; set; }

        public List<AssertField> Fields { get; set; }

        public Assertion()
        {
            Fields = new List<AssertField>();
            Memos = new List<KeyValuePair<string, string>>();
        }

        public virtual void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            foreach (var attr in new XName[] { "name", ModelHelper.XSI + "type", "operation", "system" })
            {
                ModelHelper.RequiredAttribute(xml, attr, typeof(Assertion));
            }

            Name = xml.Attribute("name").Value;
            Operation = xml.Attribute("operation").Value;
            if (!systems.ContainsKey(xml.Attribute("system").Value))
            {
                throw new XmlParseException("Assertion references undefined system", xml, typeof(IAssertion));
            }

            System = systems[xml.Attribute("system").Value];

            foreach (var m in xml.Elements(ModelHelper.SI + "memo"))
            {
                Memos.Add(new KeyValuePair<string, string>(m.Attribute("name").Value, m.Value));
            }

            foreach (var f in xml.Elements(ModelHelper.SI + "field"))
            {
                var t = AssertFieldType.Value;
                if (f.Attributes("type").Count() > 0)
                {
                    try
                    {
                        t = (AssertFieldType)Enum.Parse(typeof(AssertFieldType), f.Attribute("type").Value);
                    }
                    catch (ArgumentException)
                    {
                        throw new XmlParseException(String.Format("Error parsing Assertion {0}. Invalid type {1}.", Name, f.Attribute("type").Value), xml, typeof(Assertion));
                    }
                }

                Fields.Add(new AssertField { Type = t, Value = f.Value, Field = f.Attributes("name").Count() > 0 ? f.Attribute("name").Value : null });
            }
        }

        public virtual bool Test()
        {
            foreach (var memo in Memos)
            {
                Evaluate.MemoValues[memo.Key] = Fixture.HandleVal(memo.Value);
            }

            return System.Provider.TestAssert(this);
        }

        public virtual XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "assert",
                new XAttribute(ModelHelper.XSI + "type", this.GetType().Name),
                new XAttribute("name", Name),
                new XAttribute("system", System.Name),
                new XAttribute("operation", Operation),
                from m in Memos
                select new XElement(ModelHelper.SI + "memo", new XAttribute("name", m.Key), m.Value),
                from f in Fields
                select new XElement(ModelHelper.SI + "field",
                    new XAttribute("name", f.Field),
                    new XAttribute("type", Enum.GetName(typeof(AssertFieldType), f.Type)),
                    f.Value)
                );

            return ret;
        }
    }

    public abstract class Fixture : IFixture
    {
        public List<KeyValuePair<string, string>> Fields { get; set; }
        public string Name { get; set; }
        public SyncSystem System { get; set; }
        public string Operation { get; set; }
        public string Path { get; set; }

        public event ProgressEvent ReportProgress = delegate { };

        public List<KeyValuePair<string, string>> Memos { get; set; }

        public Fixture()
        {
            Fields = new List<KeyValuePair<string, string>>();
            Memos = new List<KeyValuePair<string, string>>();
        }

        public virtual string GetField(string field)
        {
            return Fields.Where(f => f.Key == field).First().Value;
        }

        public virtual void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            foreach (var attr in new XName[] { "name", ModelHelper.XSI + "type", "operation", "system" })
            {
                ModelHelper.RequiredAttribute(xml, attr, typeof(Fixture));
            }

            foreach (var m in xml.Elements(ModelHelper.SI + "memo"))
            {
                Memos.Add(new KeyValuePair<string, string>(m.Attribute("name").Value, m.Value));
            }

            Name = xml.Attribute("name").Value;
            if (!systems.ContainsKey(xml.Attribute("system").Value))
            {
                throw new XmlParseException("Fixture references undefined system", xml, typeof(IFixture));
            }

            System = systems[xml.Attribute("system").Value];
            Operation = xml.Attribute("operation").Value;

            foreach (var f in xml.Elements(ModelHelper.SI + "field"))
            {
                Fields.Add(new KeyValuePair<string, string>(f.Attribute("name").Value, f.Value));
            }
        }

        public virtual XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "fixture",
                new XAttribute(ModelHelper.XSI + "type", this.GetType().Name), // fix null
                new XAttribute("name", Name),
                new XAttribute("operation", Operation),
                new XAttribute("system", System.Name),
                from m in Memos
                select new XElement(ModelHelper.SI + "memo", new XAttribute("name", m.Key), m.Value),
                from f in Fields
                select new XElement(ModelHelper.SI + "field", new XAttribute("name", f.Key), f.Value));
        }

        public static object HandleVal(string val)
        {
            return Evaluate.FixtureVal(val,
                new NullEscape(),
                new BooleanEscape(),
                new IntEscape(),
                new DateEscape(),
                new RuleEscape(),
                new DecimalEscape());
        }

        public virtual void Load()
        {
            foreach (var memo in Memos)
            {
                Evaluate.MemoValues[memo.Key] = HandleVal(memo.Value);
            }

            System.Provider.WriteFixture((IFixture)this);
        }
    }
}
