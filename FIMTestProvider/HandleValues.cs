﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SoftwareIDM.FIMTest.ProviderModel
{
    public interface ICompareHandler
    {
        Regex Pattern { get; }
        bool Test(string compare, AttributeValue value);
    }

    public interface IValueHandler
    {
        Regex Pattern { get; }
        object Handle(string value);
    }

    public class NullEscape : IValueHandler
    {
        public Regex Pattern
        {
            get { return new Regex(@"^\[null]$"); }
        }

        public object Handle(string value)
        {
            return null;
        }
    }

    public class NullCompare : ICompareHandler
    {
        public Regex Pattern
        {
            get { return new Regex(@"^\[null|not null]$"); }
        }

        public bool Test(string compare, AttributeValue value)
        {
            if (compare == "[null]")
            {
                return value.Value == null;
            }
            else
            {
                return value.Value != null;
            }
        }
    }

    public class BooleanEscape : IValueHandler
    {
        public Regex Pattern
        {
            get { return new Regex(@"^\[(true|false|True|False)\]$"); }
        }

        public object Handle(string value)
        {
            value = value.Substring(1, value.Length - 2);
            return Boolean.Parse(value.ToLower());
        }
    }

    public class IntEscape : IValueHandler
    {

        public Regex Pattern
        {
            get { return new Regex(@"^\[(?:0x|-)?(?:\d+)\]$"); }
        }

        public object Handle(string value)
        {
            value = value.Substring(1, value.Length - 2);
            if (value.StartsWith("0x"))
            {
                return Convert.ToInt64(value, 16);
            }
            else
            {
                return long.Parse(value);
            }
        }
    }

    public class DateEscape : IValueHandler
    {

        public Regex Pattern
        {
            get { return new Regex(@"^\[\s?(today|now)([^\]]*)](?:(.+))?$"); }
        }

        public object Handle(string value)
        {
            var match = Pattern.Match(value);
            DateTime ret = DateTime.Today;
            if (match.Groups[1].Value.Contains("now"))
            {
                ret = DateTime.Now;
            }

            var offset = match.Groups[2].Value.Trim();
            if (!String.IsNullOrEmpty(offset))
            {
                if (offset.Contains(":"))
                {
                    ret = ret.Add(TimeSpan.Parse(offset.Replace(" ", "")));
                }
                else
                {
                    offset = offset.Replace(" ", "");
                    if (offset.StartsWith("+"))
                    {
                        offset = offset.Substring(1);
                    }

                    ret = ret.AddDays(double.Parse(offset));
                }
            }

            if (match.Groups.Count > 3 && !String.IsNullOrEmpty(match.Groups[3].Value))
            {
                return ret.ToString(match.Groups[3].Value.Substring(1).Trim());
            }
            else
            {
                return ret.ToString();
            }
        }
    }

    public class DateCompare : ICompareHandler
    {
        public Regex Pattern
        {
            get { return new Regex(@"^\[today]$"); }
        }

        public bool Test(string compare, AttributeValue value)
        {
            try
            {
                return DateTime.Parse(value.StringValue).Date == DateTime.Today;
            }
            catch
            {
                return false;
            }
        }
    }

    public class RuleEscape : IValueHandler
    {
        public RuleEscape() { }

        public Regex Pattern
        {
            get { return new Regex(@"^\[Rule\((.+)\)]$", RegexOptions.Singleline); }
        }

        public object Handle(string value)
        {
            var rule = new Rule(Pattern.Match(value).Groups[1].Value, null);
            return rule.GetValue(new Dictionary<string, AttributeValue>()).Value;
        }
    }

    public class DecimalEscape : IValueHandler
    {

        public Regex Pattern
        {
            get { return new Regex(@"^\[(?:-?\d*\.\d+)\]$"); }
        }

        public object Handle(string value)
        {
            return double.Parse(value.Substring(1, value.Length - 2));
        }
    }
    
    public static class Evaluate
    {
        static Dictionary<string, object> memoValues;
        public static Dictionary<string, object> MemoValues
        {
            get
            {
                if (memoValues == null) { memoValues = new Dictionary<string, object>(); }
                return memoValues;
            }
            set
            {
                memoValues = value;
            }
        }

        public static object FixtureVal(string val, params IValueHandler[] handlers)
        {
            foreach (var handler in handlers)
            {
                if (handler.Pattern.IsMatch(val.Trim()))
                {
                    return handler.Handle(val.Trim());
                }
            }

            return val;
        }


        public static List<ICompareHandler> ValueCompareHandlers;

        public static bool TestAssertVal(AssertField field, IDictionary<string, AttributeValue> data, IProvider provider)
        {
            return TestAssertVal(field.Type, field.Field, field.Value, data, provider);
        }

        public static bool TestAssertVal(AssertField field, IDictionary<string, AttributeValue> data, IProvider provider, params ICompareHandler[] compareHandlers)
        {
            return TestAssertVal(field.Type, field.Field, field.Value, data, provider, compareHandlers);
        }

        public static bool TestAssertVal(AssertFieldType fieldType, string attribute, string value, IDictionary<string, AttributeValue> data, IProvider provider)
        {
            return TestAssertVal(fieldType, attribute, value, data, provider, new NullCompare(), new DateCompare());
        }

        public static bool TestAssertVal(AssertFieldType fieldType, string attribute, string value, IDictionary<string, AttributeValue> data, IProvider provider, params ICompareHandler[] compareHandlers)
        {
            AttributeValue attr;
            if (!String.IsNullOrEmpty(attribute))
            {
                attr = data[attribute];
            }
            else
            {
                attr = new AttributeValue();
            }

            if (attr.DataType == AttributeType.MultiValue)
            {
                bool pass = false;
                foreach (var val in attr.Values)
                {
                    switch (fieldType)
                    {
                        case AssertFieldType.Regex:
                            if (val.StringValue == null)
                                return false;
                            pass = Regex.IsMatch(val.StringValue, value);
                            break;
                        case AssertFieldType.Rule:
                            var rule = new Rule(value, attribute);
                            pass = rule.GetValue(new Dictionary<string, AttributeValue> { { attribute, val } }).BooleanValue;
                            break;
                        default:
                            var handled = false;
                            foreach (var handler in compareHandlers)
                            {
                                if (handler.Pattern.IsMatch(value))
                                {
                                    handled = true;
                                    pass = handler.Test(value, val);
                                    break;
                                }
                            }

                            if (!handled)
                            {
                                pass = new AttributeValue(value).Equals(val);
                            }

                            break;
                    }

                    if (pass)
                        break;
                }

                return pass;
            }
            else
            {
                switch (fieldType)
                {
                    case AssertFieldType.Regex:
                        if (data[attribute].StringValue == null)
                            return false;
                        return Regex.IsMatch(data[attribute].StringValue, value);
                    case AssertFieldType.Rule:
                        var rule = new Rule(value, attribute);
                        return rule.GetValue(data).BooleanValue;
                    default:
                        foreach (var handler in compareHandlers)
                        {
                            if (handler.Pattern.IsMatch(value))
                            {
                                return handler.Test(value, attr);
                            }
                        }

                        return new AttributeValue(value).Equals(attr);
                }
            }
        }
    }
}
