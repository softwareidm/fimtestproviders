﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace SoftwareIDM.FIMTest.ProviderModel
{
    /// <summary>
    /// RuleType lists the possible kinds of rule
    /// </summary>
    public enum RuleType
    {
        Literal,     // A String literal. e.g. "Hello World."
        Number,     // A positive or negative number. e.g. 42
        Bool,        // Rule is literally true or false
        Function,    // A more complicated rule, represents a function to invoke
        Parenthetical, //A parenthetical expression, typically to group items for operator precedence
        Field,       // A field lookup on a CS or MV object.
        Concatenate, // String concatenation using the + operator
        Comparison,  // Comparison Operator
        Empty        // An empty Rule does nothing.
    }

    static class ComparisonOperators
    {
        public static AttributeValue Compare(AttributeValue left, AttributeValue right, string oper)
        {
            switch (oper)
            {
                case "<":
                    return new AttributeValue(left.CompareTo(right) < 0);
                case ">":
                    return new AttributeValue(left.CompareTo(right) > 0);
                case "<=":
                    return new AttributeValue(left.CompareTo(right) <= 0);
                case ">=":
                    return new AttributeValue(left.CompareTo(right) >= 0);
                case "==":
                    return new AttributeValue(left.Equals(right));
                default:
                    return new AttributeValue(false);
            }
        }
    }

    public class Rule
    {
        List<Rule> parameters = new List<Rule>();
        Dictionary<string, Rule> namedParameters = new Dictionary<string, Rule>();

        public RuleType RuleType { get; set; }
        public string Token { get; set; }
        public IDictionary<string, AttributeValue> Data { get; set; }

        string ComparisonOperator;
        IProvider Provider { get; set; }
        string DefaultAttribute { get; set; }

        readonly Regex reFunc = new Regex(@"^(\w+)\(");
        readonly Regex reNamedParam = new Regex(@"^(\w+)(?:=|\s=\s)");
        readonly Regex reField = new Regex(@"^[-\w.]+");
        readonly Regex reNumber = new Regex(@"^(?:(?:0x|-)?[\dA-F]+(?:\.\d+)?)");
        readonly Regex reBool = new Regex(@"^(true|false|True|False)");
        readonly Regex reLit = new Regex(@"^""(.*?)(?<!\\)""", RegexOptions.Multiline); // negative lookbehind allows escaping of "
        
        readonly Regex reComparison = new Regex(@"^(>=|<=|>|<|==)");

        readonly Regex reInlineComment = new Regex(@"(?<!\\)//.*?\n", RegexOptions.Singleline);
        readonly Regex reBlockComment = new Regex(@"/\*.+?\*/", RegexOptions.Singleline);

        private Rule() { }

        string fullToken;

        public Rule(string rule, string defaultAttribute)
        {
            fullToken = rule;
            string left;
            Rule hold;
            try
            {
                hold = new Rule(rule, defaultAttribute, out left);
            }
            catch (Exception e)
            {
                throw new FormatException(String.Format("Unable to parse {0}, {1}", fullToken, e.Message), e);
            }

            DefaultAttribute = defaultAttribute;
            RuleType = hold.RuleType;
            Token = hold.Token;
            ComparisonOperator = hold.ComparisonOperator;
            parameters = hold.parameters;
            namedParameters = hold.namedParameters;
        }

        /// <summary>
        /// Tokenizes the rule string into a parse tree of Rule instances.
        /// 
        /// The simple parsing algorithm consumes the rule from beginning to end, tokenizing with
        /// regular expressions (and in a couple cases String.StartsWith).
        /// In the case of the Function RuleType, Rules may be nested as function parameters or named parameters.
        /// 
        /// For included available Rule Functions refer to CommonFunctions.cs
        /// </summary>
        /// <param name="rule">String to parse as a rule.</param>
        /// <param name="left">Remainder of string. Unconsumed text may be further parsed as other Rules.</param>
        private Rule(string rule, string defaultAttribute, out string left)
        {
            DefaultAttribute = defaultAttribute;
            if (reInlineComment.IsMatch(rule))
            {
                rule = reInlineComment.Replace(rule, delegate(Match m) { return ""; });
            }

            if (reBlockComment.IsMatch(rule))
            {
                rule = reBlockComment.Replace(rule, delegate(Match m) { return ""; });
            }

            rule = rule.Trim();

            left = "";
            if (String.IsNullOrEmpty(rule))
            {
                RuleType = RuleType.Empty;
                return;
            }

            var prevLength = rule.Length;
            while (rule.Length > 0)
            {
                if (reFunc.IsMatch(rule))
                {
                    Token = reFunc.Match(rule).Groups[1].Value;
                    RuleType = RuleType.Function;
                    rule = rule.Substring(Token.Length + 1);
                    while (!rule.StartsWith(")")) // parse arguments until we get to the close paren
                    {
                        if (rule.Length == 0)
                        {
                            throw new FormatException("Unmatched parenthesis.");
                        }

                        if (reNamedParam.IsMatch(rule))
                        {
                            var match = reNamedParam.Match(rule);
                            var name = match.Groups[1].Value;
                            rule = rule.Substring(match.Length);
                            namedParameters.Add(name, new Rule(rule, defaultAttribute, out rule));
                        }
                        else
                        {
                            parameters.Add(new Rule(rule, defaultAttribute, out rule));
                        }

                        if (rule.StartsWith(",")) // technically arguments don't have to be comma separated, but this handles the comma
                        {
                            rule = rule.Substring(1);
                        }

                        rule = rule.Trim();
                    }

                    rule = rule.Substring(1); // remove terminating )
                }
                else if (rule.StartsWith("("))
                {
                    Token = "(";
                    RuleType = RuleType.Parenthetical;
                    rule = rule.Substring(1).Trim();

                    parameters.Add(new Rule(rule, defaultAttribute, out rule));

                    if (!rule.StartsWith(")"))
                    {
                        throw new FormatException("Unable to parse rule, unmatched parenthesis.");
                    }

                    rule = rule.Substring(1); // remove close paren
                }
                else if (reNumber.IsMatch(rule))
                {
                    Token = reNumber.Match(rule).Value;
                    RuleType = RuleType.Number;
                    rule = rule.Substring(Token.Length);
                }
                else if (reBool.IsMatch(rule.ToLower()))
                {
                    Token = reBool.Match(rule.ToLower()).Groups[1].Value;
                    RuleType = RuleType.Bool;
                    rule = rule.Substring(Token.Length);
                }
                else if (reField.IsMatch(rule))
                {
                    Token = reField.Match(rule).Value;
                    RuleType = RuleType.Field;
                    rule = rule.Substring(Token.Length);
                }
                else if (reLit.IsMatch(rule))
                {
                    Token = reLit.Match(rule).Groups[1].Value;
                    RuleType = RuleType.Literal;
                    rule = rule.Substring(Token.Length + 2);
                    Token = Token.Replace(@"\\", @"\");
                    var specials = new Dictionary<string, string> { { "\\r", "\r" }, { "\\n", "\n" }, { "\\t", "\t" }, { "\\\"", "\"" } };
                    foreach (var pair in specials)
                    {
                        Token = Regex.Replace(Token, @"(<!\\)\" + pair.Key, pair.Value);
                    }
                }
                else if (reComparison.IsMatch(rule))
                {
                    // copy current rule into first parameter
                    var add = new Rule { 
                        Token = Token, 
                        RuleType = RuleType, 
                        parameters = parameters, 
                        namedParameters = namedParameters, 
                        ComparisonOperator = ComparisonOperator, 
                        DefaultAttribute = defaultAttribute };
                    parameters = new List<Rule> { add };
                    Token = "";

                    var match = reComparison.Match(rule);
                    ComparisonOperator = match.Groups[1].Value;
                    Token = ComparisonOperator;
                    RuleType = RuleType.Comparison;
                    parameters.Add(new Rule(rule.Substring(match.Length), defaultAttribute, out rule));
                }
                else if (rule.StartsWith("+"))
                {
                    var add = new Rule { Token = Token, RuleType = RuleType, parameters = parameters };
                    parameters = new List<Rule> { add };
                    Token = "";

                    RuleType = RuleType.Concatenate;
                    parameters.Add(new Rule(rule.Substring(1), defaultAttribute, out rule));
                }
                else
                {
                    left = rule.Trim();
                    return;
                }

                rule = rule.Trim();

                if (rule.Length == prevLength)
                {
                    throw new FormatException("Unable to parse remainder of rule: " + rule);
                }
            } // end while
        } // end constructor

        /// <summary>
        /// Invoke a Function Rule using System.Reflection
        /// </summary>
        private AttributeValue Invoke()
        {
            MethodInfo method;
            try
            {
                method = ProviderTypes.RuleFunctions[Token];
            }
            catch (KeyNotFoundException)
            {
                throw new KeyNotFoundException(String.Format("Unable to find function {0} in loaded classes", Token));
            }

            var pi = method.GetParameters();
            var args = new List<object>();
            int positionalArg = 0;
            for (int i = 0; i < pi.Length; i++)
            {
                if (i < pi.Length - 1)
                {
                    if (namedParameters.ContainsKey(pi[i].Name))
                    {
                        args.Add(namedParameters[pi[i].Name].GetValue(Data));
                    }
                    else if (parameters.Count > positionalArg)
                    {
                        args.Add(parameters[positionalArg].GetValue(Data));
                        positionalArg++;
                    }
                    else
                    {
                        args.Add(null);
                    }
                }
                else if (pi.Last().ParameterType.IsArray)
                {
                    var array = new List<AttributeValue>();
                    while (positionalArg < parameters.Count)
                    {
                        array.Add(parameters[positionalArg].GetValue(Data));
                        positionalArg++;
                    }

                    args.Add(array.ToArray());
                }
                else if (pi.Last().ParameterType.IsAssignableFrom(typeof(Dictionary<string, AttributeValue>)))
                {
                    var named = new Dictionary<string, AttributeValue>();
                    foreach (var key in namedParameters.Keys)
                    {
                        named[key] = namedParameters[key].GetValue(Data);
                    }

                    args.Add(named);
                }
                else
                {
                    if (namedParameters.ContainsKey(pi[i].Name))
                    {
                        args.Add(namedParameters[pi[i].Name].GetValue(Data));
                    }
                    else if (parameters.Count > positionalArg)
                    {
                        args.Add(parameters[positionalArg].GetValue(Data));
                        positionalArg++;
                    }
                    else
                    {
                        args.Add(null);
                    }
                }
            }

            try
            {
                return (AttributeValue)method.Invoke(null, args.ToArray());
            }
            catch (TargetInvocationException e)
            {
                throw new ArgumentException(String.Format(
                    "Error executing rule {0}: {1}", Token, e.InnerException.Message), e.InnerException);
            }
            catch (TargetParameterCountException e)
            {
                throw new TargetParameterCountException(String.Format("Parameter count mismatch for {0}({1})",
                    Token,
                    String.Join(",", (from p in parameters select p.Token).ToArray())), e);
            }
        } // end Invoke

        /// <summary>
        /// Construct an AttributeValue that's primed to bind itself when the value is accessed.
        /// </summary>
        public AttributeValue GetValue(IDictionary<string, AttributeValue> data)
        {
            this.Data = data;
            AttributeValue ret = new AttributeValue(this);
            return ret;
        }

        /// <summary>
        /// Callback invoked by Rule based AttributeValue to execute the Rule and bind the value.
        /// </summary>
        public AttributeValue LateBind()
        {
            switch (RuleType)
            {
                case RuleType.Field:
                    if (Token.ToLower() == "value")
                    {
                        return Data[DefaultAttribute];
                    }
                    else if (Token.ToLower() == "provider")
                    {
                        return new AttributeValue(Provider);
                    }
                    else
                    {
                        return Data[Token];
                    }
                case RuleType.Literal:
                    return new AttributeValue(Token);
                case RuleType.Parenthetical:
                    return parameters[0].GetValue(Data);
                case RuleType.Number:
                    try
                    {
                        if (Token.StartsWith("0x"))
                        {
                            return new AttributeValue(Convert.ToInt64(Token, 16));
                        }
                        else
                        {
                            return new AttributeValue(long.Parse(Token));
                        }
                    }
                    catch (FormatException)
                    {
                        return new AttributeValue(double.Parse(Token));
                    }
                case RuleType.Bool:
                    return new AttributeValue(bool.Parse(Token));
                case RuleType.Concatenate:
                    if (parameters.Count == 2)
                    {
                        return new AttributeValue
                        {
                            StringValue = String.Join("", new string[] {
                                parameters[0].GetValue(Data).StringValue, 
                                parameters[1].GetValue(Data).StringValue
                            })
                        };
                    }
                    else
                    {
                        return parameters[0].GetValue(Data);
                    }
                case RuleType.Comparison:
                    if (parameters.Count == 2)
                    {
                        return ComparisonOperators.Compare(parameters[0].GetValue(Data), parameters[1].GetValue(Data), this.ComparisonOperator);
                    }
                    else
                    {
                        return parameters[0].GetValue(Data);
                    }
                case RuleType.Function:
                    return Invoke();
                default:
                    return null;
            }
        }
    }
}
