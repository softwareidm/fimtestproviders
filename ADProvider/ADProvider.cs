﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SoftwareIDM.FIMTest.ProviderModel;

namespace SoftwareIDM.FIMTest.Provider
{
    class EntryWrapper : IDictionary<string, AttributeValue>
    {
        DirectoryEntry Entry { get; set; }
        public EntryWrapper(DirectoryEntry entry)
        {
            Entry = entry;
        }

        #region Unimplemented
        public void Add(string key, AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(string key)
        {
            throw new NotImplementedException();
        }

        public ICollection<string> Keys
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(string key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(string key, out AttributeValue value)
        {
            throw new NotImplementedException();
        }

        public ICollection<AttributeValue> Values
        {
            get { throw new NotImplementedException(); }
        }

        public void Add(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<string, AttributeValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(KeyValuePair<string, AttributeValue> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<string, AttributeValue>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion

        static long ConvertLargeInt(object val, Type type)
        {
            if (type.GetMember("HighPart") == null) { throw new ArgumentException("Unable to convert AD COM Type"); }

            int highPart = (int)type.InvokeMember("HighPart", System.Reflection.BindingFlags.GetProperty, null, val, null);
            int lowPart = (int)type.InvokeMember("LowPart", System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.Public, null, val, null);

            return (long)highPart << 32 | (uint)lowPart;
        }
        
        public AttributeValue this[string key]
        {
            get
            {
                if (key.ToLower() == "dn")
                {
                    Regex parseDN = new Regex(@"((?:CN|OU)=.*)$");
                    return new AttributeValue(parseDN.Match(Entry.Path).Groups[1].Value);
                }

                if (key.ToLower() == "objecttype")
                {
                    return new AttributeValue(Entry.SchemaClassName);
                }
                
                if (!Entry.Properties.Contains(key))
                {
                    Entry.RefreshCache(new string[] { key });
                }

                var val = Entry.Properties[key].Value;

                if (val == null)
                {
                    return new AttributeValue(null);
                }
                else
                {
                    var t = val.GetType();

                    if (t.Name == "__ComObject") // try if it's a long int
                    {
                        return new AttributeValue(ConvertLargeInt(val, t));
                    }
                    else if (t == typeof(object[]))
                    {
                        var vals = new List<IComparable>();
                        foreach (var v in (object[])val)
                        {
                            vals.Add((IComparable)v);
                        }

                        return AttributeValue.MultiValue(vals, MultiBehavior.Append);
                    }
                    else
                    {
                        return new AttributeValue(val);
                    }
                }
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public struct ADAttr
    {
        public string Name { get; set; }
        public string CN { get; set; }
        public bool Multivalued { get; set; }
        public string ID { get; set; }

        public JObject GetJson()
        {
            return new JObject(
                new JProperty("Name", Name),
                new JProperty("CN", CN),
                new JProperty("Multivalued", Multivalued),
                new JProperty("ID", ID));
        }
    }

    public class ADSchema
    {
        public string Name { get; set; }
        public string PathName { get; set; }
        public List<string> Attributes { get; set; }

        public ADSchema()
        {
            Attributes = new List<string>();
        }

        public JObject GetJson()
        {
            return new JObject(
                new JProperty("Name", Name),
                new JProperty("PathName", PathName),
                new JProperty("Attributes", new JArray(Attributes)));
        }
    }

    public class ADProvider : IProvider
    {
        public string ConnectionString { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string DNRoot { get; set; }
        public string AuthType { get; set; }

        AuthenticationTypes authType;

        public void Initialize(XElement xml)
        {
            ModelHelper.RequiredElement(xml, ModelHelper.SI + "connectionString", typeof(ADProvider));
            ConnectionString = xml.Element(ModelHelper.SI + "connectionString").Value;

            if (!ConnectionString.EndsWith("/") || Regex.IsMatch(ConnectionString, @"\w{1,3}=.*,"))
            {
                throw new XmlParseException("ADProvider connectionString invalid format: must end in '/' and must not contain a DN", xml, typeof(ADProvider));
            }

            if (xml.Elements(ModelHelper.SI + "user").Count() > 0 && xml.Elements(ModelHelper.SI + "password").Count() > 0)
            {
                User = xml.Element(ModelHelper.SI + "user").Value;
                Password = xml.Element(ModelHelper.SI + "password").Value;
            }

            if (xml.Elements(ModelHelper.SI + "authType").Count() > 0)
            {
                AuthType = xml.Element(ModelHelper.SI + "authType").Value;
                foreach (var type in AuthType.Split('|'))
                {
                    authType = authType | (AuthenticationTypes)Enum.Parse(typeof(AuthenticationTypes), type);
                }
            }

            if (xml.Elements(ModelHelper.SI + "dnRoot").Count() > 0)
            {
                DNRoot = xml.Element(ModelHelper.SI + "dnRoot").Value;
            }
        }

        public XElement GetXml()
        {
            var ret = new XElement(ModelHelper.SI + "provider",
                new XAttribute(ModelHelper.XSI + "type", "ADProvider"),
                new XElement(ModelHelper.SI + "connectionString", ConnectionString));
            if (!String.IsNullOrEmpty(User))
            {
                ret.SetElementValue(ModelHelper.SI + "user", User);
                ret.SetElementValue(ModelHelper.SI + "password", Password);
            }

            if (!String.IsNullOrEmpty(AuthType))
            {
                ret.SetElementValue(ModelHelper.SI + "authType", AuthType);
            }

            if (!String.IsNullOrEmpty(DNRoot))
            {
                ret.SetElementValue(ModelHelper.SI + "dnRoot", DNRoot);
            }

            return ret;
        }

        public JObject GetSchema()
        {
            DirectoryEntry schema = BaseEntry("schema");
            
            var classes = new List<ADSchema>();
            var attributes = new Dictionary<string, ADAttr>();
            var mustSkip = new List<string> { "instanceType", "nTSecurityDescriptor", "objectCategory", "objectClass" };
            foreach (DirectoryEntry res in schema.Children)
            {
                if (res.SchemaClassName == "Class")
                {
                    var add = new ADSchema { Name = (string)res.Properties["cn"].Value, PathName = res.Name };
                    var tempList = new List<string>();
                    foreach (string attr in res.Properties["mustContain"])
                    {
                        if (mustSkip.Contains(attr))
                        {
                            continue;
                        }

                        tempList.Add(attr);
                    }

                    tempList.Sort();
                    add.Attributes.AddRange(tempList);
                    tempList.Clear();

                    foreach (string attr in res.Properties["mayContain"])
                    {
                        tempList.Add(attr);
                    }

                    tempList.Sort();
                    add.Attributes.AddRange(tempList);

                    classes.Add(add);
                }
                else if (res.SchemaClassName == "Property")
                {
                    var add = new ADAttr
                    {
                        Name = res.Name,
                        CN = (string)res.Properties["cn"].Value,
                        Multivalued = !(bool)res.Properties["isSingleValued"].Value,
                        ID = res.Properties["attributeID"].Value.ToString()
                    };

                    attributes.Add(add.Name, add);
                }

                res.Dispose();
            }

            schema.Dispose();

            return new JObject(
                new JProperty("Classes",
                    new JArray(
                        from c in classes
                        select c.GetJson())),
                new JProperty("Attributes", new JObject(
                    from a in attributes.Values
                    select new JProperty(a.Name, a.GetJson()))));
        }

        DirectoryEntry BaseEntry(string dn)
        {
            DirectoryEntry ret;
            if (String.IsNullOrEmpty(User))
            {
                ret = new DirectoryEntry(ConnectionString + dn);
            }
            else if (String.IsNullOrEmpty(AuthType))
            {
                ret = new DirectoryEntry(ConnectionString + dn, User, Password);
            }
            else
            {
                ret = new DirectoryEntry(ConnectionString + dn, User, Password, authType);
            }

            return ret;
        }

        void CreateObject(ADFixture fixture)
        {
            Regex parseDN = new Regex(@"^((?:CN|OU)=.*?)(?<!\\),(.*)$");
            var res = parseDN.Match(fixture.DN.Replace("[dnRoot]", DNRoot)).Groups;
            string cn = res[1].Value;
            string container = res[2].Value;
            DirectoryEntry parent = User == null ?
                new DirectoryEntry(ConnectionString + container) :
                new DirectoryEntry(ConnectionString + container, User, Password);

            if (!DirectoryEntry.Exists(parent.Path))
            {
                var addContainer = new ADFixture
                {
                    DN = container,
                    SchemaClassName = "OrganizationalUnit",
                    Operation = "Add"
                };

                CreateObject(addContainer);
                CreateObject(fixture);
                return;
            }

            // Use existing object if already created.
            DirectoryEntry add;
            if (!DirectoryEntry.Exists(ConnectionString + fixture.DN.Replace("[dnRoot]", DNRoot)))
            {
                add = parent.Children.Add(cn, fixture.SchemaClassName);
            }
            else
            {
                add = parent.Children.Find(cn, fixture.SchemaClassName);
            }

            parent.Dispose();

            if (!String.IsNullOrEmpty(fixture.SAMAccountName))
            {
                add.Properties["sAMAccountName"].Value = fixture.SAMAccountName;
            }

            if (!String.IsNullOrEmpty(fixture.GroupType))
            {
                add.Properties["groupType"].Value = ADFixture.HandleADVal(fixture.GroupType, this);
            }

            foreach (var field in fixture.Fields)
            {
                add.Properties[field.Key].Value = ADFixture.HandleADVal(field.Value, this);
            }

            foreach (var field in fixture.Multifields)
            {
                if (field.ReplaceMode)
                {
                    add.Properties[field.Name].Clear();
                }

                foreach (string val in field.Values)
                {
                    if (!add.Properties[field.Name].Contains(ADFixture.HandleADVal(val, this)))
                    {
                        add.Properties[field.Name].Add(ADFixture.HandleADVal(val, this));
                    }
                }
            }

            const long PASSWD_NOTREQD = 0x0020;
            if (fixture.SchemaClassName.ToLower() == "user")
            {
                var uac = long.Parse(ADFixture.HandleADVal(fixture.UserAccountControl, this).ToString());
                add.Properties["userAccountControl"].Value = (uac | PASSWD_NOTREQD).ToString();
                add.CommitChanges();
                add.Invoke("SetPassword", new Object[] { fixture.UnicodePwd });
                add.Properties["userAccountControl"].Value = uac.ToString();
                add.CommitChanges();
            }
            else
            {
                add.CommitChanges();
            }

            add.Dispose();
        }

        void UpdateObject(ADFixture fixture)
        {
            DirectoryEntry entry = GetEntry(fixture.DN, fixture.Filter);

            if (!String.IsNullOrEmpty(fixture.NewDN))
            {
                var NewDN = fixture.NewDN.Replace("[dnRoot]", DNRoot);
                Regex parseDN = new Regex(@"^((?:CN|OU)=.*?)(?<!\\),(.*)$");
                var res = parseDN.Match(NewDN).Groups;
                string cn = res[1].Value;
                string container = res[2].Value;
                DirectoryEntry parent = BaseEntry(container);

                entry.MoveTo(parent, cn);
                parent.Dispose();
            }

            if (!String.IsNullOrEmpty(fixture.UnicodePwd))
            {
                entry.Invoke("SetPassword", new Object[] { fixture.UnicodePwd });
            }

            if (!String.IsNullOrEmpty(fixture.SAMAccountName))
            {
                entry.Properties["sAMAccountName"].Value = fixture.SAMAccountName;
            }

            if (!String.IsNullOrEmpty(fixture.UserAccountControl))
            {
                entry.Properties["userAccountControl"].Value = ADFixture.HandleADVal(fixture.UserAccountControl, this).ToString();
            }

            foreach (var field in fixture.Fields)
            {
                entry.Properties[field.Key].Value = ADFixture.HandleADVal(field.Value, this);
            }

            foreach (var field in fixture.Multifields)
            {
                if (field.ReplaceMode)
                {
                    entry.Properties[field.Name].Clear();
                }

                foreach (string val in field.Values)
                {
                    if (!entry.Properties[field.Name].Contains(val))
                    {
                        entry.Properties[field.Name].Add(ADFixture.HandleADVal(val, this));
                    }
                }
            }

            entry.CommitChanges();
            entry.Dispose();
        }

        void DeletesFilter(ADFixture fixture)
        {
            DirectoryEntry root = BaseEntry(DNRoot);

            var filt = ADFixture.HandleADVal(fixture.Filter, this).ToString();
            var search = new DirectorySearcher(root, filt);
            search.PageSize = 500;

            foreach (SearchResult s in search.FindAll())
            {
                using (var entry = s.GetDirectoryEntry())
                {
                    entry.DeleteTree();
                }
            }
            
        }

        void DeleteObject(ADFixture fixture)
        {
            if (!String.IsNullOrEmpty(fixture.Filter) && fixture.DeleteMultiple)
            {
                DeletesFilter(fixture);
                return;
            }

            DirectoryEntry entry = GetEntry(fixture.DN, fixture.Filter);
            if (entry == null)
            {
                return;
            }

            try
            {
                entry.DeleteTree();
            }
            catch (DirectoryServicesCOMException e)
            {
                if (!e.Message.Contains("no such object"))
                    throw e;
            }

            entry.Dispose();
        }

        void ClearObject(ADFixture fixture)
        {
            DirectoryEntry entry = GetEntry(fixture.DN, fixture.Filter);

            try
            {
                foreach (DirectoryEntry child in entry.Children)
                {
                    child.DeleteTree();
                }
            }
            catch (DirectoryServicesCOMException e)
            {
                if (!e.Message.Contains("no such object"))
                    throw e;
            }

            entry.Dispose();
        }

        public void WriteFixture(IFixture write)
        {
            if (!(write is ADFixture))
            {
                throw new Exception("ADProvider must take ADFixture");
            }
            ADFixture fix = (ADFixture)write;

            switch (write.Operation)
            {
                case "Add":
                    CreateObject(fix);
                    break;
                case "Update":
                case "Move":
                    UpdateObject(fix);
                    break;
                case "Delete":
                    DeleteObject(fix);
                    break;
                case "Clear":
                    ClearObject(fix);
                    break;
                default:
                    throw new NotImplementedException(String.Format("This provider does not support fixture operation {0}", fix.Operation));
            }
        }

        DirectoryEntry GetEntry(string dn, string filter)
        {
            DirectoryEntry entry = null;
            if (String.IsNullOrEmpty(dn))
            {
                DirectoryEntry root = BaseEntry(DNRoot);

                var filt = ADFixture.HandleADVal(filter, this).ToString();
                var search = new DirectorySearcher(root, filt);
                try
                {
                    var s = search.FindOne();
                    entry = s.GetDirectoryEntry();
                }
                catch
                {
                    entry = null;
                }
            }
            else
            {
                entry = BaseEntry(ADFixture.HandleADVal(dn, this).ToString());
            }

            return entry;
        }

        public bool TestAssert(IAssertion assertion)
        {

            if (!(assertion is ADAssert))
            {
                throw new ArgumentException("ADProvider only accepts ADAssertions");
            }

            var assert = (ADAssert)assertion;

            DirectoryEntry entry = GetEntry(assert.DN, assert.Filter);
            bool exists = false;
            using (entry)
            {
                switch (assert.Operation)
                {
                    case "Exists":
                        try
                        {
                            var tmp = entry.SchemaClassName;
                            exists = true;
                        }
                        catch { }
                        
                        return exists;
                    case "NotExists":
                        try
                        {
                            var tmp = entry.SchemaClassName;
                            exists = true;
                        }
                        catch { }
                        
                        return !exists;
                    case "NotHasValue":
                        // iterate fields, fail if any DO match
                        foreach (var field in assert.Fields)
                        {
                            if (Evaluate.TestAssertVal(field, new EntryWrapper(entry), this))
                            {
                                throw new FormatException(String.Format("Field {0}:{1} matches {2}",
                                        field.Field, entry.Properties[field.Field].Value, field.Value));
                            }
                        }

                        // iterate fields, fail if any DO contain the value
                        foreach (var multi in assert.MultiFields)
                        {
                            if (Evaluate.TestAssertVal(multi, new EntryWrapper(entry), this))
                            {
                                throw new FormatException(String.Format("Field {0} matches {2}",
                                        multi.Field, multi.Value));
                            }
                        }

                        return true;
                    case "HasValue":
                        // iterate fields, fail if any don't match
                        foreach (var field in assert.Fields)
                        {
                            if (!Evaluate.TestAssertVal(field, new EntryWrapper(entry), this))
                            {
                                throw new FormatException(String.Format("Field {0}:{1} does not match {2}",
                                        field.Field, entry.Properties[field.Field].Value, field.Value));
                            }
                        }

                        // iterate multifields, fail if any don't contain the value
                        foreach (var multi in assert.MultiFields)
                        {
                            if (!Evaluate.TestAssertVal(multi, new EntryWrapper(entry), this))
                            {
                                throw new FormatException(String.Format("Field {0} does not match {1}",
                                        multi.Field, multi.Value));
                            }
                        }

                        return true;
                    default:
                        return false;
                }
            }
        }
    }

    public class MultiField
    {
        public bool ReplaceMode { get; set; }
        public string Name { get; set; }
        public List<string> Values { get; set; }

        public MultiField()
        {
            Values = new List<string>();
        }

        public MultiField(XElement xml)
        {
            Values = new List<string>();
            ModelHelper.RequiredAttribute(xml, "name", typeof(ADFixture));
            Name = xml.Attribute("name").Value;
            if (xml.Attributes("type").Count() > 0 && xml.Attribute("type").Value == "Replace")
            {
                ReplaceMode = true;
            }

            Values.AddRange(
                from v in xml.Elements(ModelHelper.SI + "value")
                select v.Value);
        }

        public XElement GetXml()
        {
            return new XElement(ModelHelper.SI + "multifield",
                new XAttribute("name", Name),
                new XAttribute("type", ReplaceMode ? "Replace" : "Append"),
                from v in Values
                select new XElement(ModelHelper.SI + "value", v));
        }
    }

    class DNRootEscape : IValueHandler
    {
        public ADProvider System { get; set; }
        public Regex Pattern
        {
            get { return new Regex(@"\[dnRoot]"); }
        }

        public object Handle(string value)
        {
            return value.Replace("[dnRoot]", System.DNRoot);
        }
    }

    class GroupTypeEscape : IValueHandler
    {
        public Regex Pattern
        {
            get { return new Regex(@"^\[(?:Domain Local Security|Global Security|Universal Security|Domain Local Distribution|Global Distribution|Universal Distribution)]$"); }
        }

        public object Handle(string value)
        {
            var groupTypes = new Dictionary<string, int> {
                { "[Domain Local Security]", -2147483644 },
                { "[Global Security]", -2147483646 },
                { "[Universal Security]", -2147483640 },
                { "[Domain Local Distribution]", 4 },
                { "[Global Distribution]", 2 },
                { "[Universal Distribution]", 8 },
            };

            return groupTypes[value.Trim()];
        }
    }

    public class ADFixture : Fixture
    {
        // AD specific attributes
        public string DN { get; set; }
        public string NewDN { get; set; }
        public string Filter { get; set; }
        public string SchemaClassName { get; set; }
        public string SAMAccountName { get; set; }
        public string UnicodePwd { get; set; }
        public string GroupType { get; set; }
        public string UserAccountControl { get; set; }
        public bool DeleteMultiple { get; set; }

        public List<MultiField> Multifields { get; set; }

        public ADFixture()
            : base()
        {
            Multifields = new List<MultiField>();
        }

        public static object HandleADVal(string val, IProvider provider)
        {
            return Evaluate.FixtureVal(val,
                new NullEscape(),
                new IntEscape(),
                new DateEscape(),
                new RuleEscape(),
                new DecimalEscape(),
                new DNRootEscape { System = (ADProvider)provider },
                new GroupTypeEscape());
        }

        void RequireDnRoot(string field, XElement xml)
        {
            if (field.Contains("[dnRoot]"))
            {
                if (String.IsNullOrEmpty(((ADProvider)System.Provider).DNRoot))
                {
                    throw new XmlParseException("ADFixture references dnRoot not defined by provider", xml, typeof(ADFixture));
                }
            }
        }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);

            if (!new List<string> { "Add", "Update", "Move", "Clear", "Delete" }.Contains(Operation))
            {
                throw new XmlParseException(String.Format("ADFixture does not support {0} operation", Operation), xml, typeof(ADFixture));
            }

            if (System.Provider.GetType() != typeof(ADProvider))
            {
                throw new XmlParseException("ADFixture requires an ADProvider system", xml, typeof(ADFixture));
            }

            if (xml.Elements(ModelHelper.SI + "DN").Count() > 0)
            {
                DN = xml.Element(ModelHelper.SI + "DN").Value;
                RequireDnRoot(DN, xml);
            }
            else
            {
                Filter = xml.Element(ModelHelper.SI + "filter").Value;
            }

            if (xml.Elements(ModelHelper.SI + "NewDN").Count() > 0)
            {
                NewDN = (string)HandleVal(xml.Element(ModelHelper.SI + "NewDN").Value);
                RequireDnRoot(NewDN, xml);
            }

            if (xml.Elements(ModelHelper.SI + "schemaClassName").Count() > 0)
            {
                SchemaClassName = xml.Element(ModelHelper.SI + "schemaClassName").Value;
            }

            if (this.Operation == "Add")
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "schemaClassName", typeof(ADFixture));
                if (SchemaClassName.ToLower() == "user")
                {
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "sAMAccountName", typeof(ADFixture));
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "unicodePwd", typeof(ADFixture));
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "userAccountControl", typeof(ADFixture));
                }
                else if (SchemaClassName.ToLower() == "group")
                {
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "sAMAccountName", typeof(ADFixture));
                    ModelHelper.RequiredElement(xml, ModelHelper.SI + "groupType", typeof(ADFixture));
                }
            }

            if (xml.Elements(ModelHelper.SI + "sAMAccountName").Count() > 0)
            {
                SAMAccountName = xml.Element(ModelHelper.SI + "sAMAccountName").Value;
            }

            if (xml.Elements(ModelHelper.SI + "groupType").Count() > 0)
            {
                GroupType = xml.Element(ModelHelper.SI + "groupType").Value;
            }

            if (xml.Elements(ModelHelper.SI + "unicodePwd").Count() > 0)
            {
                UnicodePwd = xml.Element(ModelHelper.SI + "unicodePwd").Value;
            }

            if (xml.Elements(ModelHelper.SI + "userAccountControl").Count() > 0 && 
                !String.IsNullOrEmpty(xml.Element(ModelHelper.SI + "userAccountControl").Value))
            {
                try
                {
                    UserAccountControl = xml.Element(ModelHelper.SI + "userAccountControl").Value;
                }
                catch
                {
                    throw new XmlParseException(
                        String.Format("Invalid format for userAccountControl: {0}", xml.Element(ModelHelper.SI + "userAccountControl").Value),
                        xml.Element(ModelHelper.SI + "userAccountControl"), typeof(ADFixture));
                }
            }

            if (Operation == "Delete" && xml.Elements(ModelHelper.SI + "deleteMultiple").Count() > 0)
            {
                DeleteMultiple = xml.Element(ModelHelper.SI + "deleteMultiple").Value.ToLower() == "true" ||
                    xml.Element(ModelHelper.SI + "deleteMultiple").Value == "1";
            }

            foreach (var f in xml.Elements(ModelHelper.SI + "multifield"))
            {
                var add = new MultiField(f);
                Multifields.Add(add);
            }

            foreach (var f in Fields)
            {
                RequireDnRoot(f.Value, xml);
            }

            foreach (var m in Multifields)
            {
                foreach (var f in m.Values)
                {
                    RequireDnRoot(f, xml);
                }
            }
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            if (!String.IsNullOrEmpty(DN))
            {
                ret.Add(new XElement(ModelHelper.SI + "DN", DN));
            }
            else
            {
                ret.Add(new XElement(ModelHelper.SI + "filter", Filter));
            }

            if (!String.IsNullOrEmpty(NewDN))
            {
                ret.Add(new XElement(ModelHelper.SI + "NewDN", NewDN));
            }

            if (!String.IsNullOrEmpty(SchemaClassName))
            {
                ret.Add(new XElement(ModelHelper.SI + "schemaClassName", SchemaClassName));
            }

            if (!String.IsNullOrEmpty(SAMAccountName))
            {
                ret.Add(new XElement(ModelHelper.SI + "sAMAccountName", SAMAccountName));
            }

            if (!String.IsNullOrEmpty(UnicodePwd))
            {
                ret.Add(new XElement(ModelHelper.SI + "unicodePwd", UnicodePwd));
            }

            if (!String.IsNullOrEmpty(UserAccountControl))
            {
                ret.Add(new XElement(ModelHelper.SI + "userAccountControl", UserAccountControl));
            }

            if (!String.IsNullOrEmpty(GroupType))
            {
                ret.Add(new XElement(ModelHelper.SI + "groupType", GroupType));
            }

            if (Operation == "Delete" && DeleteMultiple)
            {
                ret.Add(new XElement(ModelHelper.SI + "deleteMultiple", true));
            }

            if (Multifields.Count > 0)
            {
                ret.Add(
                    from m in Multifields
                    select m.GetXml());
            }

            return ret;
        }
    }

    public class ADAssert : Assertion
    {
        public string DN { get; set; }
        public string Filter { get; set; }
        public List<AssertField> MultiFields { get; set; }

        public ADAssert()
            : base()
        {
            MultiFields = new List<AssertField>();
        }

        public override void Initialize(XElement xml, IDictionary<string, SyncSystem> systems)
        {
            base.Initialize(xml, systems);

            if (xml.Elements(ModelHelper.SI + "DN").Count() > 0)
            {
                DN = xml.Element(ModelHelper.SI + "DN").Value;
            }
            else
            {
                ModelHelper.RequiredElement(xml, ModelHelper.SI + "filter", typeof(ADAssert));
                Filter = xml.Element(ModelHelper.SI + "filter").Value;
            }
            
            foreach (var f in xml.Elements(ModelHelper.SI + "multifield"))
            {
                var t = AssertFieldType.Value;
                if (f.Attributes("type").Count() > 0)
                {
                    try
                    {
                        t = (AssertFieldType)Enum.Parse(typeof(AssertFieldType), f.Attribute("type").Value);
                    }
                    catch (ArgumentException)
                    {
                        throw new XmlParseException(String.Format("Error parsing Assertion {0}. Invalid type {1}.", Name, f.Attribute("type").Value), xml, typeof(ADAssert));
                    }
                }

                MultiFields.Add(new AssertField { Type = t, Value = f.Value, Field = f.Attribute("name").Value });
            }
        }

        public override XElement GetXml()
        {
            var ret = base.GetXml();
            if (!String.IsNullOrEmpty(DN))
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "DN", DN));
            }
            else
            {
                ret.AddFirst(new XElement(ModelHelper.SI + "filter", Filter));
            }

            foreach (var multi in MultiFields)
            {
                var add = new XElement(ModelHelper.SI + "multifield",
                    new XAttribute("name", multi.Field),
                    multi.Value);
                if (multi.Type == AssertFieldType.Regex)
                {
                    add.Add(new XAttribute("type", "Regex"));
                }

                ret.Add(add);
            }

            return ret;
        }
    }
}
